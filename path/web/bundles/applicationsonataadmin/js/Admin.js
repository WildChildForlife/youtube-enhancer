jQuery(document).ready(function(){
    jQuery(".hide-associations").slideUp();
    jQuery(document).on('click', '.preventDefault', function(event) {
        event.preventDefault();
    });
    jQuery(document).on('click', '.show-associations', function(event) {
        event.preventDefault();
        jQuery(this).parents("td").find(".association-list").slideToggle(function(){
            if ( jQuery(".association-list").is(":hidden") )
                jQuery(".show-associations").find("span").text(" (show)");
            else
                jQuery(".show-associations").find("span").text(" (hide)");
        });
    });
});