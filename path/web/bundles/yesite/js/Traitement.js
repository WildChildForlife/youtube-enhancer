$(document).ready(function(){

	var $iNumberImageToShow = 24;
	var $iNumberImgStored = $(".grid-artists .bankImage img").length;
	//$iNumberImageToShow *= 2;

	//$(".bankImage").shuffle();

	//var $iNextIndex = 0;
	//console.log($iNumberImgStored - 2);
	$(".grid-artists .bankImage img").each(function(index, value){
		//console.log(index);
		// ( $iNumberImgStored % 2 === 1 && index + 1 === $iNumberImgStored - 2 )
		//console.log($(".grid-artists .bankImage img").eq(parseInt(index) + 1).length);
		//console.log(index);
		//console.log("condition = " + $(".grid-artists .bankImage img").eq(parseInt(index) + 1).length);
		// 38 + 1 = 26 * 2 || IMG 39 N'existe pas
		if ( index + 1 === $iNumberImageToShow * 2 || $(".grid-artists .bankImage img").eq(parseInt(index) + 1).length === 0 ) return false;
		//console.log(value);
		if ( $(".grid-artists ul img[src='" + $(".grid-artists .bankImage img").eq(index).attr("src") + "']").length ) return true;
		var $oToAppend = 	'<li class="col-xs-1 no-padding">' + 
							$('<div>').append($(".grid-artists .bankImage img").eq(index).clone()).html() + 
							'<div class="front"><a href="#">' + 
							$('<div>').append($(".grid-artists .bankImage img").eq(index).removeClass("no-opacity").clone()).html() + 
							'</a></div><div class="back"><a href="#">' + 
							$('<div>').append($(".grid-artists .bankImage img").eq(index + 1).removeClass("no-opacity").clone()).html() +
							'</a></div></li>';
		$(".grid-artists ul").append($oToAppend);
		$(".grid-artists .bankImageUsed img[src='" + $(".grid-artists .bankImage img").eq(index).attr("src") + "']").remove();
		$(".grid-artists .bankImageUsed img[src='" + $(".grid-artists .bankImage img").eq(index + 1).attr("src") + "']").remove();
		//console.log(index);
		//$iNextIndex = index + 1;
	});

	$(".grid-artists ul").shuffle();

	/*$(".grid-artists ul li").slice(0, $iNumberImageToShow - 1).css({ display:"block" });
	$(".grid-artists ul li").slice($iNumberImageToShow).css({ display:"none" });*/

	setInterval(function(){
		var randToModify = Math.floor(Math.random() * $(".grid-artists li").length) + 0;
		var randFromToGet = Math.floor(Math.random() * $(".bankImageUsed img").length) + 0;
		if ($(".grid-artists li:visible").eq(randToModify).hasClass("flip"))
		{
			$(".grid-artists ul li").eq(randToModify).removeClass("flip");

			var sActualSource = $(".grid-artists ul li").eq(randToModify).find(".front img").attr("src");
			$(".grid-artists ul li").eq(randToModify).find(".front img").attr("src", $(".bankImageUsed img").eq(randFromToGet).attr("src"));
			$(".bankImageUsed img").eq(randFromToGet).attr("src", sActualSource);
		}
		else
		{
			$(".grid-artists ul li").eq(randToModify).addClass("flip");	

			var sActualSource = $(".grid-artists ul li").eq(randToModify).find(".back img").attr("src");
			$(".grid-artists ul li").eq(randToModify).find(".back img").attr("src", $(".bankImageUsed img").eq(randFromToGet).attr("src"));
			$(".bankImageUsed img").eq(randFromToGet).attr("src", sActualSource);

		} 
		//$(".bankImage img").eq(randFromToGet).appendTo(".bankImageUsed");
	}, 4000);

	$(document).on('click', '.toggle-header', function(e) {
		event.preventDefault();
		$("header").stop().animate({ marginTop: "-160px" }, function(){
			if ( $("header .opened").hasClass('hidden') )
			{
				$("header .opened").removeClass('hidden');
				$("header .collapsed").addClass('hidden');
			}
			else
			{
				$("header .collapsed").removeClass('hidden');
				$("header .opened").addClass('hidden');	
			}
			$("header").stop().animate({ marginTop: "0px" });
		});
	});

	$(".logged-user .form-search-website").css({ display:'none', opacity:1 });

	$(document).on('click', ".toggle-search", function(event) 
	{
		event.preventDefault();
		console.log('y');
		if ( $(".form-search-website:visible").length ) $(".form-search-website").stop().hide(500);
		else $(".form-search-website").stop().show(500);
	});

	

	//$('.filter').filterMe();
});