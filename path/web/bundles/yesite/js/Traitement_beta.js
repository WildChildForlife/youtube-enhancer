$(document).ready(function(){

	var $sVideoID = '';
	var $sUrl = '';
	var $sTitle = '';

	$("html").niceScroll(); 
	// Typed animation
	$(".typed").typed({
    	strings: ["http://www.youtube.com/watch?v=Hfjdfbdnf", 'http://www.youtube.com/watch?v=^500fdhsjkfl8767'],
    	typeSpeed: 0,
    	showCursor: true,
    	cursorChar: "|",
    	callback: function(){
    		// Focus l'input à la fin
    		$(".input-convert").focus();
    		// Hide l'animation
    		$(".typed-container").hide(500);
    	}
  	});

	// En cliquant sur l'animatione elle disparait
  	$(document).on("click", ".typed-container", function(){
  		$(this).hide(500);
  		$(".input-convert").focus();
  	});

  	// En faisant une première recherche pour afficher la video en question
  	$(document).on("submit", ".beta-version .form-search-website", function(event){
  		event.preventDefault();
  		// Préservation de l'URL
  		$sUrl = $(".input-convert").val();
  		$(".beta-version .result-convesion").animate({ opacity: 0 }, 500);
  		$(".beta-version .ajax-show-result").animate({ opacity: 1 }, 500);
  		$(".beta-version .loader-result-ajax").show(1000);
  		$("html").getNiceScroll(0).doScrollTop($(".beta-version .ajax-show-result").position().top, 3000);
  		$.ajax({
	        type: "POST",
	        url: PATH_GET,
	        data: {
	           sUrl: $(".input-convert").val()
	        },
	        dataType: "json",
	        success: function(response) {
	        	//console.log(response);
	        	if (response)
	        	{
	        		$sVideoID = response.idVideo;
	        		$sTitle = response.title;
	        		setTimeout(function(){
	        			$(".loader-result-ajax").hide(1000);
		        		$(".beta-version .result-convesion").animate({ opacity: 1 }, 500);
			            $(".video-iframe").empty().append("<iframe src='https://www.youtube.com/embed/" + response.idVideo + "?autoplay=0&showinfo=0&controls=0' frameborder='0' allowfullscreen width='100%' height='220' >");
			            $(".result-title").text(response.title);
			            $(".result-date").text(response.publishedAt);
			            $(".result-channel").html('<a href="https://www.youtube.com/channel/' + response.channelId + '" target="_blank" >' + response.channelTitle + '</a>');
			            if ( response.description.length == 0 ) $(".result-description-container").hide(); 
			            else
		            	{
		            		var $sDescription = response.description;
		            		console.log($sDescription.length);
		            		if ( $sDescription.length > 480 ) $sDescription = $sDescription.substring(0, 480) + '...';
		            		$(".result-description-container").show(); 
		            		$(".result-description").html($sDescription);
		            	}
	        		}, 1000);
	        	}
	        }
	    });
  	});

	$(document).on("click", ".beta-version .form-search-website i", function(event){
		event.preventDefault();
		$(".beta-version .form-search-website").submit();
	});

	$(document).on("submit", ".form-download-file", function(event)
	{
		event.preventDefault();
		$(".loader-preparing-download").css({ display : 'block' }).animate({ opacity : 1 }, 500);
		$.ajax({
	        type: $(this).attr('method'),
	        url: $(this).attr('action'),
	        data: $(this).serialize()
	    }).done(function (data) {
	    	//alert(data.fileToDownload);
	    	$.fileDownload(data.sFileToDownload)
	    	.done(function () { alert('File download a success!'); })
		    .fail(function () { alert('File download failed!'); });
	    	$(".loader-preparing-download").animate({ opacity : 0 }, 500, function(){
	    		$(this).css({ display : 'none' })
	    		$.ajax({
	    			url: PATH_DEL,
	    			type: 'POST',
	    			data: {sFileName: data.sFileName}
	    		}).done(function (data) {
	    			//console.log(data);
	    		});
	    	});
            // if (typeof data.file-to-download !== 'undefined') {
            //     alert(data.file-to-download);
            // }
        });
	});

	$(document).on("click", ".download-ajax-file", function(event){
		//$(".beta-version .form-search-website").unbind("submit");
		event.preventDefault();
		
		var $sUrlToSend = '';
		if ( $(this).hasClass('download-ajax-mp4') ) $sUrlToSend = $sUrl + '??mp4';
		else if ( $(this).hasClass('download-ajax-mp3') ) $sUrlToSend = $sUrl + '??mp3';
		$(".input-download-file").val($sUrlToSend);
		$(".beta-version .form-download-file").submit();
	});
});