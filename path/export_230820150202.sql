-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Dim 23 Août 2015 à 03:02
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `YE`
--

-- --------------------------------------------------------

--
-- Structure de la table `acl_classes`
--

DROP TABLE IF EXISTS `acl_classes`;
CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_entries`
--

DROP TABLE IF EXISTS `acl_entries`;
CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identities`
--

DROP TABLE IF EXISTS `acl_object_identities`;
CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identity_ancestors`
--

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;
CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_security_identities`
--

DROP TABLE IF EXISTS `acl_security_identities`;
CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bands`
--

DROP TABLE IF EXISTS `bands`;
CREATE TABLE `bands` (
  `id` int(11) NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bands`
--

INSERT INTO `bands` (`id`, `facebook_id`, `name`, `genre`, `media`) VALUES
(1, '69116329538', 'The Beatles', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music/17/07/36/mzi.ldnorbao.100x100-75.jpg'),
(2, '109411479084590', 'Tool', 'Hip-Hop/Rap', 'http://is2.mzstatic.com/image/pf/us/r30/Music/47/ca/2f/mzi.qayltypd.100x100-75.jpg'),
(3, '144387547720', 'H-KAYNE', 'Hip-Hop/Rap', 'http://is5.mzstatic.com/image/pf/us/r30/Music4/v4/d2/2a/89/d22a89b1-da65-7321-6105-fc391f7a6776/05099994938050.100x100-75.jpg'),
(4, '277841428975970', 'Kayzo', 'Dance', 'http://is3.mzstatic.com/image/pf/us/r30/Music4/v4/20/33/b1/2033b1a4-9516-6bf4-da4d-ed8606ddc6d4/SSR11.100x100-75.jpg'),
(5, '71760664768', 'Prince Ea', 'Spoken Word', 'http://is5.mzstatic.com/image/pf/us/r30/Music3/v4/c3/e1/5e/c3e15e41-29c6-0cbc-df90-684c8eda63c8/859713396425_cover.100x100-75.jpg'),
(6, '7234204593', 'Mobydick', 'Rock', 'http://is2.mzstatic.com/image/pf/us/r30/Music7/v4/a7/c2/37/a7c237a1-7d66-0eeb-c143-b123c4df7428/dj.rhubuvkd.100x100-75.jpg'),
(7, '116206108413908', 'Robert Plant', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/e2/47/a3/mzi.loloiycx.100x100-75.jpg'),
(8, '517659061606845', 'Twisted', 'R&B/Soul', 'http://is1.mzstatic.com/image/pf/us/r30/Music/y2004/m11/d17/h16/s06.snrfggjm.100x100-75.jpg'),
(9, '5630135837', 'Joss Stone', 'R&B/Soul', 'http://is4.mzstatic.com/image/pf/us/r30/Music1/v4/16/45/5a/16455ab4-c725-fe61-4295-4f8d5a51cbd0/5060186928319_1.100x100-75.jpg'),
(10, '172780842770895', 'Heavy metal des années 80', '', NULL),
(11, '473374319361548', 'Official Motörhead', 'Rock & Roll', NULL),
(12, '165985866829751', 'Audioslave', 'Alternative', 'http://is1.mzstatic.com/image/pf/us/r30/Features/c1/eb/3b/dj.owymmnty.100x100-75.jpg'),
(13, '406098276075144', 'Albert King', 'Blues', 'http://is5.mzstatic.com/image/pf/us/r30/Features/v4/e6/b7/34/e6b7342f-0c75-0d6c-c2c3-61b402bbfe0e/V4HttpAssetRepositoryClient-ticket.vdemctxt.jpg-9038073290925355643.100x100-75.jpg'),
(14, '9721897972', 'Motley Crue', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music4/v4/e2/e2/de/e2e2de3b-2a59-00a0-6646-8c0537f85a31/dj.ilrneisu.100x100-75.jpg'),
(15, '6187954123', 'Sigur Rós', 'Alternative', 'http://is4.mzstatic.com/image/pf/us/r30/Music/v4/f6/ed/71/f6ed71ce-0f3f-44bd-f36e-e0d488c68b89/Valtari_iTunes.100x100-75.jpg'),
(16, '22476490672', 'Daft Punk', 'Pop', 'http://is3.mzstatic.com/image/pf/us/r30/Music2/v4/52/aa/50/52aa5008-4934-0c27-a08d-8ebd7d13c030/886443919266.100x100-75.jpg'),
(17, '49423275782', 'Marvin Gaye', 'Pop', 'http://is3.mzstatic.com/image/pf/us/r30/Music5/v4/0b/ec/ff/0becff83-c4b9-707c-f5f0-ae61e7ca0a55/075679925800.100x100-75.jpg'),
(18, '113348062009130', 'Eugene Hütz', 'Brazilian', 'http://is2.mzstatic.com/image/pf/us/r30/Music/33/43/22/mzi.tctceehv.100x100-75.jpg'),
(19, '9037686647', 'Dimebag Darrell', 'Hard Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music3/v4/f4/50/79/f450794c-e302-1032-82d0-1fa44347fe84/889176412202.100x100-75.jpg'),
(20, '65461877454', 'Godsmack', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Features/55/92/c4/dj.iqpdjksb.100x100-75.jpg'),
(21, '361973030536320', 'Luxe Radio Covers.', '', NULL),
(22, '12179710441', 'Ben Harper', 'Singer/Songwriter', 'http://is2.mzstatic.com/image/pf/us/r30/Music4/v4/59/5d/f5/595df54d-9fd3-7f52-986e-341c1869df0f/00724383932054.100x100-75.jpg'),
(23, '6174621106', 'Kings Of Leon', 'Alternative', 'http://is3.mzstatic.com/image/pf/us/r30/Features/48/c1/1b/dj.eunilvua.100x100-75.jpg'),
(24, '112452045457881', 'Ronnie James Dio', 'Metal', 'http://is5.mzstatic.com/image/pf/us/r30/Music/62/b6/e0/mzi.gmkipasv.100x100-75.jpg'),
(25, '6513804564', 'Paolo Nutini', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/f4/3d/26/mzi.hiibcvvr.100x100-75.jpg'),
(26, '131051123745752', 'Windmill', 'Alternative', 'http://is1.mzstatic.com/image/pf/us/r30/Music/4f/74/64/mzi.syhrkmlp.100x100-75.jpg'),
(27, '158788436700', 'Black Label Society', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Features/4d/8e/e3/dj.qmjcmmje.100x100-75.jpg'),
(28, '54602560692', 'C2C', 'Electronic', 'http://is4.mzstatic.com/image/pf/us/r30/Music/v4/f8/d2/2d/f8d22d16-975a-6339-73b7-eea4b22ac7b9/UMG_cvrart_00602537299478_01_RGB72_1500x1500_12UMGIM47214.100x100-75.jpg'),
(29, '104044589630831', 'Rebel Meets Rebel (album)', '', NULL),
(30, '158050734208053', 'La page officielle des fans de la guitare', '', NULL),
(31, '356486257749047', 'Yusuf / Cat Stevens', 'Rock', 'http://is2.mzstatic.com/image/pf/us/r30/Music4/v4/1e/0b/86/1e0b8616-dc23-6c5f-bc69-948d9d06193f/886444794824.100x100-75.jpg'),
(32, '190177721065194', 'VICIOUS VISION', 'Electronic', 'http://is4.mzstatic.com/image/pf/us/r30/Music6/v4/9f/95/5e/9f955eee-3ca6-3868-7962-2264eb86d300/cover.100x100-75.jpg'),
(33, '149427318451128', 'Frank Zappa', 'Rock', 'http://is3.mzstatic.com/image/pf/us/r30/Music/v4/f8/1b/7a/f81b7a09-772e-c9c5-aac9-d8d6fde33f2c/UMG_cvrart_00824302557129_01_RGB72_1200x1200_12UMGIM36720.100x100-75.jpg'),
(34, '9565558573', 'David Gilmour', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music/ca/e1/7c/mzi.rtonxoxf.100x100-75.jpg'),
(35, '135350959839456', 'Dave Mustaine', 'Pop', 'http://is2.mzstatic.com/image/pf/us/r30/Music/a0/6d/95/mzi.xxcbpjby.100x100-75.jpg'),
(36, '109412882418712', 'Joss Stone', 'R&B/Soul', 'http://is4.mzstatic.com/image/pf/us/r30/Music1/v4/16/45/5a/16455ab4-c725-fe61-4295-4f8d5a51cbd0/5060186928319_1.100x100-75.jpg'),
(37, '5361113535', 'Interpol', 'Alternative', 'http://is4.mzstatic.com/image/pf/us/r30/Music/06/b2/74/mzi.acshuihr.100x100-75.jpg'),
(38, '70358446004', '"Weird Al" Yankovic', 'Pop', 'http://is3.mzstatic.com/image/pf/us/r30/Music/8b/06/da/mzi.ybtjtckx.100x100-75.jpg'),
(39, '116796165021821', 'Mogwai', 'Soundtrack', 'http://is5.mzstatic.com/image/pf/us/r30/Music/27/6c/2e/mzi.ogrfbhdu.100x100-75.jpg'),
(40, '16942753414', 'Faith No More', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music/88/fc/aa/mzi.pqhqsipo.100x100-75.jpg'),
(41, '170771849653403', 'Orange Goblin', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/y2005/m07/d18/h13/s06.shlbddkm.100x100-75.jpg'),
(42, '6005919727', 'P.O.D.', 'Rock', 'http://is3.mzstatic.com/image/pf/us/r30/Music/1c/78/8b/mzi.ecffiroe.100x100-75.jpg'),
(43, '6871006677', 'Death Cab for Cutie', 'Alternative', 'http://is3.mzstatic.com/image/pf/us/r30/Music3/v4/0f/e1/0a/0fe10a85-4fb9-c546-5004-ca4ebbb92ec7/075679930682.100x100-75.jpg'),
(44, '20523226680', 'orianthi', 'Pop', 'http://is5.mzstatic.com/image/pf/us/r30/Music/6c/e6/51/mzi.esbcjfaq.100x100-75.jpg'),
(45, '160720447318504', 'Daniel Licht', 'Soundtrack', 'http://is4.mzstatic.com/image/pf/us/r30/Music/31/ce/3d/mzi.sgaciirt.100x100-75.jpg'),
(46, '142093805875875', 'Static-X', 'Rock', 'http://is2.mzstatic.com/image/pf/us/r30/Music/84/62/f2/mzi.tjwjzwmf.100x100-75.jpg'),
(47, '24965126030', 'Yoshida Brothers', 'World', 'http://is5.mzstatic.com/image/pf/us/r30/Music/79/f1/f1/mzi.jpxwnith.100x100-75.jpg'),
(48, '172685102050', 'Iron Maiden', 'Metal', 'http://is2.mzstatic.com/image/pf/us/r30/Music7/v4/b8/92/95/b892959e-7bfb-7fe3-3dd3-24ecec2a6bf4/4050538155013_Cover.100x100-75.jpg'),
(49, '191278307572557', 'Fiona Apple', 'Alternative', 'http://is5.mzstatic.com/image/pf/us/r30/Music/a3/fa/d7/mzi.vuqppvxn.100x100-75.jpg'),
(50, '104052852963939', 'Iron Maiden', 'Metal', 'http://is2.mzstatic.com/image/pf/us/r30/Music7/v4/b8/92/95/b892959e-7bfb-7fe3-3dd3-24ecec2a6bf4/4050538155013_Cover.100x100-75.jpg'),
(51, '116713035013014', 'Mike Portnoy', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/y2005/m06/d14/h20/s06.cxgbmtre.100x100-75.jpg'),
(52, '8232689025', 'Steve Vai', 'Rock', 'http://is2.mzstatic.com/image/pf/us/r30/Music/5a/d4/c0/mzi.edixmxmg.100x100-75.jpg'),
(53, '56848544614', 'Black Sabbath', 'Metal', 'http://is5.mzstatic.com/image/pf/us/r30/Music4/v4/eb/0d/f6/eb0df65b-d936-a781-37b2-a7fe943291a4/075992732727.100x100-75.jpg'),
(54, '158718347511051', 'Enrico Macias', 'French Pop', 'http://is3.mzstatic.com/image/pf/us/r30/Music/94/a5/18/mzi.mbwyldpt.100x100-75.jpg'),
(55, '110816895632762', 'Bush', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music1/v4/f6/fd/4e/f6fd4eed-08f1-81ab-1dc2-08f0df0dc41b/dj.gqzesrtg.100x100-75.jpg'),
(56, '9760369095', 'Jean-Michel Jarre', 'Electronic', 'http://is2.mzstatic.com/image/pf/us/r30/Music/y2004/m12/d08/h23/s06.uhalrpwq.100x100-75.jpg'),
(57, '112766928736508', 'Agent Steel', 'Metal', 'http://is4.mzstatic.com/image/pf/us/r30/Music3/v4/21/f3/5d/21f35da7-05d8-dead-28b1-e797cc68a3fc/886445129168.100x100-75.jpg'),
(58, '6536888869', 'Airbourne', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music/v4/4b/b7/ef/4bb7effb-8e98-9369-f88f-d31b744b02d2/016861767242.100x100-75.jpg'),
(59, '44596990399', 'Alice Cooper', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/y2005/m08/d29/h23/mzi.xkpwiwzu.100x100-75.jpg'),
(60, '27422071944', 'The All-American Rejects', 'Alternative', 'http://is3.mzstatic.com/image/pf/us/r30/Features/4a/64/e9/dj.xnceuoss.100x100-75.jpg'),
(61, '15295887490', 'Alice in Chains', 'Alternative', 'http://is3.mzstatic.com/image/pf/us/r30/Music/ee/8c/ed/mzi.qrsardza.100x100-75.jpg'),
(62, '15082530875', 'Stereophonics', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Features/85/11/d1/dj.phpnavbq.100x100-75.jpg'),
(63, '96557069470', 'Apocalyptica', 'Rock', 'http://is2.mzstatic.com/image/pf/us/r30/Music/d7/ad/c4/mzi.lhzwawzv.100x100-75.jpg'),
(64, '8308655582', 'Avenged Sevenfold', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music/v4/7c/27/29/7c27295b-573c-8c02-b357-240657d7c881/093624942214.100x100-75.jpg'),
(65, '113764745300489', 'Angra', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/26/42/27/mzi.wwozikro.100x100-75.jpg'),
(66, '103704446366593', 'Blackfield', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/y2005/m02/d15/h11/s05.jyxpthrf.100x100-75.jpg'),
(67, '56216285377', 'Ace Frehley', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/v4/6d/9c/ee/6d9ceee1-5cde-e5bf-d9e2-89b2f82bb23d/UMG_cvrart_00602537855742_01_RGB72_1500x1500_14UMGIM16125.100x100-75.jpg'),
(68, '27916307584', 'The Airborne Toxic Event', 'Alternative', 'http://is2.mzstatic.com/image/pf/us/r30/Music/4b/67/3d/mzi.ibhmrtqe.100x100-75.jpg'),
(69, '19963953067', 'BETTY', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/1f/14/fd/mzi.cdzvaxqj.100x100-75.jpg'),
(70, '8746730308', 'The Black Keys', 'Alternative', 'http://is2.mzstatic.com/image/pf/us/r30/Music/5a/36/92/mzi.jznlrlwo.100x100-75.jpg'),
(71, '105612319770', 'Blacklight Barbarian', 'rock', NULL),
(72, '55994179779', 'Blue Oyster Cult', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/12/40/5b/mzi.ilvcijbf.100x100-75.jpg'),
(73, '108552149168574', 'Canned Heat (album)', 'Pop', 'http://is2.mzstatic.com/image/pf/us/r30/Music5/v4/03/fb/0a/03fb0a1f-e234-610c-b63c-a66b8b42d38b/dj.cgnchcho.100x100-75.jpg'),
(74, '108196809208278', 'Brand New Sin', 'Metal', 'http://is5.mzstatic.com/image/pf/us/r30/Music/v4/90/8b/e3/908be3cd-3135-c320-3bc9-ba77bbbce949/Cover.100x100-75.jpg'),
(75, '106492446060516', 'Corrosion Of Conformity', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/84/22/4e/mzi.ciektycd.100x100-75.jpg'),
(76, '29737937983', 'Bruce Hornsby', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Features/2e/cc/b3/dj.rhkauhku.100x100-75.jpg'),
(77, '106273979403575', 'Clutch (album)', 'Rap', 'http://is4.mzstatic.com/image/pf/us/r30/Music5/v4/c6/27/6e/c6276e70-7eab-c0f1-8e51-10d7a56374eb/640901164656.100x100-75.jpg'),
(78, '106025249428334', 'Deep Purple', 'Pop', 'http://is5.mzstatic.com/image/pf/us/r30/Music/y2004/m12/d26/h21/s06.arzyryux.100x100-75.jpg'),
(79, '105614049471805', 'Dio', 'Rock', 'http://is1.mzstatic.com/image/pf/us/r30/Music/61/6b/40/mzi.edhdsmue.100x100-75.jpg'),
(80, '7916037383', 'Disturbed', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music/y2005/m07/d12/h09/s06.neivmedj.100x100-75.jpg'),
(81, '164798226877251', 'Gojira', 'Metal', 'http://is4.mzstatic.com/image/pf/us/r30/Features/v4/a0/3b/9d/a03b9de3-aa72-fe24-4b46-4e767a8b388b/V4HttpAssetRepositoryClient-ticket.myjqcpil.jpg-4359453987889282758.100x100-75.jpg'),
(82, '110904328944159', 'Gorilla Monsoon', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/a6/ce/9b/mzi.sonfwjcw.100x100-75.jpg'),
(83, '106110252753861', 'Eric Johnson', 'Rock', 'http://is2.mzstatic.com/image/pf/us/r30/Music/v4/2c/54/6e/2c546e94-0fdd-b48c-b148-7156ec6a7632/00724359030852.100x100-75.jpg'),
(84, '33870215569', 'The Great Fiction', 'Alternative', 'http://is3.mzstatic.com/image/pf/us/r30/Music/cc/0f/0f/mzi.gniytmnw.100x100-75.jpg'),
(85, '6228338509', 'Hammock', 'Alternative', 'http://is1.mzstatic.com/image/pf/us/r30/Music/v4/2f/9f/7a/2f9f7afc-1ee3-eb65-d348-5471afe27d1f/634457571327.100x100-75.jpg'),
(86, '103696143011420', 'Infected Mushroom', 'Dance', 'http://is4.mzstatic.com/image/pf/us/r30/Music/ab/41/b5/mzi.ocihducn.100x100-75.jpg'),
(87, '305434150156', 'Hurts', 'Pop', 'http://is1.mzstatic.com/image/pf/us/r30/Music/64/be/10/mzi.nwohkzxe.100x100-75.jpg'),
(88, '48402572885', 'Jack Lucan', 'Singer/Songwriter', 'http://is3.mzstatic.com/image/pf/us/r30/Music/39/f2/08/mzi.ciekccma.100x100-75.jpg'),
(89, '106035039427159', 'Cat Stevens', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Features/b5/25/e3/dj.dqmqbpse.100x100-75.jpg'),
(90, '105673092800549', 'Anathema', 'Metal', 'http://is2.mzstatic.com/image/pf/us/r30/Music/v4/b1/2d/09/b12d0937-47bf-a1e1-a6cf-e849fee5ca91/654436025108.100x100-75.jpg'),
(91, '12853613964', 'Judas Priest', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music3/v4/1d/70/84/1d70846d-661f-ce0e-7533-82b0283c5da1/dj.msxviebt.100x100-75.jpg'),
(92, '9012139058', 'Joe Satriani', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/63/d8/8d/mzi.rwutvmgy.100x100-75.jpg'),
(93, '30379242251', 'Kiko Loureiro', 'Rock', 'http://is4.mzstatic.com/image/pf/us/r30/Music4/v4/d9/43/83/d943830a-00db-4ae9-465c-c3adb1e3b117/887158485671_Cover.100x100-75.jpg'),
(94, '9008741434', 'Lamb of God', 'Metal', 'http://is3.mzstatic.com/image/pf/us/r30/Music5/v4/5c/14/a4/5c14a4f1-b7e8-fc8a-c3d5-410392db451c/886445276350.100x100-75.jpg'),
(95, '7709052329', 'Megadeth', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music6/v4/c5/0e/8a/c50e8aa5-903f-ac68-5361-ad17fb4cff59/00724359862057.100x100-75.jpg'),
(96, '6028461107', 'Moby', 'Electronic', 'http://is1.mzstatic.com/image/pf/us/r30/Features/99/48/26/dj.snfubbon.100x100-75.jpg'),
(97, '109691175715354', 'Mondo Generator', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/1d/40/a0/mzi.rdoobtgp.100x100-75.jpg'),
(98, '104054412965080', 'Moloko', 'Dance', 'http://is2.mzstatic.com/image/pf/us/r30/Music/ac/0f/9b/mzi.xmvvwniw.100x100-75.jpg'),
(99, '14054750116', 'Mudvayne', 'Metal', 'http://is1.mzstatic.com/image/pf/us/r30/Music/cb/d8/c1/mzi.ioqkzibz.100x100-75.jpg'),
(100, '107777615916905', 'OSI', 'Rock', 'http://is5.mzstatic.com/image/pf/us/r30/Music/v4/97/7e/61/977e619f-27cc-bc42-edf7-f463954693a6/Cover.100x100-75.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `classification__category`
--

DROP TABLE IF EXISTS `classification__category`;
CREATE TABLE `classification__category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `classification__category`
--

INSERT INTO `classification__category` (`id`, `parent_id`, `context`, `media_id`, `name`, `enabled`, `slug`, `description`, `position`, `created_at`, `updated_at`) VALUES
(1, NULL, 'default', NULL, 'default', 1, 'default', 'default', NULL, '2015-08-07 01:36:06', '2015-08-07 01:36:06'),
(2, NULL, 'sonata_collection', NULL, 'sonata_collection', 1, 'sonata-collection', 'sonata_collection', NULL, '2015-08-07 01:36:14', '2015-08-07 01:36:14'),
(4, NULL, 'News', NULL, 'News', 1, 'news', 'News', NULL, '2015-08-10 17:34:44', '2015-08-10 17:34:44'),
(5, NULL, 'users', NULL, 'users', 1, 'users', 'users', NULL, '2015-08-11 00:24:45', '2015-08-11 00:24:45');

-- --------------------------------------------------------

--
-- Structure de la table `classification__collection`
--

DROP TABLE IF EXISTS `classification__collection`;
CREATE TABLE `classification__collection` (
  `id` int(11) NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `classification__context`
--

DROP TABLE IF EXISTS `classification__context`;
CREATE TABLE `classification__context` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `classification__context`
--

INSERT INTO `classification__context` (`id`, `name`, `enabled`, `created_at`, `updated_at`) VALUES
('default', 'default', 1, '2015-08-07 01:36:06', '2015-08-07 01:36:06'),
('News', 'News', 1, '2015-08-10 17:32:35', '2015-08-10 17:32:35'),
('sonata_collection', 'sonata_collection', 1, '2015-08-07 01:36:14', '2015-08-07 01:36:14'),
('users', 'users', 1, '2015-08-11 00:24:45', '2015-08-11 00:24:45');

-- --------------------------------------------------------

--
-- Structure de la table `classification__tag`
--

DROP TABLE IF EXISTS `classification__tag`;
CREATE TABLE `classification__tag` (
  `id` int(11) NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `downloads`
--

DROP TABLE IF EXISTS `downloads`;
CREATE TABLE `downloads` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `videoID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateDownload` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `downloads`
--

INSERT INTO `downloads` (`id`, `ip`, `videoID`, `dateDownload`) VALUES
(1, '127.0.0.1', '71Ii97Vvlzc', '2015-08-22 23:38:46'),
(2, '127.0.0.1', 'iD2rhdFRehU', '2015-08-23 00:06:55');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_group`
--

DROP TABLE IF EXISTS `fos_user_group`;
CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fos_user_group`
--

INSERT INTO `fos_user_group` (`id`, `name`, `roles`) VALUES
(1, 'Administrateur', 'a:3:{i:0;s:9:"ROLE_USER";i:1;s:10:"ROLE_ADMIN";i:2;s:17:"ROLE_SONATA_ADMIN";}');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user`
--

DROP TABLE IF EXISTS `fos_user_user`;
CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user_group`
--

DROP TABLE IF EXISTS `fos_user_user_group`;
CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `lexik_translation_file`
--

DROP TABLE IF EXISTS `lexik_translation_file`;
CREATE TABLE `lexik_translation_file` (
  `id` int(11) NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `extention` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `lexik_translation_file`
--

INSERT INTO `lexik_translation_file` (`id`, `domain`, `locale`, `extention`, `path`, `hash`) VALUES
(1, 'HWIOAuthBundle', 'en', 'yml', '../vendor/hwi/oauth-bundle/HWI/Bundle/OAuthBundle/Resources/translations', '7cea3ba196718abceb7cb46c19e27022'),
(2, 'FOSUserBundle', 'en', 'yml', '../vendor/friendsofsymfony/user-bundle/Resources/translations', 'd97da25a03ff725d3fc287e9a83224f4'),
(3, 'validators', 'en', 'yml', '../vendor/friendsofsymfony/user-bundle/Resources/translations', '71399f539c5b830a11fd8d06c8ef6088'),
(4, 'SonataCoreBundle', 'en', 'xliff', '../vendor/sonata-project/core-bundle/Resources/translations', '6456ee5f1b52e70dd0a63b7a0c4f87e0'),
(5, 'SonataAdminBundle', 'en', 'xliff', '../vendor/sonata-project/admin-bundle/Resources/translations', '65a311b0107314a7c0d15f69674d1862'),
(6, 'SonataTranslationBundle', 'en', 'xliff', '../vendor/sonata-project/translation-bundle/Resources/translations', 'f385cee5cd918e6a53a6b576ded5957f'),
(7, 'SonataSeoBundle', 'en', 'xliff', '../vendor/sonata-project/seo-bundle/Resources/translations', '5f2951384e783d6990d2980972ca4859'),
(8, 'SonataNotificationBundle', 'en', 'xliff', '../vendor/sonata-project/notification-bundle/Resources/translations', 'd0528f714af52714eaff8d33a45147ba'),
(9, 'SonataPageBundle', 'en', 'xliff', '../vendor/sonata-project/page-bundle/Resources/translations', '3237892257af75581c8abc293bee88e4'),
(10, 'validators', 'en', 'xliff', '../vendor/sonata-project/page-bundle/Resources/translations', 'c9c4bdeb7233cfbc136d5c27eb10e041'),
(11, 'CmfRoutingBundle', 'en', 'xliff', '../vendor/symfony-cmf/routing-bundle/Resources/translations', '427ce6410c2c2e3d88d47f626dac9e21'),
(12, 'LexikTranslationBundle', 'en', 'yml', '../vendor/lexik/translation-bundle/Lexik/Bundle/TranslationBundle/Resources/translations', '79a865dd49d816b7e4521487cce065cf'),
(13, 'IbrowsSonataTranslationBundle', 'en', 'yml', '../vendor/ibrows/sonata-translation-bundle/Resources/translations', '9ab8c63ed1782a67d0b1ea26d78fbfe2'),
(14, 'SonataAdminBundle', 'en', 'yml', '../vendor/ibrows/sonata-translation-bundle/Resources/translations', '91339035da2c564a5c810e9a27c723f4'),
(15, 'FOSUserBundle', 'en', 'xliff', '../vendor/sonata-project/user-bundle/Resources/translations', '208905becc24965538484517719c0720'),
(16, 'SonataUserBundle', 'en', 'xliff', '../vendor/sonata-project/user-bundle/Resources/translations', 'b9793bc3e4e394d42746fed9ad4abc2d'),
(17, 'validators', 'en', 'xlf', '../vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations', '628abd6816f58ff33e553cdce63fd3e8'),
(18, 'validators', 'en', 'xlf', '../vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations', 'c4b8022b823d602fc720a1398bb90802'),
(19, 'security', 'en', 'xlf', '../vendor/symfony/symfony/src/Symfony/Component/Security/Core/Exception/../../Resources/translations', '32707d332023bdeb2b3ea91bfdb8ab52'),
(20, 'messages', 'fr', 'xlf', '../src/YE/SiteBundle/Resources/translations', '65fa2f325b65475bc1dbaf510c2e14bd'),
(21, 'HWIOAuthBundle', 'fr', 'yml', '../vendor/hwi/oauth-bundle/HWI/Bundle/OAuthBundle/Resources/translations', 'a76bad0e799351ec64d55b71ab7536e2'),
(22, 'FOSUserBundle', 'fr', 'yml', '../vendor/friendsofsymfony/user-bundle/Resources/translations', '65e565e7ade7b8f98128dcdd8561a2dc'),
(23, 'validators', 'fr', 'yml', '../vendor/friendsofsymfony/user-bundle/Resources/translations', '52348932fe5c083b70312c35ac83a7d8'),
(24, 'SonataCoreBundle', 'fr', 'xliff', '../vendor/sonata-project/core-bundle/Resources/translations', 'b966e7c06ce0451f1e5b6d2eeb281423'),
(25, 'SonataAdminBundle', 'fr', 'xliff', '../vendor/sonata-project/admin-bundle/Resources/translations', '07be107449c0c0565228afb9a19aa991'),
(26, 'SonataTranslationBundle', 'fr', 'xliff', '../vendor/sonata-project/translation-bundle/Resources/translations', '32789b2cfd28ab653104791ffb46e94b'),
(27, 'SonataSeoBundle', 'fr', 'xliff', '../vendor/sonata-project/seo-bundle/Resources/translations', '8cc33d8277456fd813d71f5997ef9dce'),
(28, 'SonataNotificationBundle', 'fr', 'xliff', '../vendor/sonata-project/notification-bundle/Resources/translations', '4f4a9e335be2f62c8b4a9566036af199'),
(29, 'SonataPageBundle', 'fr', 'xliff', '../vendor/sonata-project/page-bundle/Resources/translations', '6776a0a624fac6a7eeb2ac7588a05bfc'),
(30, 'validators', 'fr', 'xliff', '../vendor/sonata-project/page-bundle/Resources/translations', '3593befd1f3e0090b69267d08ffdd10d'),
(31, 'CmfRoutingBundle', 'fr', 'xliff', '../vendor/symfony-cmf/routing-bundle/Resources/translations', 'd47a2d04fe8920736b1ffaacf4b8066e'),
(32, 'LexikTranslationBundle', 'fr', 'yml', '../vendor/lexik/translation-bundle/Lexik/Bundle/TranslationBundle/Resources/translations', 'e56b385b8deabc4eef2671036a06a248'),
(33, 'FOSUserBundle', 'fr', 'xliff', '../vendor/sonata-project/user-bundle/Resources/translations', '63324b9f732609b9738e1bcb130cb9da'),
(34, 'SonataUserBundle', 'fr', 'xliff', '../vendor/sonata-project/user-bundle/Resources/translations', '7274133654ae0a6e131ce2ea938207c3'),
(35, 'validators', 'fr', 'xlf', '../vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations', '466b3746ee40cc9ae23579f1d84ae8b8'),
(36, 'validators', 'fr', 'xlf', '../vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations', 'af5f508d5bd5160a67e457d724dc7d51'),
(37, 'security', 'fr', 'xlf', '../vendor/symfony/symfony/src/Symfony/Component/Security/Core/Exception/../../Resources/translations', '2a4e20d303de6d5adb964d0f3e0d8485');

-- --------------------------------------------------------

--
-- Structure de la table `lexik_trans_unit`
--

DROP TABLE IF EXISTS `lexik_trans_unit`;
CREATE TABLE `lexik_trans_unit` (
  `id` int(11) NOT NULL,
  `key_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `lexik_trans_unit`
--

INSERT INTO `lexik_trans_unit` (`id`, `key_name`, `domain`, `created_at`, `updated_at`) VALUES
(1, 'header.connecting', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(2, 'header.success', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(3, 'header.register', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(4, 'header.registration_success', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(5, 'connect.confirm.cancel', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(6, 'connect.confirm.submit', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(7, 'connect.confirm.text', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(8, 'connect.registration.cancel', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(9, 'connect.registration.submit', 'HWIOAuthBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(10, 'group.edit.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(11, 'group.show.name', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(12, 'group.new.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(13, 'group.flash.updated', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(14, 'group.flash.created', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(15, 'group.flash.deleted', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(16, 'security.login.username', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(17, 'security.login.password', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(18, 'security.login.remember_me', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(19, 'security.login.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(20, 'profile.show.username', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(21, 'profile.show.email', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(22, 'profile.edit.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(23, 'profile.flash.updated', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(24, 'change_password.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(25, 'change_password.flash.success', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(26, 'registration.check_email', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(27, 'registration.confirmed', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(28, 'registration.back', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(29, 'registration.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(30, 'registration.flash.user_created', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(31, 'registration.email.subject', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(32, 'registration.email.message', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(33, 'resetting.password_already_requested', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(34, 'resetting.check_email', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(35, 'resetting.request.invalid_username', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(36, 'resetting.request.username', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(37, 'resetting.request.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(38, 'resetting.reset.submit', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(39, 'resetting.flash.success', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(40, 'resetting.email.subject', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(41, 'resetting.email.message', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(42, 'layout.logout', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(43, 'layout.login', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(44, 'layout.register', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(45, 'layout.logged_in_as', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(46, 'form.group_name', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(47, 'form.username', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(48, 'form.email', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(49, 'form.current_password', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(50, 'form.password', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(51, 'form.password_confirmation', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(52, 'form.new_password', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(53, 'form.new_password_confirmation', 'FOSUserBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(54, 'fos_user.username.already_used', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(55, 'fos_user.username.blank', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(56, 'fos_user.username.short', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(57, 'fos_user.username.long', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(58, 'fos_user.email.already_used', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(59, 'fos_user.email.blank', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(60, 'fos_user.email.short', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(61, 'fos_user.email.long', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(62, 'fos_user.email.invalid', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(63, 'fos_user.password.blank', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(64, 'fos_user.password.short', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(65, 'fos_user.password.mismatch', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(66, 'fos_user.new_password.blank', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(67, 'fos_user.new_password.short', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(68, 'fos_user.current_password.invalid', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(69, 'fos_user.group.blank', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(70, 'fos_user.group.short', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(71, 'fos_user.group.long', 'validators', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(72, 'link_add', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(73, 'label_type_yes', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(74, 'label_type_no', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(75, 'sonata_core_template_box_file_found_in', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(76, 'label_type_equals', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(77, 'label_type_not_equals', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(78, 'date_range_start', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(79, 'date_range_end', 'SonataCoreBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(80, 'sonata_administration', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(81, 'action_delete', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(82, 'btn_batch', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(83, 'btn_create', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(84, 'btn_create_and_edit_again', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(85, 'btn_create_and_create_a_new_one', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(86, 'btn_create_and_return_to_list', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(87, 'btn_filter', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(88, 'btn_advanced_filters', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(89, 'btn_update', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(90, 'btn_update_and_edit_again', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(91, 'btn_update_and_return_to_list', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(92, 'link_delete', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(93, 'link_action_create', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(94, 'link_action_list', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(95, 'link_action_show', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(96, 'link_action_edit', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(97, 'link_add', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(98, 'link_list', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(99, 'link_reset_filter', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(100, 'title_create', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(101, 'title_dashboard', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(102, 'title_edit', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(103, 'title_list', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(104, 'link_next_pager', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(105, 'link_previous_pager', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(106, 'link_first_pager', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(107, 'link_last_pager', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(108, 'Admin', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(109, 'link_expand', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(110, 'no_result', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(111, 'confirm_msg', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(112, 'action_edit', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(113, 'action_show', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(114, 'all_elements', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(115, 'flash_batch_empty', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(116, 'flash_create_success', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(117, 'flash_create_error', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(118, 'flash_edit_success', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(119, 'flash_edit_error', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(120, 'flash_batch_delete_success', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(121, 'flash_batch_delete_error', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(122, 'flash_delete_error', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(123, 'flash_delete_success', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(124, 'breadcrumb.link_dashboard', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(125, 'title_delete', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(126, 'message_delete_confirmation', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(127, 'btn_delete', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(128, 'title_batch_confirmation', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(129, 'message_batch_confirmation', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(130, 'message_batch_all_confirmation', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(131, 'btn_execute_batch_action', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(132, 'label_type_yes', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(133, 'label_type_no', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(134, 'label_type_contains', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(135, 'label_type_not_contains', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(136, 'label_type_equals', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(137, 'label_type_not_equals', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(138, 'label_type_equal', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(139, 'label_type_greater_equal', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(140, 'label_type_greater_than', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(141, 'label_type_less_equal', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(142, 'label_type_less_than', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(143, 'label_date_type_equal', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(144, 'label_date_type_greater_equal', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(145, 'label_date_type_greater_than', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(146, 'label_date_type_less_equal', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(147, 'label_date_type_less_than', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(148, 'label_date_type_null', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(149, 'label_date_type_not_null', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(150, 'label_date_type_between', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(151, 'label_date_type_not_between', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(152, 'label_filters', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(153, 'delete_or', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(154, 'link_action_history', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(155, 'td_action', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(156, 'td_compare', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(157, 'td_revision', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(158, 'td_timestamp', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(159, 'td_username', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(160, 'td_role', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(161, 'label_view_revision', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(162, 'label_compare_revision', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(163, 'list_results_count_prefix', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(164, 'list_results_count', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(165, 'label_export_download', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(166, 'export_format_json', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(167, 'export_format_xml', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(168, 'export_format_csv', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(169, 'export_format_xls', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(170, 'loading_information', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(171, 'btn_preview', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(172, 'btn_preview_approve', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(173, 'btn_preview_decline', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(174, 'label_per_page', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(175, 'list_select', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(176, 'confirm_exit', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(177, 'link_edit_acl', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(178, 'btn_update_acl', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(179, 'flash_acl_edit_success', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(180, 'link_action_acl', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(181, 'short_object_description_placeholder', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(182, 'title_search_results', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(183, 'search_placeholder', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(184, 'no_results_found', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(185, 'add_new_entry', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(186, 'link_actions', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(187, 'noscript_warning', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(188, 'message_form_group_empty', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(189, 'link_filters', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(190, 'stats_view_more', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(191, 'title_select_subclass', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(192, 'no_subclass_available', 'SonataAdminBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(193, 'sonata_translation', 'SonataTranslationBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(194, 'admin.locale_switcher.tooltip', 'SonataTranslationBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(195, 'sonata_seo_homepage_breadcrumb', 'SonataSeoBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(196, 'sonata_seo_share_by_email', 'SonataSeoBundle', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(197, 'sonata_notification', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(198, 'notifications', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(199, 'breadcrumb.link_message_list', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(200, 'list.label_id', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(201, 'list.label_type', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(202, 'list.label_created_at', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(203, 'list.label_started_at', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(204, 'list.label_completed_at', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(205, 'list.label_get_state_name', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(206, 'show.label_id', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(207, 'show.label_type', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(208, 'show.label_created_at', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(209, 'show.label_started_at', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(210, 'show.label_completed_at', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(211, 'show.label_get_state_name', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(212, 'breadcrumb.link_message_show', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(213, 'show.label_body', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(214, 'filter.label_type', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(215, 'filter.label_state', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(216, 'batch.message_publish', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(217, 'batch.message_cancelled', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(218, 'list.label_restart_count', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(219, 'show.label_restart_count', 'SonataNotificationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(220, 'block', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(221, 'breadcrumb.link_block_batch', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(222, 'breadcrumb.link_block_create', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(223, 'breadcrumb.link_block_delete', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(224, 'breadcrumb.link_block_edit', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(225, 'breadcrumb.link_block_list', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(226, 'breadcrumb.link_block_update', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(227, 'breadcrumb.link_dashboard', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(228, 'breadcrumb.link_page_batch', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(229, 'breadcrumb.link_page_create', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(230, 'breadcrumb.link_page_delete', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(231, 'breadcrumb.link_page_edit', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(232, 'breadcrumb.link_page_list', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(233, 'breadcrumb.link_page_update', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(234, 'breadcrumb.link_snapshot_batch', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(235, 'breadcrumb.link_snapshot_create', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(236, 'breadcrumb.link_snapshot_delete', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(237, 'breadcrumb.link_snapshot_edit', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(238, 'breadcrumb.link_snapshot_list', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(239, 'breadcrumb.link_snapshot_update', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(240, 'btn_save_position', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(241, 'button_create_snapshot', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(242, 'cms', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(243, 'create_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(244, 'create_snapshot', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(245, 'error.404', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(246, 'filter.label_enabled', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(247, 'filter.label_hybrid', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(248, 'filter.label_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(249, 'filter.label_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(250, 'filter.label_route_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(251, 'filter.label_type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(252, 'form.group_advanced_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(253, 'form.group_main_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(254, 'form.group_seo_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(255, 'form.label_children', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(256, 'form.label_custom_url', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(257, 'form.label_decorate', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(258, 'form.label_enabled', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(259, 'form.label_javascript', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(260, 'form.label_meta_description', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(261, 'form.label_meta_keyword', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(262, 'form.label_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(263, 'form.label_parent', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(264, 'form.label_position', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(265, 'form.label_publication_date_end', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(266, 'form.label_publication_date_start', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(267, 'form.label_raw_headers', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(268, 'form.label_settings', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(269, 'form.label_slug', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(270, 'form.label_stylesheet', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(271, 'form.label_target', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(272, 'form.label_template_code', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(273, 'form.label_type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(274, 'form.label_url', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(275, 'form_page.group_advanced_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(276, 'form_page.group_main_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(277, 'form_page.group_seo_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(278, 'header.create_snapshot', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(279, 'header.edit_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(280, 'header.page_is_disabled', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(281, 'header.show_zone', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(282, 'header.view_all_exceptions', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(283, 'header.view_all_pages', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(284, 'help_page_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(285, 'hybrid', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(286, 'list.label_decorate', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(287, 'list.label_enabled', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(288, 'list.label_hybrid', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(289, 'list.label_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(290, 'list.label_position', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(291, 'list.label_publication_date_end', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(292, 'list.label_publication_date_start', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(293, 'list.label_type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(294, 'list.label_updated_at', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(295, 'list.label_url', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(296, 'manage_exception', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(297, 'message_create_snapshot', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(298, 'or_list', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(299, 'page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(300, 'show.label_custom_url', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(301, 'show.label_decorate', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(302, 'show.label_enabled', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(303, 'show.label_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(304, 'show.label_route_name', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(305, 'show.label_slug', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(306, 'sidemenu.link_edit_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(307, 'sidemenu.link_list_blocks', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(308, 'sidemenu.link_list_snapshots', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(309, 'snapshot', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(310, 'sonata_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(311, 'title_create_snapshot', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(312, 'title_list_exceptions', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(313, 'title_page_not_found', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(314, 'toggle_enabled', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(315, 'view_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(316, 'header.switch_user_exit', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(317, 'header.switch_user', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(318, 'header.sonata_admin_dashboard', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(319, 'list.label_is_default', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(320, 'list.label_host', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(321, 'list.label_relative_path', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(322, 'list.label_enabled_to', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(323, 'list.label_enabled_from', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(324, 'form.label_is_default', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(325, 'form.label_host', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(326, 'form.label_relative_path', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(327, 'form.label_enabled_to', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(328, 'form.label_enabled_from', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(329, 'breadcrumb.link_site_list', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(330, 'breadcrumb.link_site_show', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(331, 'breadcrumb.link_site_create', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(332, 'breadcrumb.link_site_edit', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(333, 'list.label_create_snapshots', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(334, 'link_create_snapshots', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(335, 'title_create_snapshots', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(336, 'message_create_snapshots', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(337, 'button_create_snapshots', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(338, 'breadcrumb.link_site_history', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(339, 'form.label_site', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(340, 'list.label_site', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(341, 'site', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(342, 'show.label_is_default', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(343, 'show.label_host', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(344, 'show.label_relative_path', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(345, 'show.label_enabled_from', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(346, 'show.label_enabled_to', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(347, 'list.label_locale', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(348, 'show.label_locale', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(349, 'form.label_locale', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(350, 'form_site.label_seo', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(351, 'form.label_title', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(352, 'form.label_meta_keywords', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(353, 'show.label_title', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(354, 'show.label_meta_description', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(355, 'show.label_meta_keywords', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(356, 'form_site.label_general', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(357, 'filter.label_site', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(358, 'filter.label_edited', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(359, 'list.label_edited', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(360, 'show.label_site', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(361, 'show.label_edited', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(362, 'breadcrumb.link_page_show', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(363, 'form.label_page_alias', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(364, 'show.label_page_alias', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(365, 'list.label_page_alias', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(366, 'filter.label_page_alias', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(367, 'title_page_redirected', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(368, 'message_page_redirected', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(369, 'filter.label_parent', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(370, 'title_select_block_type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(371, 'form.field_group_general', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(372, 'form.field_group_options', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(373, 'flash_snapshots_created_success', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(374, 'title_message_create_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(375, 'message_create_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(376, 'sidemenu.link_compose_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(377, 'sidemenu.link_list_pages', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(378, 'sidemenu.link_pages_tree', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(379, 'breadcrumb.link_page_tree', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(380, 'pages.tree_site_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(381, 'pages.tree_mode', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(382, 'pages.list_mode', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(383, 'header.compose_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(384, 'no_type_available', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(385, 'page.compose_page', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(386, 'page.orphan_containers', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(387, 'composer.remove', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(388, 'composer.remove.confirm', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(389, 'yes', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(390, 'cancel', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(391, 'loading', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(392, 'composer.block.add.type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(393, 'notice', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(394, 'blocks', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(395, 'composer.disable', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(396, 'composer.enable', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(397, 'shared_block', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(398, 'breadcrumb.link_shared_block_batch', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(399, 'breadcrumb.link_shared_block_create', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(400, 'breadcrumb.link_shared_block_delete', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(401, 'breadcrumb.link_shared_block_edit', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(402, 'breadcrumb.link_shared_block_list', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(403, 'breadcrumb.link_shared_block_update', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(404, 'sidemenu.link_list_shared_blocks', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(405, 'title_select_shared_block_type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(406, 'composer.shared_block.add.type', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(407, 'pages.compose_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(408, 'pages.edited_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(409, 'page.compose_template_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(410, 'page.compose_blocks_label', 'SonataPageBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(411, 'error.uniq_url', 'validators', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(412, 'dashboard.cmf', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(413, 'dashboard.label_routing', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(414, 'dashboard.label_redirect_routing', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(415, 'breadcrumb.link_route_list', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(416, 'breadcrumb.link_route_create', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(417, 'breadcrumb.link_route_edit', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(418, 'breadcrumb.link_route_delete', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(419, 'breadcrumb.link_redirect_route_list', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(420, 'breadcrumb.link_redirect_route_create', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(421, 'breadcrumb.link_redirect_route_edit', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(422, 'breadcrumb.link_redirect_route_delete', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(423, 'filter.label_name', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(424, 'list.label_path', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(425, 'form.group_general', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(426, 'form.group_advanced', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(427, 'form.label_parent', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(428, 'form.label_name', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(429, 'form.label_variable_pattern', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(430, 'form.help_variable_pattern', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(431, 'form.label_content', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(432, 'form.label_defaults', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(433, 'form.label_options', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(434, 'form.label_route_name', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(435, 'form.label_uri', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(436, 'form.label_route_target', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(437, 'form.group_routes', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(438, 'form.label_routes', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(439, 'form.label_add_locale_pattern', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(440, 'form.label_add_format_pattern', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(441, 'form.label_add_trailing_slash', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(442, 'form.help_options', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(443, 'admin.menu_frontend_link_caption', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(444, 'admin.menu_frontend_link_title', 'CmfRoutingBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(445, 'translations.invalidate_cache', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(446, 'translations.domain', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(447, 'translations.key', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(448, 'translations.grid_caption', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(449, 'translations.cache_removed', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(450, 'translations.new_translation', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(451, 'translations.add_translation', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(452, 'translations.save', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(453, 'translations.save_add', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(454, 'translations.page_title', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(455, 'translations.show_hide_columns', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(456, 'translations.toggle_all_columns', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(457, 'translations.save_row', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(458, 'translations.succesfully_updated', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(459, 'translations.succesfully_added', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(460, 'translations.update_failed', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(461, 'translations.back_to_list', 'LexikTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(462, 'show_non_translated_only', 'IbrowsSonataTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(463, 'group.translation', 'IbrowsSonataTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(464, 'translation.title', 'IbrowsSonataTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(465, 'action.clear_cache', 'IbrowsSonataTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(466, 'action.upload', 'IbrowsSonataTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(467, 'batch.download', 'IbrowsSonataTranslationBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(468, 'translations.cache_removed', 'SonataAdminBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(469, 'translations.flash_batch_download_error', 'SonataAdminBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(470, 'translations.flash_batch_download_success', 'SonataAdminBundle', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(766, 'title_user_registration', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(767, 'title_user_account', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(768, 'title_user_edit_profile', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(769, 'title_user_authentication', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(770, 'sonata_user_submit', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(771, 'breadcrumb.link_user_list', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(772, 'breadcrumb.link_user_create', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(773, 'breadcrumb.link_user_edit', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(774, 'breadcrumb.link_user_delete', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(775, 'breadcrumb.link_group_list', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(776, 'breadcrumb.link_group_create', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(777, 'breadcrumb.link_group_edit', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(778, 'users', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(779, 'groups', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(780, 'switch_user_exit', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(781, 'switch_user', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(782, 'user_block_logout', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(783, 'user_block_profile', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(784, 'form.label_username', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(785, 'form.label_email', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(786, 'form.label_plain_password', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(787, 'form.label_groups', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(788, 'form.label_roles', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(789, 'form.label_locked', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(790, 'form.label_expired', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(791, 'form.label_enabled', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(792, 'form.label_credentials_expired', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(793, 'form.label_created_at', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(794, 'form.label_last_login', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(795, 'form.label_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(796, 'filter.label_username', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(797, 'filter.label_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(798, 'filter.label_email', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(799, 'filter.label_locked', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(800, 'filter.label_id', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(801, 'filter.label_enabled', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(802, 'filter.label_groups', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(803, 'filter.label_created_at', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(804, 'filter.label_last_login', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(805, 'filter.label_firstname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(806, 'filter.label_lastname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(807, 'list.label_username', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(808, 'list.label_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(809, 'list.label_email', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(810, 'list.label_groups', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(811, 'list.label_locked', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(812, 'list.label_enabled', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(813, 'list.label_created_at', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(814, 'list.label_last_login', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(815, 'list.label_impersonating', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(816, 'list.label_roles', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42');
INSERT INTO `lexik_trans_unit` (`id`, `key_name`, `domain`, `created_at`, `updated_at`) VALUES
(817, 'list.label_firstname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(818, 'list.label_lastname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(819, 'label_two_step_code', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(820, 'message_two_step_code_help', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(821, 'label_two_step_code_error', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(822, 'sonata_user', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(823, 'General', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(824, 'Management', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(825, 'field.label_roles_editable', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(826, 'form.label_date_of_birth', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(827, 'form.label_firstname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(828, 'form.label_lastname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(829, 'form.label_website', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(830, 'form.label_biography', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(831, 'form.label_gender', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(832, 'form.label_locale', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(833, 'form.label_timezone', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(834, 'form.label_phone', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(835, 'form.label_facebook_uid', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(836, 'form.label_facebook_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(837, 'form.label_twitter_uid', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(838, 'form.label_twitter_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(839, 'form.label_gplus_uid', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(840, 'form.label_gplus_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(841, 'form.label_token', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(842, 'form.label_two_step_verification_code', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(843, 'show.label_username', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(844, 'show.label_email', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(845, 'show.label_groups', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(846, 'show.label_date_of_birth', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(847, 'show.label_firstname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(848, 'show.label_lastname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(849, 'show.label_website', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(850, 'show.label_biography', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(851, 'show.label_gender', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(852, 'show.label_locale', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(853, 'show.label_timezone', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(854, 'show.label_phone', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(855, 'show.label_facebook_uid', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(856, 'show.label_facebook_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(857, 'show.label_twitter_uid', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(858, 'show.label_twitter_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(859, 'show.label_gplus_uid', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(860, 'show.label_gplus_name', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(861, 'show.label_token', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(862, 'show.label_two_step_verification_code', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(863, 'show.label_created_at', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(864, 'show.label_last_login', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(865, 'breadcrumb.link_user_show', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(866, 'gender_unknown', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(867, 'gender_female', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(868, 'gender_male', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(869, 'sonata_profile_title', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(870, 'link_show_profile', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(871, 'link_edit_profile', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(872, 'title_user_edit_authentication', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(873, 'link_edit_authentication', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(874, 'label_profile_gender', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(875, 'label_profile_firstname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(876, 'label_profile_lastname', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(877, 'label_profile_website', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(878, 'label_profile_dob', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(879, 'label_profile_biography', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(880, 'label_profile_locale', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(881, 'label_profile_timezone', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(882, 'label_profile_phone', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(883, 'profile.flash.updated', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(884, 'sonata_change_password_link', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(885, 'link_register', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(886, 'link_login', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(887, 'link_logout', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(888, 'registration.flash.user_created', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(889, 'forgotten_password', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(890, 'sonata_user_profile_breadcrumb_index', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(891, 'sonata_user_profile_breadcrumb_edit', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(892, 'sonata_user_already_authenticated', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(893, 'security.login.username', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(894, 'security.login.password', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(895, 'form.username', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(896, 'form.email', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(897, 'form.password', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(898, 'form.password_confirmation', 'SonataUserBundle', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(899, 'This value should be false.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(900, 'This value should be true.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(901, 'This value should be of type {{ type }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(902, 'This value should be blank.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(903, 'The value you selected is not a valid choice.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(904, 'You must select at least {{ limit }} choice.|You must select at least {{ limit }} choices.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(905, 'You must select at most {{ limit }} choice.|You must select at most {{ limit }} choices.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(906, 'One or more of the given values is invalid.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(907, 'This field was not expected.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(908, 'This field is missing.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(909, 'This value is not a valid date.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(910, 'This value is not a valid datetime.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(911, 'This value is not a valid email address.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(912, 'The file could not be found.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(913, 'The file is not readable.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(914, 'The file is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(915, 'The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(916, 'This value should be {{ limit }} or less.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(917, 'This value is too long. It should have {{ limit }} character or less.|This value is too long. It should have {{ limit }} characters or less.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(918, 'This value should be {{ limit }} or more.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(919, 'This value is too short. It should have {{ limit }} character or more.|This value is too short. It should have {{ limit }} characters or more.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(920, 'This value should not be blank.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(921, 'This value should not be null.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(922, 'This value should be null.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(923, 'This value is not valid.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(924, 'This value is not a valid time.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(925, 'This value is not a valid URL.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(926, 'The two values should be equal.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(927, 'The file is too large. Allowed maximum size is {{ limit }} {{ suffix }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(928, 'The file is too large.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(929, 'The file could not be uploaded.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(930, 'This value should be a valid number.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(931, 'This file is not a valid image.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(932, 'This is not a valid IP address.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(933, 'This value is not a valid language.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(934, 'This value is not a valid locale.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(935, 'This value is not a valid country.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(936, 'This value is already used.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(937, 'The size of the image could not be detected.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(938, 'The image width is too big ({{ width }}px). Allowed maximum width is {{ max_width }}px.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(939, 'The image width is too small ({{ width }}px). Minimum width expected is {{ min_width }}px.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(940, 'The image height is too big ({{ height }}px). Allowed maximum height is {{ max_height }}px.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(941, 'The image height is too small ({{ height }}px). Minimum height expected is {{ min_height }}px.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(942, 'This value should be the user''s current password.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(943, 'This value should have exactly {{ limit }} character.|This value should have exactly {{ limit }} characters.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(944, 'The file was only partially uploaded.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(945, 'No file was uploaded.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(946, 'No temporary folder was configured in php.ini.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(947, 'Cannot write temporary file to disk.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(948, 'A PHP extension caused the upload to fail.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(949, 'This collection should contain {{ limit }} element or more.|This collection should contain {{ limit }} elements or more.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(950, 'This collection should contain {{ limit }} element or less.|This collection should contain {{ limit }} elements or less.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(951, 'This collection should contain exactly {{ limit }} element.|This collection should contain exactly {{ limit }} elements.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(952, 'Invalid card number.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(953, 'Unsupported card type or invalid card number.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(954, 'This is not a valid International Bank Account Number (IBAN).', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(955, 'This value is not a valid ISBN-10.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(956, 'This value is not a valid ISBN-13.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(957, 'This value is neither a valid ISBN-10 nor a valid ISBN-13.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(958, 'This value is not a valid ISSN.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(959, 'This value is not a valid currency.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(960, 'This value should be equal to {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(961, 'This value should be greater than {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(962, 'This value should be greater than or equal to {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(963, 'This value should be identical to {{ compared_value_type }} {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(964, 'This value should be less than {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(965, 'This value should be less than or equal to {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(966, 'This value should not be equal to {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(967, 'This value should not be identical to {{ compared_value_type }} {{ compared_value }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(968, 'The image ratio is too big ({{ ratio }}). Allowed maximum ratio is {{ max_ratio }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(969, 'The image ratio is too small ({{ ratio }}). Minimum ratio expected is {{ min_ratio }}.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(970, 'The image is square ({{ width }}x{{ height }}px). Square images are not allowed.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(971, 'The image is landscape oriented ({{ width }}x{{ height }}px). Landscape oriented images are not allowed.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(972, 'The image is portrait oriented ({{ width }}x{{ height }}px). Portrait oriented images are not allowed.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(973, 'An empty file is not allowed.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(974, 'The host could not be resolved.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(975, 'This value does not match the expected {{ charset }} charset.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(976, 'This form should not contain extra fields.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(977, 'The uploaded file was too large. Please try to upload a smaller file.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(978, 'The CSRF token is invalid. Please try to resubmit the form.', 'validators', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(979, 'An authentication exception occurred.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(980, 'Authentication credentials could not be found.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(981, 'Authentication request could not be processed due to a system problem.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(982, 'Invalid credentials.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(983, 'Cookie has already been used by someone else.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(984, 'Not privileged to request the resource.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(985, 'Invalid CSRF token.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(986, 'Digest nonce has expired.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(987, 'No authentication provider found to support the authentication token.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(988, 'No session available, it either timed out or cookies are not enabled.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(989, 'No token could be found.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(990, 'Username could not be found.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(991, 'Account has expired.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(992, 'Credentials have expired.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(993, 'Account is disabled.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(994, 'Account is locked.', 'security', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(996, 'Symfony2 is great', 'messages', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(997, 'form_not_available', 'SonataAdminBundle', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(998, 'Profile', 'SonataUserBundle', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(999, 'Social', 'SonataUserBundle', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1000, 'Security', 'SonataUserBundle', '2015-08-06 02:24:41', '2015-08-06 02:24:41');

-- --------------------------------------------------------

--
-- Structure de la table `lexik_trans_unit_translations`
--

DROP TABLE IF EXISTS `lexik_trans_unit_translations`;
CREATE TABLE `lexik_trans_unit_translations` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `trans_unit_id` int(11) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1403 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `lexik_trans_unit_translations`
--

INSERT INTO `lexik_trans_unit_translations` (`id`, `file_id`, `trans_unit_id`, `locale`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'en', 'Connecting', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(2, 1, 2, 'en', 'Successfully connected the account ''%name%''!', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(3, 1, 3, 'en', 'Register with the account ''%name%''', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(4, 1, 4, 'en', 'Successfully registered and connected the account ''%username%''!', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(5, 1, 5, 'en', 'Cancel', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(6, 1, 6, 'en', 'Connect account', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(7, 1, 7, 'en', 'Are you sure that you want to connect your %service% account ''%name%'' to your current account?', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(8, 1, 8, 'en', 'Cancel', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(9, 1, 9, 'en', 'Register account', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(10, 2, 10, 'en', 'Update group', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(11, 2, 11, 'en', 'Group name', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(12, 2, 12, 'en', 'Create group', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(13, 2, 13, 'en', 'The group has been updated', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(14, 2, 14, 'en', 'The group has been created', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(15, 2, 15, 'en', 'The group has been deleted', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(16, 2, 16, 'en', 'Username:', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(17, 2, 17, 'en', 'Password:', '2015-08-06 02:13:27', '2015-08-06 02:47:26'),
(18, 2, 18, 'en', 'Remember me', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(19, 2, 19, 'en', 'Login', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(20, 2, 20, 'en', 'Username', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(21, 2, 21, 'en', 'Email', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(22, 2, 22, 'en', 'Update', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(23, 2, 23, 'en', 'The profile has been updated', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(24, 2, 24, 'en', 'Change password', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(25, 2, 25, 'en', 'The password has been changed', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(26, 2, 26, 'en', 'An email has been sent to %email%. It contains an activation link you must click to activate your account.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(27, 2, 27, 'en', 'Congrats %username%, your account is now activated.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(28, 2, 28, 'en', 'Back to the originating page.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(29, 2, 29, 'en', 'Register', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(30, 2, 30, 'en', 'The user has been created successfully', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(31, 2, 31, 'en', 'Welcome %username%!', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(32, 2, 32, 'en', 'Hello %username%!\n\nTo finish activating your account - please visit %confirmationUrl%\n\nRegards,\nthe Team.\n', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(33, 2, 33, 'en', 'The password for this user has already been requested within the last 24 hours.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(34, 2, 34, 'en', 'An email has been sent to %email%. It contains a link you must click to reset your password.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(35, 2, 35, 'en', 'The username or email address "%username%" does not exist.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(36, 2, 36, 'en', 'Username or email address:', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(37, 2, 37, 'en', 'Reset password', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(38, 2, 38, 'en', 'Change password', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(39, 2, 39, 'en', 'The password has been reset successfully', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(40, 2, 40, 'en', 'Reset Password', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(41, 2, 41, 'en', 'Hello %username%!\n\nTo reset your password - please visit %confirmationUrl%\n\nRegards,\nthe Team.\n', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(42, 2, 42, 'en', 'Logout', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(43, 2, 43, 'en', 'Login', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(44, 2, 44, 'en', 'Register', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(45, 2, 45, 'en', 'Logged in as %username%', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(46, 2, 46, 'en', 'Group name:', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(54, 3, 54, 'en', 'The username is already used', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(55, 3, 55, 'en', 'Please enter a username', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(56, 3, 56, 'en', '[-Inf,Inf]The username is too short', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(57, 3, 57, 'en', '[-Inf,Inf]The username is too long', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(58, 3, 58, 'en', 'The email is already used', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(59, 3, 59, 'en', 'Please enter an email', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(60, 3, 60, 'en', '[-Inf,Inf]The email is too short', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(61, 3, 61, 'en', '[-Inf,Inf]The email is too long', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(62, 3, 62, 'en', 'The email is not valid', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(63, 3, 63, 'en', 'Please enter a password', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(64, 3, 64, 'en', '[-Inf,Inf]The password is too short', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(65, 3, 65, 'en', 'The entered passwords don''t match', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(66, 3, 66, 'en', 'Please enter a new password', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(67, 3, 67, 'en', '[-Inf,Inf]The new password is too short', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(68, 3, 68, 'en', 'The entered password is invalid', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(69, 3, 69, 'en', 'Please enter a name', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(70, 3, 70, 'en', '[-Inf,Inf]The name is too short', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(71, 3, 71, 'en', '[-Inf,Inf]The name is too long', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(72, 4, 72, 'en', 'Add new', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(73, 4, 73, 'en', 'yes', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(74, 4, 74, 'en', 'no', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(75, 4, 75, 'en', 'This file can be found in', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(76, 4, 76, 'en', 'is equal to', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(77, 4, 77, 'en', 'is not equal to', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(78, 4, 78, 'en', 'From', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(79, 4, 79, 'en', 'To', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(80, 5, 80, 'en', 'Administration', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(81, 5, 81, 'en', 'Delete', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(82, 5, 82, 'en', 'OK', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(83, 5, 83, 'en', 'Create', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(84, 5, 84, 'en', 'Create', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(85, 5, 85, 'en', 'Create and add another', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(86, 5, 86, 'en', 'Create and return to list', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(87, 5, 87, 'en', 'Filter', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(88, 5, 88, 'en', 'Advanced filters', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(89, 5, 89, 'en', 'Update', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(90, 5, 90, 'en', 'Update', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(91, 5, 91, 'en', 'Update and close', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(92, 5, 92, 'en', 'Delete', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(93, 5, 93, 'en', 'Add new', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(94, 5, 94, 'en', 'Return to list', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(95, 5, 95, 'en', 'Show', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(96, 5, 96, 'en', 'Edit', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(97, 5, 97, 'en', 'Add new', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(98, 5, 98, 'en', 'List', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(99, 5, 99, 'en', 'Reset', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(100, 5, 100, 'en', 'Create', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(101, 5, 101, 'en', 'Dashboard', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(102, 5, 102, 'en', 'Edit "%name%"', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(103, 5, 103, 'en', 'List', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(104, 5, 104, 'en', 'Next', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(105, 5, 105, 'en', 'Previous', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(106, 5, 106, 'en', 'First', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(107, 5, 107, 'en', 'Last', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(108, 5, 108, 'en', 'Admin', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(109, 5, 109, 'en', 'expand/collapse', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(110, 5, 110, 'en', 'No result', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(111, 5, 111, 'en', 'Are you sure ?', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(112, 5, 112, 'en', 'Edit', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(113, 5, 113, 'en', 'Show', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(114, 5, 114, 'en', 'All elements', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(115, 5, 115, 'en', 'Action aborted. No items were selected.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(116, 5, 116, 'en', 'Item "%name%" has been successfully created.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(117, 5, 117, 'en', 'An error has occurred during the creation of item "%name%".', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(118, 5, 118, 'en', 'Item "%name%" has been successfully updated.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(119, 5, 119, 'en', 'An error has occurred during update of item "%name%".', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(120, 5, 120, 'en', 'Selected items have been successfully deleted.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(121, 5, 121, 'en', 'An Error has occurred during selected items deletion.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(122, 5, 122, 'en', 'An Error has occurred during deletion of item "%name%".', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(123, 5, 123, 'en', 'Item "%name%" has been deleted successfully.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(124, 5, 124, 'en', '<i class="fa fa-home"></i>', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(125, 5, 125, 'en', 'Confirm deletion', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(126, 5, 126, 'en', 'Are you sure you want to delete the selected "%object%" element?', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(127, 5, 127, 'en', 'Yes, delete', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(128, 5, 128, 'en', 'Confirm batch action ''%action%''', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(129, 5, 129, 'en', 'Are you sure you want to confirm this action and execute it for the selected element?|Are you sure you want to confirm this action and execute it for the %count% selected elements?', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(130, 5, 130, 'en', 'Are you sure you want to confirm this action and execute it for all the elements?', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(131, 5, 131, 'en', 'Yes, execute', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(132, 5, 132, 'en', 'yes', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(133, 5, 133, 'en', 'no', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(134, 5, 134, 'en', 'contains', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(135, 5, 135, 'en', 'does not contain', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(136, 5, 136, 'en', 'is equal to', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(137, 5, 137, 'en', 'is not equal to', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(138, 5, 138, 'en', '=', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(139, 5, 139, 'en', '>=', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(140, 5, 140, 'en', '>', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(141, 5, 141, 'en', '<=', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(142, 5, 142, 'en', '<', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(143, 5, 143, 'en', '=', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(144, 5, 144, 'en', '>=', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(145, 5, 145, 'en', '>', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(146, 5, 146, 'en', '<=', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(147, 5, 147, 'en', '<', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(148, 5, 148, 'en', 'is empty', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(149, 5, 149, 'en', 'is not empty', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(150, 5, 150, 'en', 'between', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(151, 5, 151, 'en', 'not between', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(152, 5, 152, 'en', 'Filters', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(153, 5, 153, 'en', 'or', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(154, 5, 154, 'en', 'Revisions', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(155, 5, 155, 'en', 'Action', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(156, 5, 156, 'en', 'Compare', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(157, 5, 157, 'en', 'Revisions', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(158, 5, 158, 'en', 'Date', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(159, 5, 159, 'en', 'Author', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(160, 5, 160, 'en', 'Role', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(161, 5, 161, 'en', 'View Revision', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(162, 5, 162, 'en', 'Compare revision', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(163, 5, 163, 'en', 'at least', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(164, 5, 164, 'en', '1 result|%count% results', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(165, 5, 165, 'en', 'Download', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(166, 5, 166, 'en', 'JSON', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(167, 5, 167, 'en', 'XML', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(168, 5, 168, 'en', 'CSV', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(169, 5, 169, 'en', 'XLS', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(170, 5, 170, 'en', 'Loading information…', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(171, 5, 171, 'en', 'Preview', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(172, 5, 172, 'en', 'Approve', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(173, 5, 173, 'en', 'Decline', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(174, 5, 174, 'en', 'Per page', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(175, 5, 175, 'en', 'Select', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(176, 5, 176, 'en', 'You have unsaved changes.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(177, 5, 177, 'en', 'Edit ACL', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(178, 5, 178, 'en', 'Update ACL', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(179, 5, 179, 'en', 'ACL has been successfuly updated.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(180, 5, 180, 'en', 'ACL', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(181, 5, 181, 'en', 'No selection', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(182, 5, 182, 'en', 'Search results: %query%', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(183, 5, 183, 'en', 'Search', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(184, 5, 184, 'en', 'no result found', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(185, 5, 185, 'en', 'add new entry', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(186, 5, 186, 'en', 'Actions', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(187, 5, 187, 'en', 'Javascript is disabled in your web browser. Some features will not work properly.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(188, 5, 188, 'en', 'No fields available.', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(189, 5, 189, 'en', 'Filters', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(190, 5, 190, 'en', 'View more', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(191, 5, 191, 'en', 'Select object type', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(192, 5, 192, 'en', 'No object types available', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(193, 6, 193, 'en', 'Translation', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(194, 6, 194, 'en', 'Switch language', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(195, 7, 195, 'en', 'Home', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(196, 7, 196, 'en', 'Share by email', '2015-08-06 02:13:27', '2015-08-06 02:13:27'),
(197, 8, 197, 'en', 'Notification', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(198, 8, 198, 'en', 'Notifications', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(199, 8, 199, 'en', 'Messages', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(200, 8, 200, 'en', 'Id', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(201, 8, 201, 'en', 'Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(202, 8, 202, 'en', 'Created At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(203, 8, 203, 'en', 'Started At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(204, 8, 204, 'en', 'Completed At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(205, 8, 205, 'en', 'State', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(206, 8, 206, 'en', 'Id', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(207, 8, 207, 'en', 'Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(208, 8, 208, 'en', 'Created At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(209, 8, 209, 'en', 'Started At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(210, 8, 210, 'en', 'Completed At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(211, 8, 211, 'en', 'State', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(212, 8, 212, 'en', 'Message', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(213, 8, 213, 'en', 'Payload', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(214, 8, 214, 'en', 'Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(215, 8, 215, 'en', 'State', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(216, 8, 216, 'en', 'Publish messages', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(217, 8, 217, 'en', 'Cancel messages', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(218, 8, 218, 'en', 'Restart #', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(219, 8, 219, 'en', 'Restart #', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(220, 9, 220, 'en', 'Block', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(221, 9, 221, 'en', 'Batch', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(222, 9, 222, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(223, 9, 223, 'en', 'Delete', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(224, 9, 224, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(225, 9, 225, 'en', 'Blocks List', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(226, 9, 226, 'en', 'Update', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(227, 9, 227, 'en', 'Dashboard', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(228, 9, 228, 'en', 'Batch', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(229, 9, 229, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(230, 9, 230, 'en', 'Delete', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(231, 9, 231, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(232, 9, 232, 'en', 'Pages List', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(233, 9, 233, 'en', 'Update', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(234, 9, 234, 'en', 'Batch', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(235, 9, 235, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(236, 9, 236, 'en', 'Delete', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(237, 9, 237, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(238, 9, 238, 'en', 'List', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(239, 9, 239, 'en', 'Update', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(240, 9, 240, 'en', 'Save Position', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(241, 9, 241, 'en', 'Create a publication', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(242, 9, 242, 'en', 'cms', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(243, 9, 243, 'en', 'Create the page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(244, 9, 244, 'en', 'Create a publication', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(245, 9, 245, 'en', 'Error 404', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(246, 9, 246, 'en', 'Enabled', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(247, 9, 247, 'en', 'Page Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(248, 9, 248, 'en', 'Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(249, 9, 249, 'en', 'Page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(250, 9, 250, 'en', 'Route Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(251, 9, 251, 'en', 'Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(252, 9, 252, 'en', 'Advanced', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(253, 9, 253, 'en', 'General', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(254, 9, 254, 'en', 'SEO', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(255, 9, 255, 'en', 'Children', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(256, 9, 256, 'en', 'Custom URL', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(257, 9, 257, 'en', 'Decorate', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(258, 9, 258, 'en', 'Enabled', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(259, 9, 259, 'en', 'Javascript', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(260, 9, 260, 'en', 'Meta Description', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(261, 9, 261, 'en', 'Meta Keyword', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(262, 9, 262, 'en', 'Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(263, 9, 263, 'en', 'Parent', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(264, 9, 264, 'en', 'Position', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(265, 9, 265, 'en', 'Publication end date', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(266, 9, 266, 'en', 'Publication start date', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(267, 9, 267, 'en', 'Raw Headers', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(268, 9, 268, 'en', 'Settings', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(269, 9, 269, 'en', 'Slug', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(270, 9, 270, 'en', 'Stylesheet', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(271, 9, 271, 'en', 'Target', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(272, 9, 272, 'en', 'Template', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(273, 9, 273, 'en', 'Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(274, 9, 274, 'en', 'Url', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(275, 9, 275, 'en', 'Advanced', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(276, 9, 276, 'en', 'Main', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(277, 9, 277, 'en', 'SEO', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(278, 9, 278, 'en', 'Create Publication', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(279, 9, 279, 'en', 'Edit Page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(280, 9, 280, 'en', 'Is Disabled', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(281, 9, 281, 'en', 'Show Zone', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(282, 9, 282, 'en', 'See all errors', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(283, 9, 283, 'en', 'View All Page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(284, 9, 284, 'en', 'The page name (used as the page title)', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(285, 9, 285, 'en', 'hybrid', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(286, 9, 286, 'en', 'Decorate', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(287, 9, 287, 'en', 'Enabled', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(288, 9, 288, 'en', 'Page Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(289, 9, 289, 'en', 'Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(290, 9, 290, 'en', 'Position', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(291, 9, 291, 'en', 'Publication end date', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(292, 9, 292, 'en', 'Publication start date', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(293, 9, 293, 'en', 'Type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(294, 9, 294, 'en', 'Updated At', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(295, 9, 295, 'en', 'Url', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(296, 9, 296, 'en', 'The %code% page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(297, 9, 297, 'en', 'Create a page snapshot', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(298, 9, 298, 'en', 'or', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(299, 9, 299, 'en', 'Pages', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(300, 9, 300, 'en', 'Custom Url', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(301, 9, 301, 'en', 'Decorate', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(302, 9, 302, 'en', 'Enabled', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(303, 9, 303, 'en', 'Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(304, 9, 304, 'en', 'Route Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(305, 9, 305, 'en', 'Slug', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(306, 9, 306, 'en', 'Edit page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(307, 9, 307, 'en', 'Block List', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(308, 9, 308, 'en', 'Publications', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(309, 9, 309, 'en', 'Publications', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(310, 9, 310, 'en', 'Pages', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(311, 9, 311, 'en', 'Create a publication', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(312, 9, 312, 'en', 'Error pages to manage:', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(313, 9, 313, 'en', 'The page does not exist', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(314, 9, 314, 'en', 'Toggle enabled', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(315, 9, 315, 'en', 'View page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(316, 9, 316, 'en', 'Exit impersonating mode', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(317, 9, 317, 'en', 'Impersonate User', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(318, 9, 318, 'en', 'Dashboard', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(319, 9, 319, 'en', 'Is Default', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(320, 9, 320, 'en', 'Host', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(321, 9, 321, 'en', 'Relative Path', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(322, 9, 322, 'en', 'Enabled To', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(323, 9, 323, 'en', 'Enabled From', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(324, 9, 324, 'en', 'Is Default', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(325, 9, 325, 'en', 'Host', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(326, 9, 326, 'en', 'Relative Path', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(327, 9, 327, 'en', 'Enabled To', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(328, 9, 328, 'en', 'Enabled From', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(329, 9, 329, 'en', 'Site List', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(330, 9, 330, 'en', 'Show', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(331, 9, 331, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(332, 9, 332, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(333, 9, 333, 'en', 'Snapshots', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(334, 9, 334, 'en', 'Create snapshots', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(335, 9, 335, 'en', 'Create snapshots', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(336, 9, 336, 'en', 'Click on the create button to setup new snapshots for website : %site%', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(337, 9, 337, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(338, 9, 338, 'en', 'History', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(339, 9, 339, 'en', 'Site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(340, 9, 340, 'en', 'Site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(341, 9, 341, 'en', 'Site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(342, 9, 342, 'en', 'Is Default', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(343, 9, 343, 'en', 'Host', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(344, 9, 344, 'en', 'Relative Path', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(345, 9, 345, 'en', 'Enabled From', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(346, 9, 346, 'en', 'Enabled To', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(347, 9, 347, 'en', 'Locale', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(348, 9, 348, 'en', 'Locale', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(349, 9, 349, 'en', 'Locale', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(350, 9, 350, 'en', 'SEO', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(351, 9, 351, 'en', 'Title', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(352, 9, 352, 'en', 'Meta Keywords', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(353, 9, 353, 'en', 'Title', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(354, 9, 354, 'en', 'Meta Description', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(355, 9, 355, 'en', 'Meta Keywords', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(356, 9, 356, 'en', 'General', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(357, 9, 357, 'en', 'Site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(358, 9, 358, 'en', 'Edited', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(359, 9, 359, 'en', 'Edited', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(360, 9, 360, 'en', 'Site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(361, 9, 361, 'en', 'Edited', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(362, 9, 362, 'en', 'Page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(363, 9, 363, 'en', 'Technical Alias', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(364, 9, 364, 'en', 'Technical Alias', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(365, 9, 365, 'en', 'Technical Alias', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(366, 9, 366, 'en', 'Technical Alias', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(367, 9, 367, 'en', 'Internal page redirection', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(368, 9, 368, 'en', '\n             This page is configured to redirect the end user to another page. In order\n             to configure it, the redirect has been blocked for editor only. <br />\n             <br />\n             Please click here to follow the redirection: <a href="%url%">%url%</a>.\n          ', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(369, 9, 369, 'en', 'Parent', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(370, 9, 370, 'en', 'Select a block type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(371, 9, 371, 'en', 'General', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(372, 9, 372, 'en', 'Options', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(373, 9, 373, 'en', 'Snapshots were successfully created.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(374, 9, 374, 'en', 'Please select the targeted site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(375, 9, 375, 'en', 'Before creating a new page, you need to select the website where the page will be located.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(376, 9, 376, 'en', 'Composer (beta)', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(377, 9, 377, 'en', 'Pages list', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(378, 9, 378, 'en', 'Tree view', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(379, 9, 379, 'en', 'Tree', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(380, 9, 380, 'en', 'Pages for site', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(381, 9, 381, 'en', 'Tree view', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(382, 9, 382, 'en', 'List view', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(383, 9, 383, 'en', 'Compose', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(384, 9, 384, 'en', 'No type available', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(385, 9, 385, 'en', 'Compose page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(386, 9, 386, 'en', 'Orphan containers', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(387, 9, 387, 'en', 'remove', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(388, 9, 388, 'en', 'confirm delete ?', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(389, 9, 389, 'en', 'yes', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(390, 9, 390, 'en', 'cancel', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(391, 9, 391, 'en', 'loading', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(392, 9, 392, 'en', 'add block of type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(393, 9, 393, 'en', 'notice', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(394, 9, 394, 'en', 'blocks', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(395, 9, 395, 'en', 'Disable', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(396, 9, 396, 'en', 'Enable', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(397, 9, 397, 'en', 'Shared Blocks', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(398, 9, 398, 'en', 'Batch', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(399, 9, 399, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(400, 9, 400, 'en', 'Delete', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(401, 9, 401, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(402, 9, 402, 'en', 'Shared Blocks list', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(403, 9, 403, 'en', 'Update', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(404, 9, 404, 'en', 'Shared Block list', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(405, 9, 405, 'en', 'Select a block type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(406, 9, 406, 'en', 'add block of type', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(407, 9, 407, 'en', 'compose', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(408, 9, 408, 'en', 'edited', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(409, 9, 409, 'en', 'template', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(410, 9, 410, 'en', '{0} blocks|{1} block|[2,Inf] blocks', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(411, 10, 411, 'en', 'The URL ''%url%'' is already associated with another page', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(412, 11, 412, 'en', 'Symfony CMF', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(413, 11, 413, 'en', 'Route', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(414, 11, 414, 'en', 'Redirect Route', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(415, 11, 415, 'en', 'Routes', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(416, 11, 416, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(417, 11, 417, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(418, 11, 418, 'en', 'Delete', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(419, 11, 419, 'en', 'Redirect Routes', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(420, 11, 420, 'en', 'Create', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(421, 11, 421, 'en', 'Edit', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(422, 11, 422, 'en', 'Delete', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(423, 11, 423, 'en', 'Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(424, 11, 424, 'en', 'URL', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(425, 11, 425, 'en', 'General', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(426, 11, 426, 'en', 'Advanced', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(427, 11, 427, 'en', 'Parent', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(428, 11, 428, 'en', 'Last URL part', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(429, 11, 429, 'en', 'Variable pattern', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(430, 11, 430, 'en', 'A pattern in the format {variable}/{more}... The fields are passed to the controller if it declares them as arguments.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(431, 11, 431, 'en', 'Content', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(432, 11, 432, 'en', 'Defaults', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(433, 11, 433, 'en', 'Options', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(434, 11, 434, 'en', 'Name', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(435, 11, 435, 'en', 'URI', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(436, 11, 436, 'en', 'Route target', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(437, 11, 437, 'en', 'Routes', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(438, 11, 438, 'en', 'Routes', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(439, 11, 439, 'en', 'Add locale pattern', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(440, 11, 440, 'en', 'Add format pattern', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(441, 11, 441, 'en', 'Add trailing slash', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(442, 11, 442, 'en', '\n            \n            Locale: Prepend locale to route like /{locale}/your/route<br/>\n            Format: Append format to route like /your/route.{format}. Default format is ''html''<br/>\n            Slash: Append a trailing slash to route like /your/route/\n            \n        ', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(443, 11, 443, 'en', 'Frontend', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(444, 11, 444, 'en', 'Open frontend view in new tab', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(445, 12, 445, 'en', 'Invalidate cache', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(446, 12, 446, 'en', 'Domain', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(447, 12, 447, 'en', 'Key', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(448, 12, 448, 'en', 'Translations list', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(449, 12, 449, 'en', 'Cache files has been removed', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(450, 12, 450, 'en', 'New translation', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(451, 12, 451, 'en', 'Add a translation', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(452, 12, 452, 'en', 'Save', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(453, 12, 453, 'en', 'Save and Add', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(454, 12, 454, 'en', 'Translations', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(455, 12, 455, 'en', 'Show/Hide columns', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(456, 12, 456, 'en', 'Show/Hide all columns', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(457, 12, 457, 'en', 'Save current row', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(458, 12, 458, 'en', 'Translations for key #%id% has been successfully updated.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(459, 12, 459, 'en', 'Translation has been successfully added.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(460, 12, 460, 'en', 'Fail to update translations for key #%id%.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(461, 12, 461, 'en', 'Back', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(462, 13, 462, 'en', 'Shown non translated only', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(463, 13, 463, 'en', 'Translation', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(464, 13, 464, 'en', 'Translation', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(465, 13, 465, 'en', 'Clear Cache', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(466, 13, 466, 'en', 'Upload Translationfile', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(467, 13, 467, 'en', 'Download', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(468, 14, 468, 'en', 'Cache has been successfully removed', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(469, 14, 469, 'en', 'Download could not be created.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(470, 14, 470, 'en', 'Download successful.', '2015-08-06 02:13:28', '2015-08-06 02:13:28'),
(471, 2, 47, 'en', 'Username', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(472, 2, 48, 'en', 'Email', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(473, 2, 50, 'en', 'Password', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(474, 2, 51, 'en', 'Verification', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(475, 2, 52, 'en', 'New password', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(476, 2, 49, 'en', 'Current password', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(477, 2, 53, 'en', 'Verification', '2015-08-06 02:14:58', '2015-08-06 02:25:00'),
(478, 16, 766, 'en', 'Register', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(479, 16, 767, 'en', 'User Profile', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(480, 16, 768, 'en', 'Edit', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(481, 16, 769, 'en', 'Authentication', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(482, 16, 770, 'en', 'Submit', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(483, 16, 771, 'en', 'Users', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(484, 16, 772, 'en', 'Create', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(485, 16, 773, 'en', 'Edit', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(486, 16, 774, 'en', 'Delete', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(487, 16, 775, 'en', 'Groups', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(488, 16, 776, 'en', 'Create', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(489, 16, 777, 'en', 'Edit', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(490, 16, 778, 'en', 'Users', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(491, 16, 779, 'en', 'Groups', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(492, 16, 780, 'en', 'Exit impersonating mode', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(493, 16, 781, 'en', 'Impersonate User', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(494, 16, 782, 'en', 'Logout', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(495, 16, 783, 'en', 'Profile', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(496, 16, 784, 'en', 'Username', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(497, 16, 785, 'en', 'E-Mail-Address', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(498, 16, 786, 'en', 'Plain password', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(499, 16, 787, 'en', 'Groups', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(500, 16, 788, 'en', 'Roles', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(501, 16, 789, 'en', 'Locked', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(502, 16, 790, 'en', 'Expired', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(503, 16, 791, 'en', 'Enabled', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(504, 16, 792, 'en', 'Credentials expired', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(505, 16, 793, 'en', 'Created at', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(506, 16, 794, 'en', 'Last login', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(507, 16, 795, 'en', 'Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(508, 16, 796, 'en', 'Username', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(509, 16, 797, 'en', 'Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(510, 16, 798, 'en', 'E-Mail-Address', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(511, 16, 799, 'en', 'Locked', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(512, 16, 800, 'en', 'ID', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(513, 16, 801, 'en', 'Active', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(514, 16, 802, 'en', 'Groups', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(515, 16, 803, 'en', 'Created at', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(516, 16, 804, 'en', 'Last login', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(517, 16, 805, 'en', 'Firstname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(518, 16, 806, 'en', 'Lastname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(519, 16, 807, 'en', 'Username', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(520, 16, 808, 'en', 'Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(521, 16, 809, 'en', 'E-Mail-Address', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(522, 16, 810, 'en', 'Groups', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(523, 16, 811, 'en', 'Locked', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(524, 16, 812, 'en', 'Enabled', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(525, 16, 813, 'en', 'Created at', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(526, 16, 814, 'en', 'Last login', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(527, 16, 815, 'en', 'Impersonate User', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(528, 16, 816, 'en', 'Roles', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(529, 16, 817, 'en', 'Firstname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(530, 16, 818, 'en', 'Lastname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(531, 16, 819, 'en', 'Two-step verification', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(532, 16, 820, 'en', 'Enter the verification code generated by your mobile application.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(533, 16, 821, 'en', 'The verification code is not valid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(534, 16, 822, 'en', 'Users', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(535, 16, 823, 'en', 'General', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(536, 16, 824, 'en', 'Management', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(537, 16, 825, 'en', 'Roles', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(538, 16, 826, 'en', 'Date of birth', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(539, 16, 827, 'en', 'Firstname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(540, 16, 828, 'en', 'Lastname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(541, 16, 829, 'en', 'Website', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(542, 16, 830, 'en', 'Biography', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(543, 16, 831, 'en', 'Gender', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(544, 16, 832, 'en', 'Locale', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(545, 16, 833, 'en', 'Timezone', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(546, 16, 834, 'en', 'Phone', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(547, 16, 835, 'en', 'Facebook Uid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(548, 16, 836, 'en', 'Facebook Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(549, 16, 837, 'en', 'Twitter Uid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(550, 16, 838, 'en', 'Twitter Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(551, 16, 839, 'en', 'Google+ Uid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(552, 16, 840, 'en', 'Google+ Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(553, 16, 841, 'en', 'Token', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(554, 16, 842, 'en', 'Two Step Verification Code', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(555, 16, 843, 'en', 'Username', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(556, 16, 844, 'en', 'Email', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(557, 16, 845, 'en', 'Groups', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(558, 16, 846, 'en', 'Date of birth', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(559, 16, 847, 'en', 'Firstname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(560, 16, 848, 'en', 'Lastname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(561, 16, 849, 'en', 'Website', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(562, 16, 850, 'en', 'Biography', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(563, 16, 851, 'en', 'Gender', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(564, 16, 852, 'en', 'Locale', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(565, 16, 853, 'en', 'Timezone', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(566, 16, 854, 'en', 'Phone', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(567, 16, 855, 'en', 'Facebook Uid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(568, 16, 856, 'en', 'Facebook Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(569, 16, 857, 'en', 'Twitter Uid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(570, 16, 858, 'en', 'Twitter Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(571, 16, 859, 'en', 'Google+ Uid', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(572, 16, 860, 'en', 'Google+ Name', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(573, 16, 861, 'en', 'Token', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(574, 16, 862, 'en', 'Two Step Verification Code', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(575, 16, 863, 'en', 'Created at', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(576, 16, 864, 'en', 'Last login', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(577, 16, 865, 'en', 'Show', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(578, 16, 866, 'en', 'unknown', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(579, 16, 867, 'en', 'female', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(580, 16, 868, 'en', 'male', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(581, 16, 869, 'en', 'Dashboard', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(582, 16, 870, 'en', 'Dashboard', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(583, 16, 871, 'en', 'Profile', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(584, 16, 872, 'en', 'Authentication', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(585, 16, 873, 'en', 'Authentication', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(586, 16, 874, 'en', 'Gender', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(587, 16, 875, 'en', 'Firstname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(588, 16, 876, 'en', 'Lastname', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(589, 16, 877, 'en', 'Website', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(590, 16, 878, 'en', 'Date of birth', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(591, 16, 879, 'en', 'Biography', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(592, 16, 880, 'en', 'Locale', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(593, 16, 881, 'en', 'Timezone', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(594, 16, 882, 'en', 'Phone', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(595, 16, 883, 'en', 'Your profile has been updated.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(596, 16, 884, 'en', 'Change your password', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(597, 16, 885, 'en', 'Register', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(598, 16, 886, 'en', 'Log In', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(599, 16, 887, 'en', 'Log out', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(600, 16, 888, 'en', 'Your account has been created successfully!', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(601, 16, 889, 'en', 'Forgotten password?', '2015-08-06 02:17:42', '2015-08-06 02:17:42');
INSERT INTO `lexik_trans_unit_translations` (`id`, `file_id`, `trans_unit_id`, `locale`, `content`, `created_at`, `updated_at`) VALUES
(602, 16, 890, 'en', 'Dashboard', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(603, 16, 891, 'en', 'Edit profile', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(604, 16, 892, 'en', 'You are already logged in', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(605, 16, 893, 'en', 'Username', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(606, 16, 894, 'en', 'Password', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(607, 16, 895, 'en', 'Username', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(608, 16, 896, 'en', 'Email', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(609, 16, 897, 'en', 'Password', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(610, 16, 898, 'en', 'Verification', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(611, 17, 899, 'en', 'This value should be false.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(612, 17, 900, 'en', 'This value should be true.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(613, 17, 901, 'en', 'This value should be of type {{ type }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(614, 17, 902, 'en', 'This value should be blank.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(615, 17, 903, 'en', 'The value you selected is not a valid choice.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(616, 17, 904, 'en', 'You must select at least {{ limit }} choice.|You must select at least {{ limit }} choices.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(617, 17, 905, 'en', 'You must select at most {{ limit }} choice.|You must select at most {{ limit }} choices.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(618, 17, 906, 'en', 'One or more of the given values is invalid.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(619, 17, 907, 'en', 'This field was not expected.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(620, 17, 908, 'en', 'This field is missing.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(621, 17, 909, 'en', 'This value is not a valid date.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(622, 17, 910, 'en', 'This value is not a valid datetime.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(623, 17, 911, 'en', 'This value is not a valid email address.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(624, 17, 912, 'en', 'The file could not be found.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(625, 17, 913, 'en', 'The file is not readable.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(626, 17, 914, 'en', 'The file is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(627, 17, 915, 'en', 'The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(628, 17, 916, 'en', 'This value should be {{ limit }} or less.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(629, 17, 917, 'en', 'This value is too long. It should have {{ limit }} character or less.|This value is too long. It should have {{ limit }} characters or less.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(630, 17, 918, 'en', 'This value should be {{ limit }} or more.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(631, 17, 919, 'en', 'This value is too short. It should have {{ limit }} character or more.|This value is too short. It should have {{ limit }} characters or more.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(632, 17, 920, 'en', 'This value should not be blank.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(633, 17, 921, 'en', 'This value should not be null.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(634, 17, 922, 'en', 'This value should be null.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(635, 17, 923, 'en', 'This value is not valid.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(636, 17, 924, 'en', 'This value is not a valid time.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(637, 17, 925, 'en', 'This value is not a valid URL.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(638, 17, 926, 'en', 'The two values should be equal.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(639, 17, 927, 'en', 'The file is too large. Allowed maximum size is {{ limit }} {{ suffix }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(640, 17, 928, 'en', 'The file is too large.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(641, 17, 929, 'en', 'The file could not be uploaded.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(642, 17, 930, 'en', 'This value should be a valid number.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(643, 17, 931, 'en', 'This file is not a valid image.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(644, 17, 932, 'en', 'This is not a valid IP address.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(645, 17, 933, 'en', 'This value is not a valid language.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(646, 17, 934, 'en', 'This value is not a valid locale.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(647, 17, 935, 'en', 'This value is not a valid country.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(648, 17, 936, 'en', 'This value is already used.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(649, 17, 937, 'en', 'The size of the image could not be detected.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(650, 17, 938, 'en', 'The image width is too big ({{ width }}px). Allowed maximum width is {{ max_width }}px.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(651, 17, 939, 'en', 'The image width is too small ({{ width }}px). Minimum width expected is {{ min_width }}px.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(652, 17, 940, 'en', 'The image height is too big ({{ height }}px). Allowed maximum height is {{ max_height }}px.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(653, 17, 941, 'en', 'The image height is too small ({{ height }}px). Minimum height expected is {{ min_height }}px.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(654, 17, 942, 'en', 'This value should be the user''s current password.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(655, 17, 943, 'en', 'This value should have exactly {{ limit }} character.|This value should have exactly {{ limit }} characters.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(656, 17, 944, 'en', 'The file was only partially uploaded.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(657, 17, 945, 'en', 'No file was uploaded.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(658, 17, 946, 'en', 'No temporary folder was configured in php.ini.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(659, 17, 947, 'en', 'Cannot write temporary file to disk.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(660, 17, 948, 'en', 'A PHP extension caused the upload to fail.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(661, 17, 949, 'en', 'This collection should contain {{ limit }} element or more.|This collection should contain {{ limit }} elements or more.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(662, 17, 950, 'en', 'This collection should contain {{ limit }} element or less.|This collection should contain {{ limit }} elements or less.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(663, 17, 951, 'en', 'This collection should contain exactly {{ limit }} element.|This collection should contain exactly {{ limit }} elements.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(664, 17, 952, 'en', 'Invalid card number.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(665, 17, 953, 'en', 'Unsupported card type or invalid card number.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(666, 17, 954, 'en', 'This is not a valid International Bank Account Number (IBAN).', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(667, 17, 955, 'en', 'This value is not a valid ISBN-10.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(668, 17, 956, 'en', 'This value is not a valid ISBN-13.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(669, 17, 957, 'en', 'This value is neither a valid ISBN-10 nor a valid ISBN-13.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(670, 17, 958, 'en', 'This value is not a valid ISSN.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(671, 17, 959, 'en', 'This value is not a valid currency.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(672, 17, 960, 'en', 'This value should be equal to {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(673, 17, 961, 'en', 'This value should be greater than {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(674, 17, 962, 'en', 'This value should be greater than or equal to {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(675, 17, 963, 'en', 'This value should be identical to {{ compared_value_type }} {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(676, 17, 964, 'en', 'This value should be less than {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(677, 17, 965, 'en', 'This value should be less than or equal to {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(678, 17, 966, 'en', 'This value should not be equal to {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(679, 17, 967, 'en', 'This value should not be identical to {{ compared_value_type }} {{ compared_value }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(680, 17, 968, 'en', 'The image ratio is too big ({{ ratio }}). Allowed maximum ratio is {{ max_ratio }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(681, 17, 969, 'en', 'The image ratio is too small ({{ ratio }}). Minimum ratio expected is {{ min_ratio }}.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(682, 17, 970, 'en', 'The image is square ({{ width }}x{{ height }}px). Square images are not allowed.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(683, 17, 971, 'en', 'The image is landscape oriented ({{ width }}x{{ height }}px). Landscape oriented images are not allowed.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(684, 17, 972, 'en', 'The image is portrait oriented ({{ width }}x{{ height }}px). Portrait oriented images are not allowed.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(685, 17, 973, 'en', 'An empty file is not allowed.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(686, 17, 974, 'en', 'The host could not be resolved.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(687, 17, 975, 'en', 'This value does not match the expected {{ charset }} charset.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(688, 18, 976, 'en', 'This form should not contain extra fields.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(689, 18, 977, 'en', 'The uploaded file was too large. Please try to upload a smaller file.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(690, 18, 978, 'en', 'The CSRF token is invalid. Please try to resubmit the form.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(691, 19, 979, 'en', 'An authentication exception occurred.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(692, 19, 980, 'en', 'Authentication credentials could not be found.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(693, 19, 981, 'en', 'Authentication request could not be processed due to a system problem.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(694, 19, 982, 'en', 'Invalid credentials.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(695, 19, 983, 'en', 'Cookie has already been used by someone else.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(696, 19, 984, 'en', 'Not privileged to request the resource.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(697, 19, 985, 'en', 'Invalid CSRF token.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(698, 19, 986, 'en', 'Digest nonce has expired.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(699, 19, 987, 'en', 'No authentication provider found to support the authentication token.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(700, 19, 988, 'en', 'No session available, it either timed out or cookies are not enabled.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(701, 19, 989, 'en', 'No token could be found.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(702, 19, 990, 'en', 'Username could not be found.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(703, 19, 991, 'en', 'Account has expired.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(704, 19, 992, 'en', 'Credentials have expired.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(705, 19, 993, 'en', 'Account is disabled.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(706, 19, 994, 'en', 'Account is locked.', '2015-08-06 02:17:42', '2015-08-06 02:17:42'),
(708, 20, 996, 'fr', 'J''aime Symfony2', '2015-08-06 02:24:39', '2015-08-13 15:36:53'),
(709, 21, 1, 'fr', 'Connexion en cours', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(710, 21, 2, 'fr', 'Connexion réussie avec le compte ''%name%'' !', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(711, 21, 3, 'fr', 'S''inscrire avec le compte ''%name%''', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(712, 21, 4, 'fr', 'Enregistrement et connexion du compte ''%username%'' réussis !', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(713, 21, 5, 'fr', 'Annuler', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(714, 21, 6, 'fr', 'Connecter le compte', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(715, 21, 7, 'fr', 'Êtes-vous sûr de vouloir connecter votre compte %service% avec votre compte ''%name%'' ?', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(716, 21, 8, 'fr', 'Annuler', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(717, 21, 9, 'fr', 'Enregistrer le compte', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(718, 22, 10, 'fr', 'Mettre à jour le groupe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(719, 22, 11, 'fr', 'Nom du groupe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(720, 22, 12, 'fr', 'Créer le groupe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(721, 22, 13, 'fr', 'Le groupe a été mis à jour', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(722, 22, 14, 'fr', 'Le groupe a été créé', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(723, 22, 15, 'fr', 'Le groupe a été supprimé', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(724, 22, 16, 'fr', 'Nom d''utilisateur :', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(725, 22, 17, 'fr', 'Mot de passe :', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(726, 22, 18, 'fr', 'Se souvenir de moi', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(727, 22, 19, 'fr', 'Connexion', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(728, 22, 20, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(729, 22, 21, 'fr', 'Adresse e-mail', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(730, 22, 22, 'fr', 'Mettre à jour', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(731, 22, 23, 'fr', 'Le profil a été mis à jour', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(732, 22, 24, 'fr', 'Modifier le mot de passe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(733, 22, 25, 'fr', 'Le mot de passe a été modifié', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(734, 22, 26, 'fr', 'Un e-mail a été envoyé à l''adresse %email%. Il contient un lien d''activation sur lequel il vous faudra cliquer afin d''activer votre compte.', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(735, 22, 27, 'fr', 'Félicitations %username%, votre compte est maintenant activé.', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(736, 22, 28, 'fr', 'Retour à la page d''origine.', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(737, 22, 29, 'fr', 'Enregistrer', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(738, 22, 30, 'fr', 'L''utilisateur a été créé avec succès', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(739, 22, 31, 'fr', 'Bienvenue %username% !', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(740, 22, 32, 'fr', 'Bonjour %username% !\n\nPour valider votre compte utilisateur, merci de vous rendre sur %confirmationUrl%\n\nCordialement,\nL''équipe.\n', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(741, 22, 33, 'fr', 'Un nouveau mot de passe a déjà été demandé pour cet utilisateur dans les dernières 24 heures.', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(742, 22, 34, 'fr', 'Un e-mail a été envoyé à l''adresse %email%. Il contient un lien sur lequel il vous faudra cliquer afin de réinitialiser votre mot de passe.', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(743, 22, 35, 'fr', 'Le nom d''utilisateur ou l''adresse e-mail "%username%" n''existe pas.', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(744, 22, 36, 'fr', 'Nom d''utilisateur ou adresse e-mail :', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(745, 22, 37, 'fr', 'Réinitialiser le mot de passe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(746, 22, 38, 'fr', 'Modifier le mot de passe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(747, 22, 39, 'fr', 'Le mot de passe a été réinitialisé avec succès', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(748, 22, 40, 'fr', 'Réinitialisation de votre mot de passe', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(749, 22, 41, 'fr', 'Bonjour %username% !\n\nPour réinitialiser votre mot de passe, merci de vous rendre sur %confirmationUrl%\n\nCordialement,\nL''équipe.\n', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(750, 22, 42, 'fr', 'Déconnexion', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(751, 22, 43, 'fr', 'Connexion', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(752, 22, 44, 'fr', 'Inscription', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(753, 22, 45, 'fr', 'Connecté en tant que %username%', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(754, 22, 46, 'fr', 'Nom du groupe :', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(755, 22, 47, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(756, 22, 48, 'fr', 'Adresse e-mail', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(757, 22, 49, 'fr', 'Mot de passe actuel', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(758, 22, 50, 'fr', 'Mot de passe', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(759, 22, 51, 'fr', 'Vérification', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(760, 22, 52, 'fr', 'Mot de passe', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(761, 22, 53, 'fr', 'Vérification', '2015-08-06 02:24:39', '2015-08-06 02:25:00'),
(762, 23, 54, 'fr', 'Le nom d''utilisateur est déjà utilisé', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(763, 23, 55, 'fr', 'Entrez un nom d''utilisateur s''il vous plait', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(764, 23, 56, 'fr', '[-Inf,Inf]Le nom d''utilisateur est trop court', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(765, 23, 57, 'fr', '[-Inf,Inf]Le nom d''utilisateur est trop long', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(766, 23, 58, 'fr', 'L''adresse e-mail est déjà utilisée', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(767, 23, 59, 'fr', 'Entrez une adresse e-mail s''il vous plait', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(768, 23, 60, 'fr', '[-Inf,Inf]L''adresse e-mail est trop courte', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(769, 23, 61, 'fr', '[-Inf,Inf]L''adresse e-mail est trop longue', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(770, 23, 62, 'fr', 'L''adresse e-mail est invalide', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(771, 23, 63, 'fr', 'Entrez un mot de passe s''il vous plait', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(772, 23, 64, 'fr', '[-Inf,Inf]Le mot de passe est trop court', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(773, 23, 65, 'fr', 'Les deux mots de passe ne sont pas identiques', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(774, 23, 66, 'fr', 'Entrez un nouveau mot de passe s''il vous plait', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(775, 23, 67, 'fr', '[-Inf,Inf]Le nouveau mot de passe est trop court', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(776, 23, 68, 'fr', 'Le mot de passe est invalide', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(777, 23, 69, 'fr', 'Entrez un nom s''il vous plait', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(778, 23, 70, 'fr', '[-Inf,Inf]Le nom est trop court', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(779, 23, 71, 'fr', '[-Inf,Inf]Le nom est trop long', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(780, 24, 72, 'fr', 'Ajouter', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(781, 24, 73, 'fr', 'oui', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(782, 24, 74, 'fr', 'non', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(783, 24, 75, 'fr', 'Ce fichier est localisé à l''emplacement', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(784, 24, 76, 'fr', 'Est égal à', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(785, 24, 77, 'fr', 'Ne contient pas', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(786, 24, 78, 'fr', 'Du', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(787, 24, 79, 'fr', 'Au', '2015-08-06 02:24:39', '2015-08-06 02:24:39'),
(788, 25, 80, 'fr', 'Administration', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(789, 25, 81, 'fr', 'Supprimer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(790, 25, 82, 'fr', 'OK', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(791, 25, 83, 'fr', 'Créer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(792, 25, 84, 'fr', 'Créer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(793, 25, 85, 'fr', 'Créer et ajouter', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(794, 25, 86, 'fr', 'Créer et retourner à la liste', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(795, 25, 87, 'fr', 'Filtrer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(796, 25, 88, 'fr', 'Filtres avancés', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(797, 25, 89, 'fr', 'Mettre à jour', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(798, 25, 90, 'fr', 'Mettre à jour', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(799, 25, 91, 'fr', 'Mettre à jour et fermer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(800, 25, 92, 'fr', 'Supprimer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(801, 25, 93, 'fr', 'Ajouter', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(802, 25, 94, 'fr', 'Retourner à la liste', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(803, 25, 95, 'fr', 'Afficher', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(804, 25, 96, 'fr', 'Editer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(805, 25, 97, 'fr', 'Ajouter', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(806, 25, 98, 'fr', 'Liste', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(807, 25, 99, 'fr', 'Effacer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(808, 25, 100, 'fr', 'Créer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(809, 25, 101, 'fr', 'Tableau de bord', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(810, 25, 102, 'fr', 'Éditer "%name%"', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(811, 25, 103, 'fr', 'Liste', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(812, 25, 104, 'fr', 'Suivant', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(813, 25, 105, 'fr', 'Précédent', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(814, 25, 106, 'fr', 'Première', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(815, 25, 107, 'fr', 'Dernière', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(816, 25, 108, 'fr', 'Admin', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(817, 25, 109, 'fr', 'Étendre/Réduire', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(818, 25, 110, 'fr', 'Aucun résultat', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(819, 25, 111, 'fr', 'Êtes-vous sûr ?', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(820, 25, 112, 'fr', 'Éditer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(821, 25, 113, 'fr', 'Afficher', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(822, 25, 114, 'fr', 'Tous les éléments', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(823, 25, 115, 'fr', 'Action interrompue. Aucun élément n''a été séléctionné.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(824, 25, 116, 'fr', 'L''élément "%name%" a été créé avec succès.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(825, 25, 117, 'fr', 'Une erreur est intervenue lors de la création de l''élément "%name%".', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(826, 25, 118, 'fr', 'L''élément "%name%" a été mis à jour avec succès.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(827, 25, 119, 'fr', 'Une erreur est intervenue lors de la mise à jour de l''élément "%name%".', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(828, 25, 120, 'fr', 'Les éléments séléctionnés ont été supprimés avec succès.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(829, 25, 121, 'fr', 'Une erreur est intervenue lors de la suppression des éléments séléctionnés.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(830, 25, 122, 'fr', 'Une erreur est intervenue lors de la suppression de l''élément "%name%".', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(831, 25, 123, 'fr', 'L''élément "%name%" a été supprimé avec succès.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(832, 25, 124, 'fr', '⌂', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(833, 25, 125, 'fr', 'Confirmation de suppression', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(834, 25, 126, 'fr', 'Êtes-vous sûr de vouloir supprimer l''élément "%object%" sélectionné?', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(835, 25, 127, 'fr', 'Oui, supprimer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(836, 25, 128, 'fr', 'Confirmation d''un traitement par lots: ''%action%''', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(837, 25, 129, 'fr', 'Êtes-vous sûr de vouloir confirmer cette action et de l''exécuter pour l''élément sélectionné?|Êtes-vous sûr de vouloir confirmer cette action et de l''exécuter pour les %count% éléments sélectionnés?', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(838, 25, 130, 'fr', 'Êtes-vous sûr de vouloir confirmer cette action et de l''exécuter pour tous les éléments?', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(839, 25, 131, 'fr', 'Oui, exécuter', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(840, 25, 132, 'fr', 'oui', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(841, 25, 133, 'fr', 'non', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(842, 25, 134, 'fr', 'contient', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(843, 25, 135, 'fr', 'ne contient pas', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(844, 25, 136, 'fr', 'est égal à', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(845, 25, 137, 'fr', 'n''est pas égal à', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(846, 25, 138, 'fr', '=', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(847, 25, 139, 'fr', '>=', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(848, 25, 140, 'fr', '>', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(849, 25, 141, 'fr', '<=', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(850, 25, 142, 'fr', '<', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(851, 25, 143, 'fr', '=', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(852, 25, 144, 'fr', '>=', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(853, 25, 145, 'fr', '>', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(854, 25, 146, 'fr', '<=', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(855, 25, 147, 'fr', '<', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(856, 25, 148, 'fr', 'est vide', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(857, 25, 149, 'fr', 'n''est pas vide', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(858, 25, 150, 'fr', 'est entre', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(859, 25, 151, 'fr', 'n''est pas entre', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(860, 25, 152, 'fr', 'Filtres', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(861, 25, 153, 'fr', 'ou', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(862, 25, 154, 'fr', 'Révisions', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(863, 25, 155, 'fr', 'Action', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(864, 25, 156, 'fr', 'Comparer', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(865, 25, 157, 'fr', 'Révisions', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(866, 25, 158, 'fr', 'Date', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(867, 25, 159, 'fr', 'Auteur', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(868, 25, 160, 'fr', 'Rôle', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(869, 25, 161, 'fr', 'Afficher la révision', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(870, 25, 162, 'fr', 'Comparer la révision', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(871, 25, 163, 'fr', 'au moins', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(872, 25, 164, 'fr', '1 résultat|%count% résultats', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(873, 25, 165, 'fr', 'Export', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(874, 25, 166, 'fr', 'JSON', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(875, 25, 167, 'fr', 'XML', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(876, 25, 168, 'fr', 'CSV', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(877, 25, 169, 'fr', 'XLS', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(878, 25, 170, 'fr', 'Chargement des informations…', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(879, 25, 171, 'fr', 'Prévisualiser', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(880, 25, 172, 'fr', 'Accepter', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(881, 25, 173, 'fr', 'Refuser', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(882, 25, 174, 'fr', 'Par page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(883, 25, 175, 'fr', 'Sélectionner', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(884, 25, 176, 'fr', 'Vous avez effectué des modifications non sauvegardées.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(885, 25, 177, 'fr', 'Editer les permissions', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(886, 25, 178, 'fr', 'Mettre à jour les permissions', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(887, 25, 179, 'fr', 'Les permissions ont été mises à jour avec succès.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(888, 25, 180, 'fr', 'Permissions', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(889, 25, 181, 'fr', 'Aucune sélection', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(890, 25, 182, 'fr', 'Résultats de recherche: %query%', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(891, 25, 183, 'fr', 'Rechercher', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(892, 25, 184, 'fr', 'pas de résultats', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(893, 25, 185, 'fr', 'rajouter un élément', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(894, 25, 997, 'fr', 'Formulaire non disponible', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(895, 25, 186, 'fr', 'Liste d''actions', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(896, 25, 187, 'fr', 'Certaines fonctionnalités ne fonctionnent sans JavaScript; merci de l''activer sur votre navigateur.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(897, 25, 188, 'fr', 'Pas de champs disponibles.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(898, 25, 189, 'fr', 'Filtres', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(899, 25, 190, 'fr', 'Voir plus', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(900, 25, 191, 'fr', 'Select object type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(901, 25, 192, 'fr', 'No object types available', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(902, 26, 193, 'fr', 'Translation', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(903, 26, 194, 'fr', 'Changer de langue', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(904, 27, 195, 'fr', 'Accueil', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(905, 27, 196, 'fr', 'Partager par e-mail', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(906, 28, 197, 'fr', 'Notification', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(907, 28, 198, 'fr', 'Notifications', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(908, 28, 199, 'fr', 'Messages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(909, 28, 200, 'fr', 'Id', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(910, 28, 201, 'fr', 'Type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(911, 28, 202, 'fr', 'Date de création', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(912, 28, 203, 'fr', 'Date de début', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(913, 28, 204, 'fr', 'Date de fin', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(914, 28, 205, 'fr', 'Etat', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(915, 28, 206, 'fr', 'Id', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(916, 28, 207, 'fr', 'Type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(917, 28, 208, 'fr', 'Date de création', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(918, 28, 209, 'fr', 'Date de début', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(919, 28, 210, 'fr', 'Date de fin', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(920, 28, 211, 'fr', 'Etat', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(921, 28, 212, 'fr', 'Message', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(922, 28, 213, 'fr', 'Payload', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(923, 28, 214, 'fr', 'Type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(924, 28, 215, 'fr', 'Etat', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(925, 28, 216, 'fr', 'Publier les messages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(926, 28, 217, 'fr', 'Annuler les messages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(927, 28, 218, 'fr', 'Restart #', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(928, 28, 219, 'fr', 'Restart #', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(929, 29, 220, 'fr', 'Bloc', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(930, 29, 221, 'fr', 'Blocs traités par lots', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(931, 29, 222, 'fr', 'Création d''un bloc', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(932, 29, 223, 'fr', 'Suppression d''un bloc', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(933, 29, 224, 'fr', 'Edition d''un bloc', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(934, 29, 225, 'fr', 'Liste des blocs', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(935, 29, 226, 'fr', 'Mise à jour des blocs', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(936, 29, 227, 'fr', 'Dashboard', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(937, 29, 228, 'fr', 'Pages traitées par lots', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(938, 29, 229, 'fr', 'Création d''une page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(939, 29, 230, 'fr', 'Suppression d''une page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(940, 29, 231, 'fr', 'Edition d''une page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(941, 29, 232, 'fr', 'Liste des pages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(942, 29, 233, 'fr', 'Mise à jour d''une page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(943, 29, 234, 'fr', 'Publications traitées par lots', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(944, 29, 235, 'fr', 'Création d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(945, 29, 236, 'fr', 'Suppression d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(946, 29, 237, 'fr', 'Edition d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(947, 29, 238, 'fr', 'Liste des publications', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(948, 29, 239, 'fr', 'Mise à jour d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(949, 29, 240, 'fr', 'Sauvegarder', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(950, 29, 241, 'fr', 'Publier', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(951, 29, 242, 'fr', 'CMS', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(952, 29, 243, 'fr', 'Créer la page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(953, 29, 244, 'fr', 'Création d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(954, 29, 245, 'fr', 'La page demandée n''existe pas.', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(955, 29, 246, 'fr', 'Activé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(956, 29, 247, 'fr', 'Hybride', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(957, 29, 248, 'fr', 'Nom', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(958, 29, 249, 'fr', 'Page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(959, 29, 250, 'fr', 'Nom de l''URL', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(960, 29, 251, 'fr', 'Type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(961, 29, 252, 'fr', 'Avancée', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(962, 29, 253, 'fr', 'Général', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(963, 29, 254, 'fr', 'SEO', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(964, 29, 255, 'fr', 'Enfants', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(965, 29, 256, 'fr', 'URL personnalisée', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(966, 29, 257, 'fr', 'Habillé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(967, 29, 258, 'fr', 'Activé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(968, 29, 259, 'fr', 'Javascript', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(969, 29, 260, 'fr', 'Métadonnées - Description', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(970, 29, 261, 'fr', 'Métadonnées - Mots clés', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(971, 29, 262, 'fr', 'Nom', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(972, 29, 263, 'fr', 'Parent', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(973, 29, 264, 'fr', 'Position', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(974, 29, 265, 'fr', 'Date de fin de publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(975, 29, 266, 'fr', 'Date de début de publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(976, 29, 267, 'fr', 'Entêtes brutes', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(977, 29, 268, 'fr', 'Paramètres', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(978, 29, 269, 'fr', 'Slug', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(979, 29, 270, 'fr', 'Feuille de style', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(980, 29, 271, 'fr', 'Destination', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(981, 29, 272, 'fr', 'Modèle de page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(982, 29, 273, 'fr', 'Type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(983, 29, 274, 'fr', 'URL', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(984, 29, 275, 'fr', 'Informations avancées', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(985, 29, 276, 'fr', 'Informations principales', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(986, 29, 277, 'fr', 'Informations pour le référencement', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(987, 29, 278, 'fr', 'Création d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(988, 29, 279, 'fr', 'Edition d''une page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(989, 29, 280, 'fr', 'La page est inactive', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(990, 29, 281, 'fr', 'Voir les zones', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(991, 29, 282, 'fr', 'Voir toutes les erreurs', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(992, 29, 283, 'fr', 'Voir toutes les pages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(993, 29, 284, 'fr', 'Nom de la page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(994, 29, 285, 'fr', 'Hybride', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(995, 29, 286, 'fr', 'Habillé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(996, 29, 287, 'fr', 'Activé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(997, 29, 288, 'fr', 'Hybride', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(998, 29, 289, 'fr', 'Nom', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(999, 29, 290, 'fr', 'Position', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1000, 29, 291, 'fr', 'Date de fin de publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1001, 29, 292, 'fr', 'Date de début de publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1002, 29, 293, 'fr', 'Type', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1003, 29, 294, 'fr', 'Mis à jour le', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1004, 29, 295, 'fr', 'URL', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1005, 29, 296, 'fr', 'la page %code%', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1006, 29, 297, 'fr', 'Création d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1007, 29, 298, 'fr', 'ou', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1008, 29, 299, 'fr', 'Pages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1009, 29, 300, 'fr', 'URL personnalisée', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1010, 29, 301, 'fr', 'Habillé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1011, 29, 302, 'fr', 'Activé', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1012, 29, 303, 'fr', 'Nom', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1013, 29, 304, 'fr', 'Nom de l''URL', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1014, 29, 305, 'fr', 'Slug', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1015, 29, 306, 'fr', 'Edition de la page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1016, 29, 307, 'fr', 'Edition des blocs', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1017, 29, 308, 'fr', 'Publications', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1018, 29, 309, 'fr', 'Publications', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1019, 29, 310, 'fr', 'Pages', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1020, 29, 311, 'fr', 'Création d''une publication', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1021, 29, 312, 'fr', 'Liste des pages d''erreur à traiter :', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1022, 29, 313, 'fr', 'La page n''existe pas', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1023, 29, 314, 'fr', 'Etendre', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1024, 29, 315, 'fr', 'Voir la page', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1025, 29, 316, 'fr', 'Arrêter d''assumer l''identité', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1026, 29, 317, 'fr', 'Assumer l''identité', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1027, 29, 318, 'fr', 'Dashboard', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1028, 29, 319, 'fr', 'Par défaut', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1029, 29, 320, 'fr', 'Nom de domaine', '2015-08-06 02:24:40', '2015-08-06 02:24:40'),
(1030, 29, 321, 'fr', 'Chemin', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1031, 29, 322, 'fr', 'Activé jusqu''à', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1032, 29, 323, 'fr', 'Activé à partir', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1033, 29, 324, 'fr', 'Par défaut', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1034, 29, 325, 'fr', 'Nom de domaine', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1035, 29, 326, 'fr', 'Chemin', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1036, 29, 327, 'fr', 'Activé jusqu''au', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1037, 29, 328, 'fr', 'Activé à partir', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1038, 29, 329, 'fr', 'Liste des sites', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1039, 29, 330, 'fr', 'Afficher', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1040, 29, 331, 'fr', 'Créer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1041, 29, 332, 'fr', 'Editer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1042, 29, 333, 'fr', 'Publications', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1043, 29, 334, 'fr', 'Ajouter Publications', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1044, 29, 335, 'fr', 'Ajouter Publications', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1045, 29, 336, 'fr', 'Valider pour rajouter des publications pour le site : %site%', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1046, 29, 337, 'fr', 'Ajouter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1047, 29, 338, 'fr', 'Historique', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1048, 29, 339, 'fr', 'Site', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1049, 29, 340, 'fr', 'Site', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1050, 29, 341, 'fr', 'Sites', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1051, 29, 342, 'fr', 'Par défaut', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1052, 29, 343, 'fr', 'Host', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1053, 29, 344, 'fr', 'Chemin', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1054, 29, 345, 'fr', 'Activé à partir', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1055, 29, 346, 'fr', 'Activé jusqu''au', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1056, 29, 347, 'fr', 'Langue', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1057, 29, 348, 'fr', 'Langue', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1058, 29, 349, 'fr', 'Langue', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1059, 29, 350, 'fr', 'Informations pour le référencement', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1060, 29, 351, 'fr', 'Titre', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1061, 29, 352, 'fr', 'Meta Keywords', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1062, 29, 353, 'fr', 'Titre', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1063, 29, 354, 'fr', 'Meta Description', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1064, 29, 355, 'fr', 'Meta Keywords', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1065, 29, 356, 'fr', 'Informations principales', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1066, 29, 357, 'fr', 'Site', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1067, 29, 358, 'fr', 'Edité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1068, 29, 359, 'fr', 'Edité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1069, 29, 360, 'fr', 'Site', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1070, 29, 361, 'fr', 'Edité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1071, 29, 362, 'fr', 'Page', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1072, 29, 363, 'fr', 'Alias technique', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1073, 29, 364, 'fr', 'Alias technique', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1074, 29, 365, 'fr', 'Alias technique', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1075, 29, 366, 'fr', 'Alias technique', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1076, 29, 367, 'fr', 'Page interne de redirection', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1077, 29, 368, 'fr', '\n             La page est configurée pour rediriger l''utilisateur vers une autre page. Pour\n             permettre aux éditeurs d''éditer cette page; la redirection a été interceptée.\n             <br />\n             <br />\n             Merci de cliquer sur le lien pour suivre la redirection: <a href="%url%">%url%</a>.\n          ', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1078, 29, 369, 'fr', 'Parent', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1079, 29, 370, 'fr', 'Sélectionner un type de bloc', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1080, 29, 371, 'fr', 'Bloc', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1081, 29, 372, 'fr', 'Options', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1082, 29, 373, 'fr', 'Publications crées avec succès', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1083, 29, 374, 'fr', 'Merci de selectionner le site cible', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1084, 29, 375, 'fr', 'Avant de créer une nouvelle page, vous devez selectionner le site où la page sera accessible.', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1085, 29, 376, 'fr', 'Composer (beta)', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1086, 29, 377, 'fr', 'Liste', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1087, 29, 378, 'fr', 'Arbre', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1088, 29, 379, 'fr', 'Arbre', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1089, 29, 380, 'fr', 'Pages pour le site', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1090, 29, 381, 'fr', 'arbre', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1091, 29, 382, 'fr', 'liste', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1092, 29, 383, 'fr', 'Composer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1093, 29, 384, 'fr', 'Aucun type disponible', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1094, 29, 385, 'fr', 'Mise en page de', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1095, 29, 386, 'fr', 'Conteneurs orphelins', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1096, 29, 387, 'fr', 'Supprimer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1097, 29, 388, 'fr', 'Confirmer la suppression ?', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1098, 29, 389, 'fr', 'Oui', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1099, 29, 390, 'fr', 'Annuler', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1100, 29, 391, 'fr', 'Chargement', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1101, 29, 392, 'fr', 'Ajouter un bloc de type', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1102, 29, 393, 'fr', 'Notice', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1103, 29, 394, 'fr', 'Blocs', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1104, 29, 395, 'fr', 'Désactiver', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1105, 29, 396, 'fr', 'Activer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1106, 29, 397, 'fr', 'Blocs Partagés', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1107, 29, 398, 'fr', 'Blocs partagés traités par lots', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1108, 29, 399, 'fr', 'Création d''un bloc partagé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1109, 29, 400, 'fr', 'Suppression d''un bloc partagé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1110, 29, 401, 'fr', 'Edition d''un block partagé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1111, 29, 402, 'fr', 'Liste des blocs partagés', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1112, 29, 403, 'fr', 'Mise à jour des blocs partagés', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1113, 29, 404, 'fr', 'Edition des blocs partagés', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1114, 29, 405, 'fr', 'Sélectionner un type de bloc partagé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1115, 29, 406, 'fr', 'Ajouter un bloc partagé de type', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1116, 29, 407, 'fr', 'Constituer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1117, 29, 408, 'fr', 'Editée', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1118, 29, 409, 'fr', 'Modèle', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1119, 29, 410, 'fr', 'page.compose_blocks_label', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1120, 30, 411, 'fr', 'L''URL ''%url%'' est déjà associée à une autre page', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1121, 31, 412, 'fr', 'Symfony CMF', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1122, 31, 413, 'fr', 'Route', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1123, 31, 414, 'fr', 'Redirection', '2015-08-06 02:24:41', '2015-08-06 02:24:41');
INSERT INTO `lexik_trans_unit_translations` (`id`, `file_id`, `trans_unit_id`, `locale`, `content`, `created_at`, `updated_at`) VALUES
(1124, 31, 415, 'fr', 'Routes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1125, 31, 416, 'fr', 'Créer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1126, 31, 417, 'fr', 'Éditer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1127, 31, 418, 'fr', 'Supprimer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1128, 31, 419, 'fr', 'Redirection', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1129, 31, 420, 'fr', 'Créer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1130, 31, 421, 'fr', 'Éditer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1131, 31, 422, 'fr', 'Supprimer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1132, 31, 423, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1133, 31, 424, 'fr', 'URL', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1134, 31, 425, 'fr', 'Général', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1135, 31, 426, 'fr', 'Avancé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1136, 31, 427, 'fr', 'Parent', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1137, 31, 428, 'fr', 'Dernière partie de l''URL', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1138, 31, 429, 'fr', 'Motif variable', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1139, 31, 430, 'fr', 'Un motif en format {variable}/{plus}... Les variables sont passé au controlleur s''il les déclare comme arguments.', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1140, 31, 431, 'fr', 'Contenu', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1141, 31, 432, 'fr', 'Defauts', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1142, 31, 433, 'fr', 'Options', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1143, 31, 434, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1144, 31, 435, 'fr', 'URI', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1145, 31, 436, 'fr', 'Route objectif', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1146, 31, 437, 'fr', 'Routes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1147, 31, 438, 'fr', 'Routes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1148, 31, 439, 'fr', 'Ajouter le langage', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1149, 31, 440, 'fr', 'Ajouter le motif du format', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1150, 31, 441, 'fr', 'Ajouter une barre oblique en fin', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1151, 31, 442, 'fr', '\n            Langage: Prefix la route avec le langage tel que /{locale}/your/route<br/>\n            Format: Ajoute le format à la route tel que /your/route.{format}. Le format par défaut est ''html''<br/>\n            Barre Oblique: Ajoute un slash à la fin de la route tel que /your/route/\n            \n        ', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1152, 31, 443, 'fr', 'Frontend', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1153, 31, 444, 'fr', 'Ouvrir ce front-end lien dans un nouvel onglet', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1154, 32, 445, 'fr', 'Invalider le cache', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1155, 32, 446, 'fr', 'Domaine', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1156, 32, 447, 'fr', 'Clé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1157, 32, 448, 'fr', 'Liste des traductions', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1158, 32, 449, 'fr', 'Fichiers du cache supprimés.', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1159, 32, 450, 'fr', 'Nouvelle traduction', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1160, 32, 451, 'fr', 'Ajouter une traduction', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1161, 32, 452, 'fr', 'Enregistrer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1162, 32, 453, 'fr', 'Enregistrer et Ajouter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1163, 32, 454, 'fr', 'Traductions', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1164, 32, 455, 'fr', 'Afficher/Masquer des colonnes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1165, 32, 456, 'fr', 'Afficher/Masquer toutes les colonnes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1166, 32, 457, 'fr', 'Sauvegarder la ligne', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1167, 32, 458, 'fr', 'Les traductions pour la clé #%id% ont bien été mises à jour.', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1168, 32, 459, 'fr', 'Traduction ajouté avec succès.', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1169, 32, 460, 'fr', 'Erreur lors de la mise à jour des traductions pour clé #%id%.', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1170, 32, 461, 'fr', 'Retour', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1171, 34, 766, 'fr', 'Inscription', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1172, 34, 767, 'fr', 'Profil utilisateur', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1173, 34, 768, 'fr', 'Éditer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1174, 34, 769, 'fr', 'Identification', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1175, 34, 770, 'fr', 'Envoyer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1176, 34, 771, 'fr', 'Utilisateurs', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1177, 34, 772, 'fr', 'Ajouter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1178, 34, 773, 'fr', 'Éditer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1179, 34, 774, 'fr', 'Supprimer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1180, 34, 775, 'fr', 'Groupes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1181, 34, 776, 'fr', 'Ajouter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1182, 34, 777, 'fr', 'Éditer', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1183, 34, 778, 'fr', 'Utilisateurs', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1184, 34, 779, 'fr', 'Groupes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1185, 34, 780, 'fr', 'Quitter l''identité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1186, 34, 781, 'fr', 'Changer d''identité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1187, 34, 782, 'fr', 'Déconnexion', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1188, 34, 783, 'fr', 'Profil', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1189, 34, 784, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1190, 34, 785, 'fr', 'Email', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1191, 34, 786, 'fr', 'Mot de passe', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1192, 34, 787, 'fr', 'Groupes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1193, 34, 788, 'fr', 'Rôles', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1194, 34, 789, 'fr', 'Verrouillé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1195, 34, 790, 'fr', 'Expiré', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1196, 34, 791, 'fr', 'Activé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1197, 34, 792, 'fr', 'Droits expirés', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1198, 34, 793, 'fr', 'Créé le', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1199, 34, 794, 'fr', 'Dernière connexion', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1200, 34, 795, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1201, 34, 796, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1202, 34, 797, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1203, 34, 798, 'fr', 'Email', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1204, 34, 799, 'fr', 'Verrouillé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1205, 34, 800, 'fr', 'ID', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1206, 34, 802, 'fr', 'Groupes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1207, 34, 803, 'fr', 'Créé le', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1208, 34, 804, 'fr', 'Dernière connexion', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1209, 34, 805, 'fr', 'Prénom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1210, 34, 806, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1211, 34, 807, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1212, 34, 808, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1213, 34, 809, 'fr', 'Adresse email', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1214, 34, 810, 'fr', 'Groupes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1215, 34, 811, 'fr', 'Verrouillé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1216, 34, 812, 'fr', 'Activé', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1217, 34, 813, 'fr', 'Créé le', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1218, 34, 814, 'fr', 'Dernière connexion', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1219, 34, 815, 'fr', 'Changer d''identité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1220, 34, 816, 'fr', 'Rôles', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1221, 34, 817, 'fr', 'Prénom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1222, 34, 818, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1223, 34, 819, 'fr', 'Validation en deux étapes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1224, 34, 820, 'fr', 'Rentrer votre code de validation provenant de votre téléphone', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1225, 34, 821, 'fr', 'Le code de validation est invalide', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1226, 34, 822, 'fr', 'Utilisateurs', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1227, 34, 823, 'fr', 'Général', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1228, 34, 824, 'fr', 'Gestion', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1229, 34, 998, 'fr', 'Profil', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1230, 34, 999, 'fr', 'Social', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1231, 34, 1000, 'fr', 'Sécurité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1232, 34, 825, 'fr', 'Rôles', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1233, 34, 826, 'fr', 'Date de naissance', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1234, 34, 827, 'fr', 'Prénom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1235, 34, 828, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1236, 34, 829, 'fr', 'Site web', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1237, 34, 830, 'fr', 'Biographie', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1238, 34, 831, 'fr', 'Civilité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1239, 34, 832, 'fr', 'Langue', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1240, 34, 833, 'fr', 'Fuseau horaire', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1241, 34, 834, 'fr', 'Téléphone', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1242, 34, 835, 'fr', 'Uid Facebook', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1243, 34, 836, 'fr', 'Nom Facebook', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1244, 34, 837, 'fr', 'Uid Twitter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1245, 34, 838, 'fr', 'Nom Twitter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1246, 34, 839, 'fr', 'Uid Google+', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1247, 34, 840, 'fr', 'Nom Google+', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1248, 34, 841, 'fr', 'Jeton', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1249, 34, 842, 'fr', 'Code de validation en deux étapes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1250, 34, 843, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1251, 34, 844, 'fr', 'Email', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1252, 34, 845, 'fr', 'Groupes', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1253, 34, 846, 'fr', 'Date de naissance', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1254, 34, 847, 'fr', 'Prénom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1255, 34, 848, 'fr', 'Nom', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1256, 34, 849, 'fr', 'Site web', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1257, 34, 850, 'fr', 'Biographie', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1258, 34, 851, 'fr', 'Civilité', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1259, 34, 852, 'fr', 'Langue', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1260, 34, 853, 'fr', 'Fuseau horaire', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1261, 34, 854, 'fr', 'Téléphone', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1262, 34, 855, 'fr', 'Uid Facebook', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1263, 34, 856, 'fr', 'Nom Facebook', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1264, 34, 857, 'fr', 'Uid Twitter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1265, 34, 858, 'fr', 'Nom Twitter', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1266, 34, 859, 'fr', 'Uid Google+', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1267, 34, 860, 'fr', 'Nom Google+', '2015-08-06 02:24:41', '2015-08-06 02:24:41'),
(1268, 34, 861, 'fr', 'Jeton', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1269, 34, 862, 'fr', 'Code de validation en deux étapes', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1270, 34, 863, 'fr', 'Créé le', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1271, 34, 864, 'fr', 'Dernière connexion', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1272, 34, 865, 'fr', 'Voir', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1273, 34, 866, 'fr', 'non défini', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1274, 34, 867, 'fr', 'femme', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1275, 34, 868, 'fr', 'homme', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1276, 34, 869, 'fr', 'Mon tableau de bord', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1277, 34, 870, 'fr', 'Mon tableau de bord', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1278, 34, 871, 'fr', 'Mon profil', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1279, 34, 872, 'fr', 'Informations de connexion', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1280, 34, 873, 'fr', 'Informations de connexion', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1281, 34, 874, 'fr', 'Civilité', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1282, 34, 875, 'fr', 'Prénom', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1283, 34, 876, 'fr', 'Nom', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1284, 34, 877, 'fr', 'Site web', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1285, 34, 878, 'fr', 'Date de naissance', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1286, 34, 879, 'fr', 'Biographie', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1287, 34, 880, 'fr', 'Langue', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1288, 34, 881, 'fr', 'Fuseau horaire', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1289, 34, 882, 'fr', 'Téléphone', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1290, 34, 883, 'fr', 'Votre profil a été mis à jour.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1291, 34, 884, 'fr', 'Changer votre mot de passe', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1292, 34, 885, 'fr', 'S''inscrire', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1293, 34, 886, 'fr', 'S''identifier', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1294, 34, 887, 'fr', 'Se déconnecter', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1295, 34, 888, 'fr', 'Votre compte est créé avec succès !', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1296, 34, 889, 'fr', 'Mot de passe oublié ?', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1297, 34, 890, 'fr', 'Tableau de bord', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1298, 34, 891, 'fr', 'Éditer votre profil', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1299, 34, 892, 'fr', 'Vous êtes déjà identifié', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1300, 34, 893, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1301, 34, 894, 'fr', 'Mot de passe', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1302, 34, 895, 'fr', 'Nom d''utilisateur', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1303, 34, 896, 'fr', 'Adresse e-mail', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1304, 34, 897, 'fr', 'Mot de passe', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1305, 34, 898, 'fr', 'Vérification', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1306, 35, 899, 'fr', 'Cette valeur doit être fausse.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1307, 35, 900, 'fr', 'Cette valeur doit être vraie.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1308, 35, 901, 'fr', 'Cette valeur doit être de type {{ type }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1309, 35, 902, 'fr', 'Cette valeur doit être vide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1310, 35, 903, 'fr', 'Cette valeur doit être l''un des choix proposés.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1311, 35, 904, 'fr', 'Vous devez sélectionner au moins {{ limit }} choix.|Vous devez sélectionner au moins {{ limit }} choix.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1312, 35, 905, 'fr', 'Vous devez sélectionner au maximum {{ limit }} choix.|Vous devez sélectionner au maximum {{ limit }} choix.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1313, 35, 906, 'fr', 'Une ou plusieurs des valeurs soumises sont invalides.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1314, 35, 907, 'fr', 'Ce champ n''a pas été prévu.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1315, 35, 908, 'fr', 'Ce champ est manquant.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1316, 35, 909, 'fr', 'Cette valeur n''est pas une date valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1317, 35, 910, 'fr', 'Cette valeur n''est pas une date valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1318, 35, 911, 'fr', 'Cette valeur n''est pas une adresse email valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1319, 35, 912, 'fr', 'Le fichier n''a pas été trouvé.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1320, 35, 913, 'fr', 'Le fichier n''est pas lisible.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1321, 35, 914, 'fr', 'Le fichier est trop volumineux ({{ size }} {{ suffix }}). Sa taille ne doit pas dépasser {{ limit }} {{ suffix }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1322, 35, 915, 'fr', 'Le type du fichier est invalide ({{ type }}). Les types autorisés sont {{ types }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1323, 35, 916, 'fr', 'Cette valeur doit être inférieure ou égale à {{ limit }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1324, 35, 917, 'fr', 'Cette chaine est trop longue. Elle doit avoir au maximum {{ limit }} caractère.|Cette chaine est trop longue. Elle doit avoir au maximum {{ limit }} caractères.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1325, 35, 918, 'fr', 'Cette valeur doit être supérieure ou égale à {{ limit }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1326, 35, 919, 'fr', 'Cette chaine est trop courte. Elle doit avoir au minimum {{ limit }} caractère.|Cette chaine est trop courte. Elle doit avoir au minimum {{ limit }} caractères.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1327, 35, 920, 'fr', 'Cette valeur ne doit pas être vide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1328, 35, 921, 'fr', 'Cette valeur ne doit pas être nulle.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1329, 35, 922, 'fr', 'Cette valeur doit être nulle.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1330, 35, 923, 'fr', 'Cette valeur n''est pas valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1331, 35, 924, 'fr', 'Cette valeur n''est pas une heure valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1332, 35, 925, 'fr', 'Cette valeur n''est pas une URL valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1333, 35, 926, 'fr', 'Les deux valeurs doivent être identiques.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1334, 35, 927, 'fr', 'Le fichier est trop volumineux. Sa taille ne doit pas dépasser {{ limit }} {{ suffix }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1335, 35, 928, 'fr', 'Le fichier est trop volumineux.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1336, 35, 929, 'fr', 'Le téléchargement de ce fichier est impossible.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1337, 35, 930, 'fr', 'Cette valeur doit être un nombre.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1338, 35, 931, 'fr', 'Ce fichier n''est pas une image valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1339, 35, 932, 'fr', 'Cette adresse IP n''est pas valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1340, 35, 933, 'fr', 'Cette langue n''est pas valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1341, 35, 934, 'fr', 'Ce paramètre régional n''est pas valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1342, 35, 935, 'fr', 'Ce pays n''est pas valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1343, 35, 936, 'fr', 'Cette valeur est déjà utilisée.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1344, 35, 937, 'fr', 'La taille de l''image n''a pas pu être détectée.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1345, 35, 938, 'fr', 'La largeur de l''image est trop grande ({{ width }}px). La largeur maximale autorisée est de {{ max_width }}px.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1346, 35, 939, 'fr', 'La largeur de l''image est trop petite ({{ width }}px). La largeur minimale attendue est de {{ min_width }}px.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1347, 35, 940, 'fr', 'La hauteur de l''image est trop grande ({{ height }}px). La hauteur maximale autorisée est de {{ max_height }}px.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1348, 35, 941, 'fr', 'La hauteur de l''image est trop petite ({{ height }}px). La hauteur minimale attendue est de {{ min_height }}px.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1349, 35, 942, 'fr', 'Cette valeur doit être le mot de passe actuel de l''utilisateur.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1350, 35, 943, 'fr', 'Cette chaine doit avoir exactement {{ limit }} caractère.|Cette chaine doit avoir exactement {{ limit }} caractères.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1351, 35, 944, 'fr', 'Le fichier a été partiellement transféré.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1352, 35, 945, 'fr', 'Aucun fichier n''a été transféré.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1353, 35, 946, 'fr', 'Aucun répertoire temporaire n''a été configuré dans le php.ini.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1354, 35, 947, 'fr', 'Impossible d''écrire le fichier temporaire sur le disque.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1355, 35, 948, 'fr', 'Une extension PHP a empêché le transfert du fichier.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1356, 35, 949, 'fr', 'Cette collection doit contenir {{ limit }} élément ou plus.|Cette collection doit contenir {{ limit }} éléments ou plus.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1357, 35, 950, 'fr', 'Cette collection doit contenir {{ limit }} élément ou moins.|Cette collection doit contenir {{ limit }} éléments ou moins.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1358, 35, 951, 'fr', 'Cette collection doit contenir exactement {{ limit }} élément.|Cette collection doit contenir exactement {{ limit }} éléments.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1359, 35, 952, 'fr', 'Numéro de carte invalide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1360, 35, 953, 'fr', 'Type de carte non supporté ou numéro invalide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1361, 35, 954, 'fr', 'Le numéro IBAN (International Bank Account Number) saisi n''est pas valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1362, 35, 955, 'fr', 'Cette valeur n''est pas un code ISBN-10 valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1363, 35, 956, 'fr', 'Cette valeur n''est pas un code ISBN-13 valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1364, 35, 957, 'fr', 'Cette valeur n''est ni un code ISBN-10, ni un code ISBN-13 valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1365, 35, 958, 'fr', 'Cette valeur n''est pas un code ISSN valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1366, 35, 959, 'fr', 'Cette valeur n''est pas une devise valide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1367, 35, 960, 'fr', 'Cette valeur doit être égale à {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1368, 35, 961, 'fr', 'Cette valeur doit être supérieure à {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1369, 35, 962, 'fr', 'Cette valeur doit être supérieure ou égale à {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1370, 35, 963, 'fr', 'Cette valeur doit être identique à {{ compared_value_type }} {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1371, 35, 964, 'fr', 'Cette valeur doit être inférieure à {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1372, 35, 965, 'fr', 'Cette valeur doit être inférieure ou égale à {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1373, 35, 966, 'fr', 'Cette valeur ne doit pas être égale à {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1374, 35, 967, 'fr', 'Cette valeur ne doit pas être identique à {{ compared_value_type }} {{ compared_value }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1375, 35, 968, 'fr', 'Le rapport largeur/hauteur de l''image est trop grand ({{ ratio }}). Le rapport maximal autorisé est {{ max_ratio }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1376, 35, 969, 'fr', 'Le rapport largeur/hauteur de l''image est trop petit ({{ ratio }}). Le rapport minimal attendu est {{ min_ratio }}.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1377, 35, 970, 'fr', 'L''image est carrée ({{ width }}x{{ height }}px). Les images carrées ne sont pas autorisées.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1378, 35, 971, 'fr', 'L''image est au format paysage ({{ width }}x{{ height }}px). Les images au format paysage ne sont pas autorisées.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1379, 35, 972, 'fr', 'L''image est au format portrait ({{ width }}x{{ height }}px). Les images au format portrait ne sont pas autorisées.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1380, 35, 973, 'fr', 'Un fichier vide n''est pas autorisé.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1381, 35, 974, 'fr', 'Le nom de domaine n''a pas pu être résolu.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1382, 35, 975, 'fr', 'Cette valeur ne correspond pas au jeu de caractères {{ charset }} attendu.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1383, 36, 976, 'fr', 'Ce formulaire ne doit pas contenir des champs supplémentaires.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1384, 36, 977, 'fr', 'Le fichier téléchargé est trop volumineux. Merci d''essayer d''envoyer un fichier plus petit.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1385, 36, 978, 'fr', 'Le jeton CSRF est invalide. Veuillez renvoyer le formulaire.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1386, 37, 979, 'fr', 'Une exception d''authentification s''est produite.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1387, 37, 980, 'fr', 'Les identifiants d''authentification n''ont pas pu être trouvés.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1388, 37, 981, 'fr', 'La requête d''authentification n''a pas pu être executée à cause d''un problème système.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1389, 37, 982, 'fr', 'Identifiants invalides.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1390, 37, 983, 'fr', 'Le cookie a déjà été utilisé par quelqu''un d''autre.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1391, 37, 984, 'fr', 'Privilèges insuffisants pour accéder à la ressource.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1392, 37, 985, 'fr', 'Jeton CSRF invalide.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1393, 37, 986, 'fr', 'Le digest nonce a expiré.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1394, 37, 987, 'fr', 'Aucun fournisseur d''authentification n''a été trouvé pour supporter le jeton d''authentification.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1395, 37, 988, 'fr', 'Aucune session disponible, celle-ci a expiré ou les cookies ne sont pas activés.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1396, 37, 989, 'fr', 'Aucun jeton n''a pu être trouvé.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1397, 37, 990, 'fr', 'Le nom d''utilisateur n''a pas pu être trouvé.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1398, 37, 991, 'fr', 'Le compte a expiré.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1399, 37, 992, 'fr', 'Les identifiants ont expiré.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1400, 37, 993, 'fr', 'Le compte est désactivé.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1401, 37, 994, 'fr', 'Le compte est bloqué.', '2015-08-06 02:24:42', '2015-08-06 02:24:42'),
(1402, NULL, 996, 'en', 'I love Symfony2', '2015-08-06 02:33:46', '2015-08-06 02:33:46');

-- --------------------------------------------------------

--
-- Structure de la table `media__gallery`
--

DROP TABLE IF EXISTS `media__gallery`;
CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media__gallery_media`
--

DROP TABLE IF EXISTS `media__gallery_media`;
CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media__media`
--

DROP TABLE IF EXISTS `media__media`;
CREATE TABLE `media__media` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `media__media`
--

INSERT INTO `media__media` (`id`, `category_id`, `name`, `description`, `enabled`, `provider_name`, `provider_status`, `provider_reference`, `provider_metadata`, `width`, `height`, `length`, `content_type`, `content_size`, `copyright`, `author_name`, `context`, `cdn_is_flushable`, `cdn_flush_identifier`, `cdn_flush_at`, `cdn_status`, `updated_at`, `created_at`) VALUES
(3, NULL, '01_The-Document-Performance-Rebecca-Schneider.jpg', NULL, 0, 'sonata.media.provider.image', 1, 'f76ee87c4d83f1d6633820fad97f88b29c476c76.jpeg', '{"filename":"01_The-Document-Performance-Rebecca-Schneider.jpg"}', 1275, 1651, NULL, 'image/jpeg', 365931, NULL, NULL, 'default', 1, NULL, NULL, NULL, '2015-08-07 01:45:09', '2015-08-07 01:44:45'),
(5, NULL, 'Ghost Towns in 8K', NULL, 1, 'sonata.media.provider.youtube', 1, 'sLprVF6d7Ug', '{"provider_name":"YouTube","version":"1.0","author_url":"https:\\/\\/www.youtube.com\\/user\\/Neumannfilms","type":"video","author_name":"Neumannfilms","thumbnail_url":"https:\\/\\/i.ytimg.com\\/vi\\/sLprVF6d7Ug\\/hqdefault.jpg","provider_url":"https:\\/\\/www.youtube.com\\/","title":"Ghost Towns in 8K","height":270,"width":480,"thumbnail_height":360,"html":"<iframe width=\\"480\\" height=\\"270\\" src=\\"https:\\/\\/www.youtube.com\\/embed\\/sLprVF6d7Ug?feature=oembed\\" frameborder=\\"0\\" allowfullscreen><\\/iframe>","thumbnail_width":480}', 480, 270, NULL, 'video/x-flv', NULL, NULL, 'Neumannfilms', 'default', 1, NULL, NULL, NULL, '2015-08-07 01:51:03', '2015-08-07 01:49:37'),
(6, NULL, 'P!nk - Family Portrait', NULL, 0, 'sonata.media.provider.youtube', 1, 'hSjIz8oQuko', '{"thumbnail_url":"https:\\/\\/i.ytimg.com\\/vi\\/hSjIz8oQuko\\/hqdefault.jpg","thumbnail_width":480,"width":480,"provider_name":"YouTube","html":"<iframe width=\\"480\\" height=\\"270\\" src=\\"https:\\/\\/www.youtube.com\\/embed\\/hSjIz8oQuko?feature=oembed\\" frameborder=\\"0\\" allowfullscreen><\\/iframe>","author_name":"PinkVEVO","title":"P!nk - Family Portrait","author_url":"https:\\/\\/www.youtube.com\\/user\\/PinkVEVO","height":270,"provider_url":"https:\\/\\/www.youtube.com\\/","type":"video","thumbnail_height":360,"version":"1.0"}', 480, 270, NULL, 'video/x-flv', NULL, NULL, 'PinkVEVO', 'default', 0, NULL, NULL, NULL, '2015-08-10 15:44:07', '2015-08-10 15:43:06'),
(7, 1, '10930112_10205364288240831_8037889286486128068_n.jpg', NULL, 1, 'sonata.media.provider.image', 1, '80242557dd90db9c41de99af30a3ef7f17d33fd7.jpeg', '{"filename":"10930112_10205364288240831_8037889286486128068_n.jpg"}', 416, 416, NULL, 'image/jpeg', 19661, NULL, NULL, 'users', 1, NULL, NULL, NULL, '2015-08-11 00:25:30', '2015-08-11 00:10:23'),
(8, 1, 'Jamiroquai - Virtual Insanity (Official Video)', NULL, 0, 'sonata.media.provider.youtube', 1, '4JkIs37a2JE', '{"author_url":"https:\\/\\/www.youtube.com\\/user\\/JamiroquaiVEVO","thumbnail_height":360,"height":344,"version":"1.0","type":"video","provider_url":"https:\\/\\/www.youtube.com\\/","width":459,"provider_name":"YouTube","thumbnail_width":480,"thumbnail_url":"https:\\/\\/i.ytimg.com\\/vi\\/4JkIs37a2JE\\/hqdefault.jpg","title":"Jamiroquai - Virtual Insanity (Official Video)","html":"<iframe width=\\"459\\" height=\\"344\\" src=\\"https:\\/\\/www.youtube.com\\/embed\\/4JkIs37a2JE?feature=oembed\\" frameborder=\\"0\\" allowfullscreen><\\/iframe>","author_name":"JamiroquaiVEVO"}', 459, 344, NULL, 'video/x-flv', NULL, NULL, 'JamiroquaiVEVO', 'default', NULL, NULL, NULL, NULL, '2015-08-19 00:30:02', '2015-08-19 00:30:02');

-- --------------------------------------------------------

--
-- Structure de la table `news__comment`
--

DROP TABLE IF EXISTS `news__comment`;
CREATE TABLE `news__comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news__post`
--

DROP TABLE IF EXISTS `news__post`;
CREATE TABLE `news__post` (
  `id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abstract` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `raw_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_formatter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_date_start` datetime DEFAULT NULL,
  `comments_enabled` tinyint(1) NOT NULL,
  `comments_close_at` datetime DEFAULT NULL,
  `comments_default_status` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news__post_tag`
--

DROP TABLE IF EXISTS `news__post_tag`;
CREATE TABLE `news__post_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `notification__message`
--

DROP TABLE IF EXISTS `notification__message`;
CREATE TABLE `notification__message` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `state` int(11) NOT NULL,
  `restart_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `page__block`
--

DROP TABLE IF EXISTS `page__block`;
CREATE TABLE `page__block` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `settings` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `enabled` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `page__block`
--

INSERT INTO `page__block` (`id`, `parent_id`, `page_id`, `name`, `type`, `settings`, `enabled`, `position`, `created_at`, `updated_at`) VALUES
(11, NULL, 4, 'Header', 'sonata.page.block.container', '{"code":"header"}', 1, 1, '2015-08-06 02:00:41', '2015-08-06 02:00:41'),
(12, NULL, 4, 'Top content', 'sonata.page.block.container', '{"code":"content_top"}', 1, 1, '2015-08-06 02:00:41', '2015-08-06 02:00:41'),
(13, NULL, 4, 'Main content', 'sonata.page.block.container', '{"code":"content"}', 1, 1, '2015-08-06 02:00:41', '2015-08-06 02:00:41'),
(14, NULL, 4, 'Bottom content', 'sonata.page.block.container', '{"code":"content_bottom"}', 1, 1, '2015-08-06 02:00:41', '2015-08-06 02:00:41'),
(15, NULL, 4, 'Footer', 'sonata.page.block.container', '{"code":"footer"}', 1, 1, '2015-08-06 02:00:41', '2015-08-06 02:00:41'),
(16, NULL, 1, 'Header', 'sonata.page.block.container', '{"code":"header"}', 1, 1, '2015-08-11 20:08:17', '2015-08-11 20:08:17'),
(17, NULL, 1, 'Top content', 'sonata.page.block.container', '{"code":"content_top"}', 1, 1, '2015-08-11 20:08:17', '2015-08-11 20:08:17'),
(18, NULL, 1, 'Main content', 'sonata.page.block.container', '{"code":"content"}', 1, 1, '2015-08-11 20:08:17', '2015-08-11 20:08:17'),
(19, NULL, 1, 'Bottom content', 'sonata.page.block.container', '{"code":"content_bottom"}', 1, 1, '2015-08-11 20:08:17', '2015-08-11 20:08:17'),
(20, NULL, 1, 'Footer', 'sonata.page.block.container', '{"code":"footer"}', 1, 1, '2015-08-11 20:08:17', '2015-08-11 20:08:17'),
(22, NULL, 4, 'Left content', 'sonata.page.block.container', '{"code":"left_col"}', 1, 1, '2015-08-16 15:52:15', '2015-08-16 15:52:15'),
(23, NULL, 4, 'Right content', 'sonata.page.block.container', '{"code":"right_col"}', 1, 1, '2015-08-16 15:52:15', '2015-08-16 15:52:15'),
(24, NULL, 35, 'Header', 'sonata.page.block.container', '{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"}', 1, 1, '2015-08-16 20:49:56', '2015-08-19 00:26:22'),
(25, NULL, 35, 'Top content', 'sonata.page.block.container', '{"code":"content_top"}', 1, 1, '2015-08-16 20:49:56', '2015-08-16 20:49:56'),
(26, NULL, 35, 'Main content', 'sonata.page.block.container', '{"code":"content"}', 1, 1, '2015-08-16 20:49:56', '2015-08-16 20:49:56'),
(27, NULL, 35, 'Bottom content', 'sonata.page.block.container', '{"code":"content_bottom"}', 1, 1, '2015-08-16 20:49:56', '2015-08-16 20:49:56'),
(28, NULL, 35, 'Footer', 'sonata.page.block.container', '{"code":"footer"}', 1, 1, '2015-08-16 20:49:56', '2015-08-16 20:49:56'),
(30, 18, 1, 'dadaz', 'sonata.formatter.block.formatter', '{"format":"richhtml","rawContent":"<p><strong>dzadazd<\\/strong><\\/p>","content":"<p><strong>dzadazd<\\/strong><\\/p>","template":"SonataFormatterBundle:Block:block_formatter.html.twig"}', 1, 1, '2015-08-17 20:28:21', '2015-08-17 20:28:49'),
(31, 26, 35, NULL, 'sonata.formatter.block.formatter', '{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"}', 1, 1, '2015-08-19 00:16:59', '2015-08-19 00:27:45'),
(32, 24, 35, NULL, 'sonata.formatter.block.formatter', '{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"}', 1, 1, '2015-08-19 00:22:03', '2015-08-19 00:22:15'),
(33, 24, 35, NULL, 'sonata.seo.block.breadcrumb.homepage', '{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null}', 1, 2, '2015-08-19 00:24:40', '2015-08-19 00:24:40'),
(34, 26, 35, NULL, 'sonata.media.block.media', '{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"}', 1, 2, '2015-08-19 00:31:01', '2015-08-19 00:35:36'),
(35, 26, 35, NULL, 'sonata.page.block.children_pages', '{"title":null,"current":false,"pageId":null,"class":null}', 1, 3, '2015-08-19 00:36:42', '2015-08-19 00:36:42'),
(36, 26, 35, 'aaaaa', 'sonata.seo.block.breadcrumb.homepage', '{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true}', 1, 28, '2015-08-19 00:37:12', '2015-08-19 00:40:31'),
(37, 26, 35, 'salut', 'sonata.seo.block.email.share_button', '{"subject":"hehe","body":"mouahaha"}', 1, 5, '2015-08-19 00:42:10', '2015-08-19 00:42:10'),
(39, 26, 35, NULL, 'sonata.seo.block.facebook.like_button', '{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"}', 1, 7, '2015-08-19 00:44:34', '2015-08-19 00:45:29'),
(40, 26, 35, NULL, 'sonata.media.block.feature_media', '{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"}', 0, 7, '2015-08-19 00:46:08', '2015-08-19 00:46:31'),
(42, 26, 35, NULL, 'sonata.timeline.block.timeline', '{"template":"SonataTimelineBundle:Block:timeline.html.twig","context":"GLOBAL","filter":true,"group_by_action":true,"paginate":true,"max_per_page":10,"title":"a"}', 1, 9, '2015-08-19 01:18:14', '2015-08-19 01:19:24'),
(43, 26, 35, 'fr', 'sonata_translation.block.locale_switcher', '[]', 1, 9, '2015-08-19 01:20:22', '2015-08-19 01:20:22'),
(44, 26, 35, NULL, 'sonata.news.block.recent_posts', '{"number":10,"title":"2","mode":"public"}', 1, 10, '2015-08-19 11:01:53', '2015-08-19 11:01:53'),
(45, NULL, 5, 'Header', 'sonata.page.block.container', '{"code":"header"}', 1, 1, '2015-08-19 11:46:39', '2015-08-19 11:46:39'),
(46, NULL, 5, 'Top content', 'sonata.page.block.container', '{"code":"content_top"}', 1, 1, '2015-08-19 11:46:39', '2015-08-19 11:46:39'),
(47, NULL, 5, 'Main content', 'sonata.page.block.container', '{"code":"content"}', 1, 1, '2015-08-19 11:46:39', '2015-08-19 11:46:39'),
(48, NULL, 5, 'Bottom content', 'sonata.page.block.container', '{"code":"content_bottom"}', 1, 1, '2015-08-19 11:46:39', '2015-08-19 11:46:39'),
(49, NULL, 5, 'Footer', 'sonata.page.block.container', '{"code":"footer"}', 1, 1, '2015-08-19 11:46:39', '2015-08-19 11:46:39');

-- --------------------------------------------------------

--
-- Structure de la table `page__page`
--

DROP TABLE IF EXISTS `page__page`;
CREATE TABLE `page__page` (
  `id` int(11) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `decorate` tinyint(1) NOT NULL,
  `edited` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` longtext COLLATE utf8_unicode_ci,
  `url` longtext COLLATE utf8_unicode_ci,
  `custom_url` longtext COLLATE utf8_unicode_ci,
  `request_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `javascript` longtext COLLATE utf8_unicode_ci,
  `stylesheet` longtext COLLATE utf8_unicode_ci,
  `raw_headers` longtext COLLATE utf8_unicode_ci,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `page__page`
--

INSERT INTO `page__page` (`id`, `site_id`, `parent_id`, `target_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `edited`, `name`, `slug`, `url`, `custom_url`, `request_method`, `title`, `meta_keyword`, `meta_description`, `javascript`, `stylesheet`, `raw_headers`, `template`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 1, 'Homepage', '', '/', 'homepage', 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-08-18 22:27:13'),
(4, 1, NULL, NULL, 'ye_site_dashboard', NULL, NULL, 1, 1, 0, 0, 'ye_site_dashboard', 'dashboard', '/dashboard', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-08-23 00:59:28'),
(5, 1, NULL, NULL, 'fos_user_security_login', NULL, NULL, 1, 1, 0, 0, 'fos_user_security_login', 'login', '/login', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-08-19 11:46:36'),
(6, 1, 1, NULL, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 0, 'fos_user_security_check', 'login-check', '/login_check', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(7, 1, 1, NULL, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 0, 'fos_user_security_logout', 'logout', '/logout', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(8, 1, 1, NULL, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 0, 'fos_user_profile_show', 'profile', '/profile/', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(9, 1, 1, NULL, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 0, 'fos_user_profile_edit', 'profile-edit', '/profile/edit', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(10, 1, 1, NULL, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 0, 'fos_user_registration_register', 'register', '/register/', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(11, 1, 1, NULL, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 0, 'fos_user_registration_check_email', 'register-check-email', '/register/check-email', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(12, 1, 1, NULL, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 0, 'fos_user_registration_confirm', 'register-confirm-token', '/register/confirm/{token}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(13, 1, 1, NULL, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 0, 'fos_user_registration_confirmed', 'register-confirmed', '/register/confirmed', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(14, 1, 1, NULL, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 0, 'fos_user_resetting_request', 'resetting-request', '/resetting/request', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(15, 1, 1, NULL, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 0, 'fos_user_resetting_send_email', 'resetting-send-email', '/resetting/send-email', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(16, 1, 1, NULL, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 0, 'fos_user_resetting_check_email', 'resetting-check-email', '/resetting/check-email', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(17, 1, 1, NULL, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 0, 'fos_user_resetting_reset', 'resetting-reset-token', '/resetting/reset/{token}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(18, 1, 1, NULL, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 0, 'fos_user_change_password', 'profile-change-password', '/profile/change-password', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 22:59:25'),
(19, 1, 1, NULL, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 0, 'hwi_oauth_connect', 'login', '/login/', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(20, 1, 1, NULL, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 0, 'hwi_oauth_connect_service', 'login-service-service', '/login/service/{service}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(21, 1, 1, NULL, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 0, 'hwi_oauth_connect_registration', 'login-registration-key', '/login/registration/{key}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(22, 1, 1, NULL, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 0, 'hwi_oauth_service_redirect', 'login-service', '/login/{service}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(23, 1, 1, NULL, 'facebook_login', NULL, NULL, 1, 1, 1, 0, 'facebook_login', 'login-check-facebook', '/login/check-facebook', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(24, 1, 1, NULL, 'google_login', NULL, NULL, 1, 1, 1, 0, 'google_login', 'login-check-google', '/login/check-google', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(25, 1, 1, NULL, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 0, 'sonata_page_exceptions_list', 'exceptions-list', '/exceptions/list', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(26, 1, 1, NULL, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 0, 'sonata_page_exceptions_edit', 'exceptions-edit-code', '/exceptions/edit/{code}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(27, 1, NULL, NULL, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, 0, '_page_internal_error_not_found', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(28, 1, NULL, NULL, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, 0, '_page_internal_error_fatal', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-07-24 18:20:00', '2015-07-24 18:20:11'),
(29, 1, 1, NULL, 'ye_site_homepage', NULL, NULL, 1, 1, 1, 0, 'ye_site_homepage', '', '/', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-12 23:23:55', '2015-08-12 23:24:39'),
(30, 1, 1, NULL, 'sonata_media_gallery_index', NULL, NULL, 1, 1, 1, 0, 'sonata_media_gallery_index', 'media-gallery', '/media/gallery/', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-12 23:23:55', '2015-08-12 23:24:39'),
(31, 1, 1, NULL, 'sonata_media_gallery_view', NULL, NULL, 1, 1, 1, 0, 'sonata_media_gallery_view', 'media-gallery-view-id', '/media/gallery/view/{id}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-12 23:23:55', '2015-08-12 23:24:39'),
(32, 1, 1, NULL, 'sonata_media_view', NULL, NULL, 1, 1, 1, 0, 'sonata_media_view', 'media-view-id-format', '/media/view/{id}/{format}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-12 23:23:55', '2015-08-12 23:24:39'),
(33, 1, 1, NULL, 'sonata_media_download', NULL, NULL, 1, 1, 1, 0, 'sonata_media_download', 'media-download-id-format', '/media/download/{id}/{format}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-12 23:23:55', '2015-08-12 23:24:39'),
(34, 1, 1, NULL, 'sonata_cache_symfony', NULL, NULL, 1, 1, 1, 0, 'sonata_cache_symfony', 'sonata-cache-symfony-token-type', '/sonata/cache/symfony/{token}/{type}', NULL, 'GET|POST|HEAD|DELETE|PUT', NULL, NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-13 09:38:06', '2015-08-13 09:38:27'),
(35, 1, 1, NULL, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 0, 'Test', 'test', '/test', 'test', 'GET|POST|HEAD|DELETE|PUT', 'test', NULL, NULL, NULL, NULL, NULL, 'default', '2015-08-16 20:49:03', '2015-08-19 11:01:53');

-- --------------------------------------------------------

--
-- Structure de la table `page__site`
--

DROP TABLE IF EXISTS `page__site`;
CREATE TABLE `page__site` (
  `id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relative_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled_from` datetime DEFAULT NULL,
  `enabled_to` datetime DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `locale` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `page__site`
--

INSERT INTO `page__site` (`id`, `enabled`, `name`, `relative_path`, `host`, `enabled_from`, `enabled_to`, `is_default`, `created_at`, `updated_at`, `locale`, `title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 'localhost', NULL, 'localhost', '2015-07-24 18:19:34', '2025-07-24 18:19:34', 1, '2015-07-24 18:19:40', '2015-07-26 01:28:30', 'en', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `page__snapshot`
--

DROP TABLE IF EXISTS `page__snapshot`;
CREATE TABLE `page__snapshot` (
  `id` int(11) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `decorate` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8_unicode_ci,
  `parent_id` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `publication_date_start` datetime DEFAULT NULL,
  `publication_date_end` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=470 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `page__snapshot`
--

INSERT INTO `page__snapshot` (`id`, `site_id`, `page_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `name`, `url`, `parent_id`, `target_id`, `content`, `publication_date_start`, `publication_date_end`, `created_at`, `updated_at`) VALUES
(195, 1, 1, 'page_slug', NULL, NULL, 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762010","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 20:31:51', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(197, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-23 00:59:28', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(198, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-19 11:46:36', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(199, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:08', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(200, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:08', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(201, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(202, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(203, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(204, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(205, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(206, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(207, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(208, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(209, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(210, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(211, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(212, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(213, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(214, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(215, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(216, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(217, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(218, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(219, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(220, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-04 22:41:06', '2015-08-17 18:48:09', '2015-08-04 22:41:06', '2015-08-04 22:41:06'),
(221, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-04 22:41:07', '2015-08-17 18:48:09', '2015-08-04 22:41:07', '2015-08-04 22:41:07'),
(222, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439323362","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-12 17:20:57', '2015-08-17 20:31:51', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(223, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]}]}', '2015-08-12 17:20:57', '2015-08-23 00:59:28', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(224, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-19 11:46:36', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(225, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:08', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(226, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:08', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(227, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(228, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(229, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(230, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(231, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(232, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(233, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(234, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(235, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(236, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(237, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(238, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(239, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(240, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(241, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(242, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(243, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(244, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(245, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(246, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(247, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-12 17:20:57', '2015-08-17 18:48:09', '2015-08-12 17:20:57', '2015-08-12 17:20:57'),
(248, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439400057","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-12 23:23:29', '2015-08-17 20:31:51', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(249, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]}]}', '2015-08-12 23:23:29', '2015-08-23 00:59:28', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(250, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-19 11:46:36', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(251, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:08', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(252, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:08', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(253, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(254, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(255, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(256, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(257, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(258, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(259, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(260, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(261, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(262, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(263, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(264, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(265, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(266, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:29'),
(267, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:29', '2015-08-17 18:48:09', '2015-08-12 23:23:29', '2015-08-12 23:23:30'),
(268, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:30', '2015-08-17 18:48:09', '2015-08-12 23:23:30', '2015-08-12 23:23:30'),
(269, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:30', '2015-08-17 18:48:09', '2015-08-12 23:23:30', '2015-08-12 23:23:30'),
(270, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:30', '2015-08-17 18:48:09', '2015-08-12 23:23:30', '2015-08-12 23:23:30'),
(271, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:23:30', '2015-08-17 18:48:09', '2015-08-12 23:23:30', '2015-08-12 23:23:30'),
(272, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-12 23:23:30', '2015-08-17 18:48:09', '2015-08-12 23:23:30', '2015-08-12 23:23:30'),
(273, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-12 23:23:30', '2015-08-17 18:48:09', '2015-08-12 23:23:30', '2015-08-12 23:23:30'),
(274, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439400057","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-12 23:24:39', '2015-08-17 20:31:51', '2015-08-12 23:24:39', '2015-08-12 23:24:39');
INSERT INTO `page__snapshot` (`id`, `site_id`, `page_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `name`, `url`, `parent_id`, `target_id`, `content`, `publication_date_start`, `publication_date_end`, `created_at`, `updated_at`) VALUES
(275, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]}]}', '2015-08-12 23:24:39', '2015-08-23 00:59:28', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(276, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-19 11:46:36', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(277, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:08', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(278, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:08', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(279, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(280, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(281, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(282, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(283, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(284, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(285, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(286, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(287, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(288, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(289, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(290, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(291, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(292, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(293, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(294, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(295, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(296, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(297, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(298, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(299, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(300, 1, 29, 'ye_site_homepage', NULL, NULL, 1, 1, 1, 'ye_site_homepage', '/', 1, NULL, '{"id":29,"name":"ye_site_homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421835","slug":"","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(301, 1, 30, 'sonata_media_gallery_index', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_index', '/media/gallery/', 1, NULL, '{"id":30,"name":"sonata_media_gallery_index","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421835","slug":"media-gallery","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(302, 1, 31, 'sonata_media_gallery_view', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_view', '/media/gallery/view/{id}', 1, NULL, '{"id":31,"name":"sonata_media_gallery_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421835","slug":"media-gallery-view-id","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(303, 1, 32, 'sonata_media_view', NULL, NULL, 1, 1, 1, 'sonata_media_view', '/media/view/{id}/{format}', 1, NULL, '{"id":32,"name":"sonata_media_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421835","slug":"media-view-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(304, 1, 33, 'sonata_media_download', NULL, NULL, 1, 1, 1, 'sonata_media_download', '/media/download/{id}/{format}', 1, NULL, '{"id":33,"name":"sonata_media_download","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421835","slug":"media-download-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-12 23:24:39', '2015-08-17 18:48:09', '2015-08-12 23:24:39', '2015-08-12 23:24:39'),
(305, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439400057","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-13 09:38:27', '2015-08-17 20:31:51', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(306, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]}]}', '2015-08-13 09:38:27', '2015-08-23 00:59:28', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(307, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-19 11:46:36', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(308, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:08', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(309, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:08', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(310, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(311, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(312, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(313, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(314, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(315, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(316, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(317, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(318, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(319, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(320, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(321, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(322, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(323, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(324, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(325, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(326, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(327, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(328, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(329, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(330, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(331, 1, 29, 'ye_site_homepage', NULL, NULL, 1, 1, 1, 'ye_site_homepage', '/', 1, NULL, '{"id":29,"name":"ye_site_homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(332, 1, 30, 'sonata_media_gallery_index', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_index', '/media/gallery/', 1, NULL, '{"id":30,"name":"sonata_media_gallery_index","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(333, 1, 31, 'sonata_media_gallery_view', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_view', '/media/gallery/view/{id}', 1, NULL, '{"id":31,"name":"sonata_media_gallery_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery-view-id","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(334, 1, 32, 'sonata_media_view', NULL, NULL, 1, 1, 1, 'sonata_media_view', '/media/view/{id}/{format}', 1, NULL, '{"id":32,"name":"sonata_media_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-view-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(335, 1, 33, 'sonata_media_download', NULL, NULL, 1, 1, 1, 'sonata_media_download', '/media/download/{id}/{format}', 1, NULL, '{"id":33,"name":"sonata_media_download","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-download-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(336, 1, 34, 'sonata_cache_symfony', NULL, NULL, 1, 1, 1, 'sonata_cache_symfony', '/sonata/cache/symfony/{token}/{type}', 1, NULL, '{"id":34,"name":"sonata_cache_symfony","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439458686","updated_at":"1439458686","slug":"sonata-cache-symfony-token-type","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:27', '2015-08-17 18:48:09', '2015-08-13 09:38:27', '2015-08-13 09:38:27'),
(337, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439400057","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-13 09:38:39', '2015-08-17 20:31:51', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(338, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]}]}', '2015-08-13 09:38:39', '2015-08-23 00:59:28', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(339, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-19 11:46:36', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(340, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:08', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(341, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:08', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(342, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:09', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(343, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:09', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(344, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:09', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(345, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:09', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(346, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:09', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(347, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:39', '2015-08-17 18:48:09', '2015-08-13 09:38:39', '2015-08-13 09:38:39'),
(348, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(349, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(350, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(351, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(352, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(353, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(354, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40');
INSERT INTO `page__snapshot` (`id`, `site_id`, `page_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `name`, `url`, `parent_id`, `target_id`, `content`, `publication_date_start`, `publication_date_end`, `created_at`, `updated_at`) VALUES
(355, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(356, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(357, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(358, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(359, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(360, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(361, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(362, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(363, 1, 29, 'ye_site_homepage', NULL, NULL, 1, 1, 1, 'ye_site_homepage', '/', 1, NULL, '{"id":29,"name":"ye_site_homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(364, 1, 30, 'sonata_media_gallery_index', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_index', '/media/gallery/', 1, NULL, '{"id":30,"name":"sonata_media_gallery_index","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(365, 1, 31, 'sonata_media_gallery_view', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_view', '/media/gallery/view/{id}', 1, NULL, '{"id":31,"name":"sonata_media_gallery_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery-view-id","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(366, 1, 32, 'sonata_media_view', NULL, NULL, 1, 1, 1, 'sonata_media_view', '/media/view/{id}/{format}', 1, NULL, '{"id":32,"name":"sonata_media_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-view-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(367, 1, 33, 'sonata_media_download', NULL, NULL, 1, 1, 1, 'sonata_media_download', '/media/download/{id}/{format}', 1, NULL, '{"id":33,"name":"sonata_media_download","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-download-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(368, 1, 34, 'sonata_cache_symfony', NULL, NULL, 1, 1, 1, 'sonata_cache_symfony', '/sonata/cache/symfony/{token}/{type}', 1, NULL, '{"id":34,"name":"sonata_cache_symfony","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439458686","updated_at":"1439458707","slug":"sonata-cache-symfony-token-type","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:38:40', '2015-08-17 18:48:09', '2015-08-13 09:38:40', '2015-08-13 09:38:40'),
(369, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439400057","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-13 09:41:58', '2015-08-17 20:31:51', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(370, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]}]}', '2015-08-13 09:41:58', '2015-08-23 00:59:28', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(371, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:58', '2015-08-19 11:46:36', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(372, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:58', '2015-08-17 18:48:08', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(373, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:58', '2015-08-17 18:48:08', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(374, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:58', '2015-08-17 18:48:09', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(375, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:58', '2015-08-17 18:48:09', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(376, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:58', '2015-08-17 18:48:09', '2015-08-13 09:41:58', '2015-08-13 09:41:58'),
(377, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(378, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(379, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(380, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(381, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(382, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(383, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(384, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(385, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(386, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(387, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(388, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(389, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(390, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(391, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(392, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(393, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(394, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(395, 1, 29, 'ye_site_homepage', NULL, NULL, 1, 1, 1, 'ye_site_homepage', '/', 1, NULL, '{"id":29,"name":"ye_site_homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(396, 1, 30, 'sonata_media_gallery_index', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_index', '/media/gallery/', 1, NULL, '{"id":30,"name":"sonata_media_gallery_index","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(397, 1, 31, 'sonata_media_gallery_view', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_view', '/media/gallery/view/{id}', 1, NULL, '{"id":31,"name":"sonata_media_gallery_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery-view-id","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(398, 1, 32, 'sonata_media_view', NULL, NULL, 1, 1, 1, 'sonata_media_view', '/media/view/{id}/{format}', 1, NULL, '{"id":32,"name":"sonata_media_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-view-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(399, 1, 33, 'sonata_media_download', NULL, NULL, 1, 1, 1, 'sonata_media_download', '/media/download/{id}/{format}', 1, NULL, '{"id":33,"name":"sonata_media_download","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-download-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(400, 1, 34, 'sonata_cache_symfony', NULL, NULL, 1, 1, 1, 'sonata_cache_symfony', '/sonata/cache/symfony/{token}/{type}', 1, NULL, '{"id":34,"name":"sonata_cache_symfony","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439458686","updated_at":"1439458707","slug":"sonata-cache-symfony-token-type","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-13 09:41:59', '2015-08-17 18:48:09', '2015-08-13 09:41:59', '2015-08-13 09:41:59'),
(401, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, 1, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439758185","slug":"test","parent_id":1,"target_id":1,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":29,"name":"test","enabled":true,"position":1,"settings":{"format":"richhtml","rawContent":"<b>Test<\\/b>","content":"<b>Test<\\/b>"},"type":"sonata.formatter.block.formatter","created_at":"1439837129","updated_at":"1439837129","blocks":[]}]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-17 18:46:23', '2015-08-19 11:01:53', '2015-08-17 18:46:23', '2015-08-17 18:49:30'),
(402, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439670661","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[{"id":21,"name":"test","enabled":true,"position":1,"settings":{"title":"test","current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439670661","updated_at":"1439670661","blocks":[]}]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-17 18:48:08', '2015-08-17 20:31:51', '2015-08-17 18:48:08', '2015-08-17 18:48:08'),
(403, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', 1, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439756558","slug":"dashboard","parent_id":1,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":22,"name":"Left content","enabled":true,"position":1,"settings":{"code":"left_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]},{"id":23,"name":"Right content","enabled":true,"position":1,"settings":{"code":"right_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]}]}', '2015-08-17 18:48:08', '2015-08-23 00:59:28', '2015-08-17 18:48:08', '2015-08-17 18:48:08'),
(404, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', 1, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:08', '2015-08-19 11:46:36', '2015-08-17 18:48:08', '2015-08-17 18:48:08'),
(405, 1, 6, 'fos_user_security_check', NULL, NULL, 1, 1, 1, 'fos_user_security_check', '/login_check', 1, NULL, '{"id":6,"name":"fos_user_security_check","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"login-check","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:08', NULL, '2015-08-17 18:48:08', '2015-08-17 18:48:08'),
(406, 1, 7, 'fos_user_security_logout', NULL, NULL, 1, 1, 1, 'fos_user_security_logout', '/logout', 1, NULL, '{"id":7,"name":"fos_user_security_logout","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"logout","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:08', NULL, '2015-08-17 18:48:08', '2015-08-17 18:48:09'),
(407, 1, 8, 'fos_user_profile_show', NULL, NULL, 1, 1, 1, 'fos_user_profile_show', '/profile/', 1, NULL, '{"id":8,"name":"fos_user_profile_show","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(408, 1, 9, 'fos_user_profile_edit', NULL, NULL, 1, 1, 1, 'fos_user_profile_edit', '/profile/edit', 1, NULL, '{"id":9,"name":"fos_user_profile_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-edit","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(409, 1, 10, 'fos_user_registration_register', NULL, NULL, 1, 1, 1, 'fos_user_registration_register', '/register/', 1, NULL, '{"id":10,"name":"fos_user_registration_register","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(410, 1, 11, 'fos_user_registration_check_email', NULL, NULL, 1, 1, 1, 'fos_user_registration_check_email', '/register/check-email', 1, NULL, '{"id":11,"name":"fos_user_registration_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(411, 1, 12, 'fos_user_registration_confirm', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirm', '/register/confirm/{token}', 1, NULL, '{"id":12,"name":"fos_user_registration_confirm","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirm-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(412, 1, 13, 'fos_user_registration_confirmed', NULL, NULL, 1, 1, 1, 'fos_user_registration_confirmed', '/register/confirmed', 1, NULL, '{"id":13,"name":"fos_user_registration_confirmed","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"register-confirmed","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(413, 1, 14, 'fos_user_resetting_request', NULL, NULL, 1, 1, 1, 'fos_user_resetting_request', '/resetting/request', 1, NULL, '{"id":14,"name":"fos_user_resetting_request","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-request","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(414, 1, 15, 'fos_user_resetting_send_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_send_email', '/resetting/send-email', 1, NULL, '{"id":15,"name":"fos_user_resetting_send_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-send-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(415, 1, 16, 'fos_user_resetting_check_email', NULL, NULL, 1, 1, 1, 'fos_user_resetting_check_email', '/resetting/check-email', 1, NULL, '{"id":16,"name":"fos_user_resetting_check_email","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-check-email","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(416, 1, 17, 'fos_user_resetting_reset', NULL, NULL, 1, 1, 1, 'fos_user_resetting_reset', '/resetting/reset/{token}', 1, NULL, '{"id":17,"name":"fos_user_resetting_reset","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"resetting-reset-token","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(417, 1, 18, 'fos_user_change_password', NULL, NULL, 1, 1, 1, 'fos_user_change_password', '/profile/change-password', 1, NULL, '{"id":18,"name":"fos_user_change_password","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437778765","slug":"profile-change-password","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(418, 1, 19, 'hwi_oauth_connect', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect', '/login/', 1, NULL, '{"id":19,"name":"hwi_oauth_connect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(419, 1, 20, 'hwi_oauth_connect_service', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_service', '/login/service/{service}', 1, NULL, '{"id":20,"name":"hwi_oauth_connect_service","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(420, 1, 21, 'hwi_oauth_connect_registration', NULL, NULL, 1, 1, 1, 'hwi_oauth_connect_registration', '/login/registration/{key}', 1, NULL, '{"id":21,"name":"hwi_oauth_connect_registration","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-registration-key","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(421, 1, 22, 'hwi_oauth_service_redirect', NULL, NULL, 1, 1, 1, 'hwi_oauth_service_redirect', '/login/{service}', 1, NULL, '{"id":22,"name":"hwi_oauth_service_redirect","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-service","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(422, 1, 23, 'facebook_login', NULL, NULL, 1, 1, 1, 'facebook_login', '/login/check-facebook', 1, NULL, '{"id":23,"name":"facebook_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-facebook","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(423, 1, 24, 'google_login', NULL, NULL, 1, 1, 1, 'google_login', '/login/check-google', 1, NULL, '{"id":24,"name":"google_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"login-check-google","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(424, 1, 25, 'sonata_page_exceptions_list', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_list', '/exceptions/list', 1, NULL, '{"id":25,"name":"sonata_page_exceptions_list","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-list","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(425, 1, 26, 'sonata_page_exceptions_edit', NULL, NULL, 1, 1, 1, 'sonata_page_exceptions_edit', '/exceptions/edit/{code}', 1, NULL, '{"id":26,"name":"sonata_page_exceptions_edit","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1437762011","slug":"exceptions-edit-code","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(426, 1, 27, '_page_internal_error_not_found', NULL, NULL, 1, 1, 0, '_page_internal_error_not_found', NULL, NULL, NULL, '{"id":27,"name":"_page_internal_error_not_found","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(427, 1, 28, '_page_internal_error_fatal', NULL, NULL, 1, 1, 0, '_page_internal_error_fatal', NULL, NULL, NULL, '{"id":28,"name":"_page_internal_error_fatal","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":null,"created_at":"1437762000","updated_at":"1437762011","slug":"","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(428, 1, 29, 'ye_site_homepage', NULL, NULL, 1, 1, 1, 'ye_site_homepage', '/', 1, NULL, '{"id":29,"name":"ye_site_homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(429, 1, 30, 'sonata_media_gallery_index', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_index', '/media/gallery/', 1, NULL, '{"id":30,"name":"sonata_media_gallery_index","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(430, 1, 31, 'sonata_media_gallery_view', NULL, NULL, 1, 1, 1, 'sonata_media_gallery_view', '/media/gallery/view/{id}', 1, NULL, '{"id":31,"name":"sonata_media_gallery_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-gallery-view-id","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(431, 1, 32, 'sonata_media_view', NULL, NULL, 1, 1, 1, 'sonata_media_view', '/media/view/{id}/{format}', 1, NULL, '{"id":32,"name":"sonata_media_view","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-view-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(432, 1, 33, 'sonata_media_download', NULL, NULL, 1, 1, 1, 'sonata_media_download', '/media/download/{id}/{format}', 1, NULL, '{"id":33,"name":"sonata_media_download","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439421835","updated_at":"1439421879","slug":"media-download-id-format","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09'),
(433, 1, 34, 'sonata_cache_symfony', NULL, NULL, 1, 1, 1, 'sonata_cache_symfony', '/sonata/cache/symfony/{token}/{type}', 1, NULL, '{"id":34,"name":"sonata_cache_symfony","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439458686","updated_at":"1439458707","slug":"sonata-cache-symfony-token-type","parent_id":1,"target_id":null,"blocks":[]}', '2015-08-17 18:48:09', NULL, '2015-08-17 18:48:09', '2015-08-17 18:48:09');
INSERT INTO `page__snapshot` (`id`, `site_id`, `page_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `name`, `url`, `parent_id`, `target_id`, `content`, `publication_date_start`, `publication_date_end`, `created_at`, `updated_at`) VALUES
(435, 1, 1, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Homepage', '/', NULL, NULL, '{"id":1,"name":"Homepage","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439843301","slug":"","parent_id":null,"target_id":null,"blocks":[{"id":16,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[{"id":21,"name":"test","enabled":true,"position":1,"settings":{"title":"test","current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439670661","updated_at":"1439670661","blocks":[]}]},{"id":17,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":18,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[{"id":30,"name":"dadaz","enabled":true,"position":1,"settings":{"format":"richhtml","rawContent":"<p><strong>dzadazd<\\/strong><\\/p>","content":"<p><strong>dzadazd<\\/strong><\\/p>","template":"SonataFormatterBundle:Block:block_formatter.html.twig"},"type":"sonata.formatter.block.formatter","created_at":"1439843301","updated_at":"1439843329","blocks":[]}]},{"id":19,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]},{"id":20,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439323697","updated_at":"1439323697","blocks":[]}]}', '2015-08-17 20:31:51', NULL, '2015-08-17 20:31:51', '2015-08-17 20:31:51'),
(436, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439943138","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:12:46', '2015-08-19 11:01:53', '2015-08-19 00:12:46', '2015-08-19 00:20:18'),
(438, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439943553","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":false,"position":1,"settings":{"layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig","code":"header"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943553","blocks":[]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439943419","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:20:42', '2015-08-19 11:01:53', '2015-08-19 00:20:42', '2015-08-19 00:20:42'),
(439, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439943723","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":false,"position":1,"settings":{"layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig","code":"header"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943553","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439943419","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:24:51', '2015-08-19 11:01:53', '2015-08-19 00:24:51', '2015-08-19 00:24:51'),
(440, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439943982","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439943419","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:26:34', '2015-08-19 11:01:53', '2015-08-19 00:26:34', '2015-08-19 00:26:34'),
(441, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944065","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"mediaId":8,"title":"Aaa","format":null},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944261","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:31:17', '2015-08-19 11:01:53', '2015-08-19 00:31:17', '2015-08-19 00:31:17'),
(442, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944345","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":"1","position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:35:36', '2015-08-19 11:01:53', '2015-08-19 00:35:36', '2015-08-19 00:35:36'),
(443, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944602","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":"1","position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:36:42', '2015-08-19 11:01:53', '2015-08-19 00:36:42', '2015-08-19 00:36:42'),
(444, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944632","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":36,"name":null,"enabled":"1","position":4,"settings":{"title":null,"cache_policy":0,"menu_name":null,"safe_labels":false,"current_class":null,"first_class":null,"last_class":null,"menu_class":null,"children_class":null,"menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944632","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:37:12', '2015-08-19 11:01:53', '2015-08-19 00:37:12', '2015-08-19 00:37:12'),
(445, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944650","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":36,"name":null,"enabled":"1","position":4,"settings":{"template":"SonataBlockBundle:Block:block_core_menu.html.twig","current_uri":null,"include_homepage_link":true,"title":"aaa","cache_policy":0,"menu_name":null,"safe_labels":false,"current_class":null,"first_class":null,"last_class":null,"menu_class":null,"children_class":null,"menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944650","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:37:30', '2015-08-19 11:01:53', '2015-08-19 00:37:30', '2015-08-19 00:37:30'),
(446, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944671","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":36,"name":null,"enabled":true,"position":4,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":false,"current_class":null,"first_class":null,"last_class":null,"current_uri":null,"menu_class":null,"children_class":null,"menu_template":"dd","include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944671","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:37:51', '2015-08-19 11:01:53', '2015-08-19 00:37:51', '2015-08-19 00:37:51'),
(447, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944715","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":4,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":false,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944715","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:38:35', '2015-08-19 11:01:53', '2015-08-19 00:38:35', '2015-08-19 00:38:35'),
(448, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944795","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":4,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944795","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:39:55', '2015-08-19 11:01:53', '2015-08-19 00:39:55', '2015-08-19 00:39:55'),
(449, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944831","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:40:31', '2015-08-19 11:01:53', '2015-08-19 00:40:31', '2015-08-19 00:40:31'),
(450, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944930","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":"1","position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:42:10', '2015-08-19 11:01:53', '2015-08-19 00:42:10', '2015-08-19 00:42:10');
INSERT INTO `page__snapshot` (`id`, `site_id`, `page_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `name`, `url`, `parent_id`, `target_id`, `content`, `publication_date_start`, `publication_date_end`, `created_at`, `updated_at`) VALUES
(451, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439944991","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":38,"name":null,"enabled":"1","position":6,"settings":{"url":"https:\\/\\/www.facebook.com\\/WildChildForLife","width":31,"height":31,"colorscheme":"light","show_faces":true,"show_header":true,"show_posts":true,"show_border":true},"type":"sonata.seo.block.facebook.like_box","created_at":"1439944991","updated_at":"1439944991","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:43:11', '2015-08-19 11:01:53', '2015-08-19 00:43:11', '2015-08-19 00:43:11'),
(452, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945041","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":38,"name":null,"enabled":"1","position":6,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_box.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":31,"height":31,"colorscheme":"light","show_faces":true,"show_header":true,"show_posts":true,"show_border":true},"type":"sonata.seo.block.facebook.like_box","created_at":"1439944991","updated_at":"1439945041","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:44:01', '2015-08-19 11:01:53', '2015-08-19 00:44:01', '2015-08-19 00:44:01'),
(453, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945074","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":38,"name":null,"enabled":true,"position":6,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_box.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":31,"height":31,"colorscheme":"light","show_faces":true,"show_header":true,"show_posts":true,"show_border":true},"type":"sonata.seo.block.facebook.like_box","created_at":"1439944991","updated_at":"1439945041","blocks":[]},{"id":39,"name":null,"enabled":"1","position":7,"settings":{"url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":13,"show_faces":true,"share":true,"layout":"standard","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945074","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:44:34', '2015-08-19 11:01:53', '2015-08-19 00:44:34', '2015-08-19 00:44:34'),
(454, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945112","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":"1","position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":13,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945112","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:45:12', '2015-08-19 11:01:53', '2015-08-19 00:45:12', '2015-08-19 00:45:12'),
(455, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945129","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":"1","position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:45:29', '2015-08-19 11:01:53', '2015-08-19 00:45:29', '2015-08-19 00:45:29'),
(456, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945168","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":"1","position":7,"settings":{"title":"dzadazd","content":"dzadzad","orientation":"left","mediaId":8,"format":null},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945168","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:46:08', '2015-08-19 11:01:53', '2015-08-19 00:46:08', '2015-08-19 00:46:08'),
(457, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945175","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":"1","position":7,"settings":{"media":false,"context":false,"template":"SonataMediaBundle:Block:block_feature_media.html.twig","orientation":"left","title":"dzadazd","content":"dzadzad","mediaId":8,"format":"default_big"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945175","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:46:15', '2015-08-19 11:01:53', '2015-08-19 00:46:15', '2015-08-19 00:46:15'),
(458, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439945191","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":"0","position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 00:46:31', '2015-08-19 11:01:53', '2015-08-19 00:46:31', '2015-08-19 00:46:31'),
(459, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439946312","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":false,"position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":41,"name":null,"enabled":"1","position":8,"settings":{"title":"b","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"a v","first_class":"fe","last_class":"fdf","menu_class":"kjk","children_class":"fdkfjk","menu_template":null},"type":"sonata.block.service.menu","created_at":"1439946312","updated_at":"1439946312","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 01:05:12', '2015-08-19 11:01:53', '2015-08-19 01:05:12', '2015-08-19 01:05:12'),
(460, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439947094","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":false,"position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":41,"name":null,"enabled":true,"position":8,"settings":{"title":"b","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"a v","first_class":"fe","last_class":"fdf","menu_class":"kjk","children_class":"fdkfjk","menu_template":null},"type":"sonata.block.service.menu","created_at":"1439946312","updated_at":"1439946312","blocks":[]},{"id":42,"name":null,"enabled":"1","position":9,"settings":{"title":"a","max_per_page":10},"type":"sonata.timeline.block.timeline","created_at":"1439947094","updated_at":"1439947094","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 01:18:14', '2015-08-19 11:01:53', '2015-08-19 01:18:14', '2015-08-19 01:18:14');
INSERT INTO `page__snapshot` (`id`, `site_id`, `page_id`, `route_name`, `page_alias`, `type`, `position`, `enabled`, `decorate`, `name`, `url`, `parent_id`, `target_id`, `content`, `publication_date_start`, `publication_date_end`, `created_at`, `updated_at`) VALUES
(461, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439947112","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":false,"position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":41,"name":null,"enabled":"0","position":8,"settings":{"template":"SonataBlockBundle:Block:block_core_menu.html.twig","current_uri":null,"title":"b","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"a v","first_class":"fe","last_class":"fdf","menu_class":"kjk","children_class":"fdkfjk","menu_template":null},"type":"sonata.block.service.menu","created_at":"1439946312","updated_at":"1439947112","blocks":[]},{"id":42,"name":null,"enabled":true,"position":9,"settings":{"title":"a","max_per_page":10},"type":"sonata.timeline.block.timeline","created_at":"1439947094","updated_at":"1439947094","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 01:18:33', '2015-08-19 11:01:53', '2015-08-19 01:18:33', '2015-08-19 01:18:33'),
(462, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439947164","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":false,"position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":42,"name":null,"enabled":"1","position":9,"settings":{"template":"SonataTimelineBundle:Block:timeline.html.twig","context":"GLOBAL","filter":true,"group_by_action":true,"paginate":true,"max_per_page":10,"title":"a"},"type":"sonata.timeline.block.timeline","created_at":"1439947094","updated_at":"1439947164","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 01:19:24', '2015-08-19 11:01:53', '2015-08-19 01:19:24', '2015-08-19 01:19:24'),
(463, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439947222","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":false,"position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":42,"name":null,"enabled":true,"position":9,"settings":{"template":"SonataTimelineBundle:Block:timeline.html.twig","context":"GLOBAL","filter":true,"group_by_action":true,"paginate":true,"max_per_page":10,"title":"a"},"type":"sonata.timeline.block.timeline","created_at":"1439947094","updated_at":"1439947164","blocks":[]},{"id":43,"name":"fr","enabled":"1","position":9,"settings":[],"type":"sonata_translation.block.locale_switcher","created_at":"1439947222","updated_at":"1439947222","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 01:20:22', '2015-08-19 11:01:53', '2015-08-19 01:20:22', '2015-08-19 01:20:22'),
(464, 1, 35, 'page_slug', NULL, 'sonata.page.service.default', 1, 1, 1, 'Test', '/test', 1, NULL, '{"id":35,"name":"Test","javascript":null,"stylesheet":null,"raw_headers":null,"title":"test","meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1439758143","updated_at":"1439982113","slug":"test","parent_id":1,"target_id":null,"blocks":[{"id":24,"name":"Header","enabled":true,"position":1,"settings":{"code":"header","layout":"{{ CONTENT }}","class":"","template":"SonataPageBundle:Block:block_container.html.twig"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439943982","blocks":[{"id":32,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p><strong>aaaaaaaaa<\\/strong><\\/p>","content":"<p><strong>aaaaaaaaa<\\/strong><\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943723","updated_at":"1439943735","blocks":[]},{"id":33,"name":null,"enabled":true,"position":2,"settings":{"title":"Test","cache_policy":0,"menu_name":null,"safe_labels":true,"current_class":"tes","first_class":"aaa","last_class":"ccc","menu_class":"aaaacc","children_class":"ffff","menu_template":null},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439943880","updated_at":"1439943880","blocks":[]}]},{"id":25,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":26,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[{"id":31,"name":null,"enabled":true,"position":1,"settings":{"template":"SonataFormatterBundle:Block:block_formatter.html.twig","format":"richhtml","rawContent":"<p>Bonjour,&nbsp;<\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n\\r\\n<p>Ceci est un test.<\\/p>","content":"<p>Bonjour,&nbsp;<\\/p>\\n\\n<p>&nbsp;<\\/p>\\n\\n<p>Ceci est un test.<\\/p>"},"type":"sonata.formatter.block.formatter","created_at":"1439943419","updated_at":"1439944065","blocks":[]},{"id":34,"name":null,"enabled":true,"position":2,"settings":{"media":false,"title":"Aaa","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_media.html.twig"},"type":"sonata.media.block.media","created_at":"1439944261","updated_at":"1439944536","blocks":[]},{"id":35,"name":null,"enabled":true,"position":3,"settings":{"title":null,"current":false,"pageId":null,"class":null},"type":"sonata.page.block.children_pages","created_at":"1439944602","updated_at":"1439944602","blocks":[]},{"id":37,"name":"salut","enabled":true,"position":5,"settings":{"subject":"hehe","body":"mouahaha"},"type":"sonata.seo.block.email.share_button","created_at":"1439944930","updated_at":"1439944930","blocks":[]},{"id":39,"name":null,"enabled":true,"position":7,"settings":{"template":"SonataSeoBundle:Block:block_facebook_like_button.html.twig","url":"https:\\/\\/www.facebook.com\\/HighDesign.ma?ref=hl","width":159,"show_faces":true,"share":true,"layout":"button","colorscheme":"light","action":"like"},"type":"sonata.seo.block.facebook.like_button","created_at":"1439945074","updated_at":"1439945129","blocks":[]},{"id":40,"name":null,"enabled":false,"position":7,"settings":{"media":false,"orientation":"left","title":"dzadazd","content":"dzadzad","context":false,"mediaId":8,"format":"default_big","template":"SonataMediaBundle:Block:block_feature_media.html.twig"},"type":"sonata.media.block.feature_media","created_at":"1439945168","updated_at":"1439945191","blocks":[]},{"id":42,"name":null,"enabled":true,"position":9,"settings":{"template":"SonataTimelineBundle:Block:timeline.html.twig","context":"GLOBAL","filter":true,"group_by_action":true,"paginate":true,"max_per_page":10,"title":"a"},"type":"sonata.timeline.block.timeline","created_at":"1439947094","updated_at":"1439947164","blocks":[]},{"id":43,"name":"fr","enabled":true,"position":9,"settings":[],"type":"sonata_translation.block.locale_switcher","created_at":"1439947222","updated_at":"1439947222","blocks":[]},{"id":44,"name":null,"enabled":"1","position":10,"settings":{"number":10,"title":"2","mode":"public"},"type":"sonata.news.block.recent_posts","created_at":"1439982113","updated_at":"1439982113","blocks":[]},{"id":36,"name":"aaaaa","enabled":true,"position":28,"settings":{"title":"aaa","cache_policy":0,"template":"SonataBlockBundle:Block:block_core_menu.html.twig","menu_name":null,"safe_labels":true,"current_class":"a","first_class":"c","last_class":"v","current_uri":null,"menu_class":"g","children_class":"d","menu_template":null,"include_homepage_link":true},"type":"sonata.seo.block.breadcrumb.homepage","created_at":"1439944632","updated_at":"1439944831","blocks":[]}]},{"id":27,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]},{"id":28,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1439758196","updated_at":"1439758196","blocks":[]}]}', '2015-08-19 11:01:53', NULL, '2015-08-19 11:01:53', '2015-08-19 11:01:53'),
(465, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 1, 'fos_user_security_login', '/login', NULL, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439984770","slug":"login","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-19 11:46:10', '2015-08-19 11:46:36', '2015-08-19 11:46:10', '2015-08-19 11:46:10'),
(466, 1, 5, 'fos_user_security_login', NULL, NULL, 1, 1, 0, 'fos_user_security_login', '/login', NULL, NULL, '{"id":5,"name":"fos_user_security_login","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1439984796","slug":"login","parent_id":null,"target_id":null,"blocks":[]}', '2015-08-19 11:46:36', NULL, '2015-08-19 11:46:36', '2015-08-19 11:46:36'),
(467, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', NULL, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1440023184","slug":"dashboard","parent_id":null,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":22,"name":"Left content","enabled":true,"position":1,"settings":{"code":"left_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]},{"id":23,"name":"Right content","enabled":true,"position":1,"settings":{"code":"right_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]}]}', '2015-08-19 22:26:24', '2015-08-23 00:59:28', '2015-08-19 22:26:24', '2015-08-19 22:26:24'),
(468, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 1, 'ye_site_dashboard', '/dashboard', NULL, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"2columns","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1440023212","slug":"dashboard","parent_id":null,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":22,"name":"Left content","enabled":true,"position":1,"settings":{"code":"left_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]},{"id":23,"name":"Right content","enabled":true,"position":1,"settings":{"code":"right_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]}]}', '2015-08-19 22:26:52', '2015-08-23 00:59:28', '2015-08-19 22:26:52', '2015-08-19 22:26:52'),
(469, 1, 4, 'ye_site_dashboard', NULL, NULL, 1, 1, 0, 'ye_site_dashboard', '/dashboard', NULL, NULL, '{"id":4,"name":"ye_site_dashboard","javascript":null,"stylesheet":null,"raw_headers":null,"title":null,"meta_description":null,"meta_keyword":null,"template_code":"default","request_method":"GET|POST|HEAD|DELETE|PUT","created_at":"1437762000","updated_at":"1440291568","slug":"dashboard","parent_id":null,"target_id":null,"blocks":[{"id":11,"name":"Header","enabled":true,"position":1,"settings":{"code":"header"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":12,"name":"Top content","enabled":true,"position":1,"settings":{"code":"content_top"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":13,"name":"Main content","enabled":true,"position":1,"settings":{"code":"content"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":14,"name":"Bottom content","enabled":true,"position":1,"settings":{"code":"content_bottom"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":15,"name":"Footer","enabled":true,"position":1,"settings":{"code":"footer"},"type":"sonata.page.block.container","created_at":"1438826441","updated_at":"1438826441","blocks":[]},{"id":22,"name":"Left content","enabled":true,"position":1,"settings":{"code":"left_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]},{"id":23,"name":"Right content","enabled":true,"position":1,"settings":{"code":"right_col"},"type":"sonata.page.block.container","created_at":"1439740335","updated_at":"1439740335","blocks":[]}]}', '2015-08-23 00:59:28', NULL, '2015-08-23 00:59:28', '2015-08-23 00:59:28');

-- --------------------------------------------------------

--
-- Structure de la table `timeline__action`
--

DROP TABLE IF EXISTS `timeline__action`;
CREATE TABLE `timeline__action` (
  `id` int(11) NOT NULL,
  `verb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_current` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_wanted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duplicate_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duplicate_priority` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__action`
--

INSERT INTO `timeline__action` (`id`, `verb`, `status_current`, `status_wanted`, `duplicate_key`, `duplicate_priority`, `created_at`) VALUES
(1, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-15 20:31:01'),
(2, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-16 15:52:11'),
(3, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-16 20:22:38'),
(4, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-16 20:49:03'),
(5, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-16 20:49:45'),
(6, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-17 18:45:29'),
(7, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-17 18:46:23'),
(8, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-17 18:49:30'),
(9, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-17 20:28:21'),
(10, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-17 20:28:41'),
(11, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-17 20:28:49'),
(12, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-17 20:31:51'),
(13, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-17 20:31:59'),
(14, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-17 20:32:00'),
(15, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-18 22:27:13'),
(16, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-18 22:27:19'),
(17, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2015-08-18 22:27:23'),
(18, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:12:18'),
(19, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:12:46'),
(20, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:13:08'),
(21, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:16:59'),
(22, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:17:11'),
(23, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:17:19'),
(24, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:19:14'),
(25, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:20:18'),
(26, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:20:42'),
(27, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:20:45'),
(28, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:22:03'),
(29, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:22:15'),
(30, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:24:40'),
(31, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:24:51'),
(32, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:24:55'),
(33, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:26:22'),
(34, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:26:34'),
(35, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:26:38'),
(36, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:27:45'),
(37, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:30:04'),
(38, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:31:01'),
(39, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:31:17'),
(40, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:31:21'),
(41, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:31:22'),
(42, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:32:25'),
(43, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:35:36'),
(44, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:36:42'),
(45, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:37:12'),
(46, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:37:30'),
(47, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:37:51'),
(48, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:38:35'),
(49, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:39:55'),
(50, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:40:31'),
(51, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:42:10'),
(52, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:43:11'),
(53, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:44:01'),
(54, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:44:34'),
(55, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2015-08-19 00:45:01'),
(56, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:45:12'),
(57, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:45:29'),
(58, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 00:46:08'),
(59, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:46:15'),
(60, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 00:46:31'),
(61, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 01:05:12'),
(62, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 01:18:14'),
(63, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 01:18:33'),
(64, 'sonata.admin.delete', 'published', 'frozen', NULL, NULL, '2015-08-19 01:18:53'),
(65, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 01:19:24'),
(66, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 01:20:22'),
(67, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 01:23:31'),
(68, 'sonata.admin.create', 'published', 'frozen', NULL, NULL, '2015-08-19 11:01:53'),
(69, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 11:46:10'),
(70, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 11:46:36'),
(71, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 22:13:39'),
(72, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 22:26:24'),
(73, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-19 22:26:52'),
(74, 'sonata.admin.update', 'published', 'frozen', NULL, NULL, '2015-08-23 00:59:28');

-- --------------------------------------------------------

--
-- Structure de la table `timeline__action_component`
--

DROP TABLE IF EXISTS `timeline__action_component`;
CREATE TABLE `timeline__action_component` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__action_component`
--

INSERT INTO `timeline__action_component` (`id`, `action_id`, `component_id`, `type`, `text`) VALUES
(1, 1, 2, 'target', NULL),
(2, 1, NULL, 'target_text', 'test ~ #21'),
(3, 1, NULL, 'admin_code', 'sonata.page.admin.block'),
(4, 1, 1, 'subject', NULL),
(5, 2, 3, 'target', NULL),
(6, 2, NULL, 'target_text', 'ye_site_dashboard'),
(7, 2, NULL, 'admin_code', 'sonata.page.admin.page'),
(8, 2, 1, 'subject', NULL),
(9, 3, 3, 'target', NULL),
(10, 3, NULL, 'target_text', 'ye_site_dashboard'),
(11, 3, NULL, 'admin_code', 'sonata.page.admin.page'),
(12, 3, 1, 'subject', NULL),
(13, 4, 4, 'target', NULL),
(14, 4, NULL, 'target_text', 'Test'),
(15, 4, NULL, 'admin_code', 'sonata.page.admin.page'),
(16, 4, 1, 'subject', NULL),
(17, 5, 4, 'target', NULL),
(18, 5, NULL, 'target_text', 'Test'),
(19, 5, NULL, 'admin_code', 'sonata.page.admin.page'),
(20, 5, 1, 'subject', NULL),
(21, 6, 5, 'target', NULL),
(22, 6, NULL, 'target_text', 'test ~ #29'),
(23, 6, NULL, 'admin_code', 'sonata.page.admin.block'),
(24, 6, 1, 'subject', NULL),
(25, 7, 6, 'target', NULL),
(26, 7, NULL, 'target_text', 'Test'),
(27, 7, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(28, 7, 1, 'subject', NULL),
(29, 8, 6, 'target', NULL),
(30, 8, NULL, 'target_text', 'Test'),
(31, 8, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(32, 8, 1, 'subject', NULL),
(33, 9, 7, 'target', NULL),
(34, 9, NULL, 'target_text', 'dadaz ~ #30'),
(35, 9, NULL, 'admin_code', 'sonata.page.admin.block'),
(36, 9, 1, 'subject', NULL),
(37, 10, 7, 'target', NULL),
(38, 10, NULL, 'target_text', 'dadaz ~ #30'),
(39, 10, NULL, 'admin_code', 'sonata.page.admin.block'),
(40, 10, 1, 'subject', NULL),
(41, 11, 7, 'target', NULL),
(42, 11, NULL, 'target_text', 'dadaz ~ #30'),
(43, 11, NULL, 'admin_code', 'sonata.page.admin.block'),
(44, 11, 1, 'subject', NULL),
(45, 12, 8, 'target', NULL),
(46, 12, NULL, 'target_text', 'Homepage'),
(47, 12, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(48, 12, 1, 'subject', NULL),
(49, 13, 8, 'target', NULL),
(50, 13, NULL, 'target_text', 'Homepage'),
(51, 13, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(52, 13, 1, 'subject', NULL),
(53, 14, 8, 'target', NULL),
(54, 14, NULL, 'target_text', 'Homepage'),
(55, 14, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(56, 14, 1, 'subject', NULL),
(57, 15, 2, 'target', NULL),
(58, 15, NULL, 'target_text', 'test ~ #21'),
(59, 15, NULL, 'admin_code', 'sonata.page.admin.block'),
(60, 15, 1, 'subject', NULL),
(61, 16, 2, 'target', NULL),
(62, 16, NULL, 'target_text', 'test ~ #21'),
(63, 16, NULL, 'admin_code', 'sonata.page.admin.block'),
(64, 16, 1, 'subject', NULL),
(65, 17, NULL, 'target_text', 'test ~ #21'),
(66, 17, NULL, 'admin_code', 'sonata.page.admin.block'),
(67, 17, 1, 'subject', NULL),
(68, 18, 4, 'target', NULL),
(69, 18, NULL, 'target_text', 'Test'),
(70, 18, NULL, 'admin_code', 'sonata.page.admin.page'),
(71, 18, 1, 'subject', NULL),
(72, 19, 9, 'target', NULL),
(73, 19, NULL, 'target_text', 'Test'),
(74, 19, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(75, 19, 1, 'subject', NULL),
(76, 20, 9, 'target', NULL),
(77, 20, NULL, 'target_text', 'Test'),
(78, 20, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(79, 20, 1, 'subject', NULL),
(80, 21, 10, 'target', NULL),
(81, 21, NULL, 'target_text', ' ~ #31'),
(82, 21, NULL, 'admin_code', 'sonata.page.admin.block'),
(83, 21, 1, 'subject', NULL),
(84, 22, 11, 'target', NULL),
(85, 22, NULL, 'target_text', 'Test'),
(86, 22, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(87, 22, 1, 'subject', NULL),
(88, 23, 11, 'target', NULL),
(89, 23, NULL, 'target_text', 'Test'),
(90, 23, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(91, 23, 1, 'subject', NULL),
(92, 24, 12, 'target', NULL),
(93, 24, NULL, 'target_text', 'Header ~ #24'),
(94, 24, NULL, 'admin_code', 'sonata.page.admin.block'),
(95, 24, 1, 'subject', NULL),
(96, 25, 9, 'target', NULL),
(97, 25, NULL, 'target_text', 'Test'),
(98, 25, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(99, 25, 1, 'subject', NULL),
(100, 26, 13, 'target', NULL),
(101, 26, NULL, 'target_text', 'Test'),
(102, 26, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(103, 26, 1, 'subject', NULL),
(104, 27, 13, 'target', NULL),
(105, 27, NULL, 'target_text', 'Test'),
(106, 27, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(107, 27, 1, 'subject', NULL),
(108, 28, 14, 'target', NULL),
(109, 28, NULL, 'target_text', ' ~ #32'),
(110, 28, NULL, 'admin_code', 'sonata.page.admin.block'),
(111, 28, 1, 'subject', NULL),
(112, 29, 14, 'target', NULL),
(113, 29, NULL, 'target_text', ' ~ #32'),
(114, 29, NULL, 'admin_code', 'sonata.page.admin.block'),
(115, 29, 1, 'subject', NULL),
(116, 30, 15, 'target', NULL),
(117, 30, NULL, 'target_text', ' ~ #33'),
(118, 30, NULL, 'admin_code', 'sonata.page.admin.block'),
(119, 30, 1, 'subject', NULL),
(120, 31, 16, 'target', NULL),
(121, 31, NULL, 'target_text', 'Test'),
(122, 31, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(123, 31, 1, 'subject', NULL),
(124, 32, 16, 'target', NULL),
(125, 32, NULL, 'target_text', 'Test'),
(126, 32, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(127, 32, 1, 'subject', NULL),
(128, 33, 12, 'target', NULL),
(129, 33, NULL, 'target_text', 'Header ~ #24'),
(130, 33, NULL, 'admin_code', 'sonata.page.admin.block'),
(131, 33, 1, 'subject', NULL),
(132, 34, 17, 'target', NULL),
(133, 34, NULL, 'target_text', 'Test'),
(134, 34, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(135, 34, 1, 'subject', NULL),
(136, 35, 17, 'target', NULL),
(137, 35, NULL, 'target_text', 'Test'),
(138, 35, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(139, 35, 1, 'subject', NULL),
(140, 36, 10, 'target', NULL),
(141, 36, NULL, 'target_text', ' ~ #31'),
(142, 36, NULL, 'admin_code', 'sonata.page.admin.block'),
(143, 36, 1, 'subject', NULL),
(144, 37, 18, 'target', NULL),
(145, 37, NULL, 'target_text', 'Jamiroquai - Virtual Insanity (Official Video)'),
(146, 37, NULL, 'admin_code', 'sonata.media.admin.media'),
(147, 37, 1, 'subject', NULL),
(148, 38, 19, 'target', NULL),
(149, 38, NULL, 'target_text', ' ~ #34'),
(150, 38, NULL, 'admin_code', 'sonata.page.admin.block'),
(151, 38, 1, 'subject', NULL),
(152, 39, 20, 'target', NULL),
(153, 39, NULL, 'target_text', 'Test'),
(154, 39, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(155, 39, 1, 'subject', NULL),
(156, 40, 20, 'target', NULL),
(157, 40, NULL, 'target_text', 'Test'),
(158, 40, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(159, 40, 1, 'subject', NULL),
(160, 41, 20, 'target', NULL),
(161, 41, NULL, 'target_text', 'Test'),
(162, 41, NULL, 'admin_code', 'sonata.page.admin.snapshot'),
(163, 41, 1, 'subject', NULL),
(164, 42, 19, 'target', NULL),
(165, 42, NULL, 'target_text', ' ~ #34'),
(166, 42, NULL, 'admin_code', 'sonata.page.admin.block'),
(167, 42, 1, 'subject', NULL),
(168, 43, 19, 'target', NULL),
(169, 43, NULL, 'target_text', ' ~ #34'),
(170, 43, NULL, 'admin_code', 'sonata.page.admin.block'),
(171, 43, 1, 'subject', NULL),
(172, 44, 21, 'target', NULL),
(173, 44, NULL, 'target_text', ' ~ #35'),
(174, 44, NULL, 'admin_code', 'sonata.page.admin.block'),
(175, 44, 1, 'subject', NULL),
(176, 45, 22, 'target', NULL),
(177, 45, NULL, 'target_text', ' ~ #36'),
(178, 45, NULL, 'admin_code', 'sonata.page.admin.block'),
(179, 45, 1, 'subject', NULL),
(180, 46, 22, 'target', NULL),
(181, 46, NULL, 'target_text', ' ~ #36'),
(182, 46, NULL, 'admin_code', 'sonata.page.admin.block'),
(183, 46, 1, 'subject', NULL),
(184, 47, 22, 'target', NULL),
(185, 47, NULL, 'target_text', ' ~ #36'),
(186, 47, NULL, 'admin_code', 'sonata.page.admin.block'),
(187, 47, 1, 'subject', NULL),
(188, 48, 22, 'target', NULL),
(189, 48, NULL, 'target_text', 'aaaaa ~ #36'),
(190, 48, NULL, 'admin_code', 'sonata.page.admin.block'),
(191, 48, 1, 'subject', NULL),
(192, 49, 22, 'target', NULL),
(193, 49, NULL, 'target_text', 'aaaaa ~ #36'),
(194, 49, NULL, 'admin_code', 'sonata.page.admin.block'),
(195, 49, 1, 'subject', NULL),
(196, 50, 22, 'target', NULL),
(197, 50, NULL, 'target_text', 'aaaaa ~ #36'),
(198, 50, NULL, 'admin_code', 'sonata.page.admin.block'),
(199, 50, 1, 'subject', NULL),
(200, 51, 23, 'target', NULL),
(201, 51, NULL, 'target_text', 'salut ~ #37'),
(202, 51, NULL, 'admin_code', 'sonata.page.admin.block'),
(203, 51, 1, 'subject', NULL),
(204, 52, 24, 'target', NULL),
(205, 52, NULL, 'target_text', ' ~ #38'),
(206, 52, NULL, 'admin_code', 'sonata.page.admin.block'),
(207, 52, 1, 'subject', NULL),
(208, 53, 24, 'target', NULL),
(209, 53, NULL, 'target_text', ' ~ #38'),
(210, 53, NULL, 'admin_code', 'sonata.page.admin.block'),
(211, 53, 1, 'subject', NULL),
(212, 54, 25, 'target', NULL),
(213, 54, NULL, 'target_text', ' ~ #39'),
(214, 54, NULL, 'admin_code', 'sonata.page.admin.block'),
(215, 54, 1, 'subject', NULL),
(216, 55, NULL, 'target_text', ' ~ #38'),
(217, 55, NULL, 'admin_code', 'sonata.page.admin.block'),
(218, 55, 1, 'subject', NULL),
(219, 56, 25, 'target', NULL),
(220, 56, NULL, 'target_text', ' ~ #39'),
(221, 56, NULL, 'admin_code', 'sonata.page.admin.block'),
(222, 56, 1, 'subject', NULL),
(223, 57, 25, 'target', NULL),
(224, 57, NULL, 'target_text', ' ~ #39'),
(225, 57, NULL, 'admin_code', 'sonata.page.admin.block'),
(226, 57, 1, 'subject', NULL),
(227, 58, 26, 'target', NULL),
(228, 58, NULL, 'target_text', ' ~ #40'),
(229, 58, NULL, 'admin_code', 'sonata.page.admin.block'),
(230, 58, 1, 'subject', NULL),
(231, 59, 26, 'target', NULL),
(232, 59, NULL, 'target_text', ' ~ #40'),
(233, 59, NULL, 'admin_code', 'sonata.page.admin.block'),
(234, 59, 1, 'subject', NULL),
(235, 60, 26, 'target', NULL),
(236, 60, NULL, 'target_text', ' ~ #40'),
(237, 60, NULL, 'admin_code', 'sonata.page.admin.block'),
(238, 60, 1, 'subject', NULL),
(239, 61, 27, 'target', NULL),
(240, 61, NULL, 'target_text', ' ~ #41'),
(241, 61, NULL, 'admin_code', 'sonata.page.admin.block'),
(242, 61, 1, 'subject', NULL),
(243, 62, 28, 'target', NULL),
(244, 62, NULL, 'target_text', ' ~ #42'),
(245, 62, NULL, 'admin_code', 'sonata.page.admin.block'),
(246, 62, 1, 'subject', NULL),
(247, 63, 27, 'target', NULL),
(248, 63, NULL, 'target_text', ' ~ #41'),
(249, 63, NULL, 'admin_code', 'sonata.page.admin.block'),
(250, 63, 1, 'subject', NULL),
(251, 64, NULL, 'target_text', ' ~ #41'),
(252, 64, NULL, 'admin_code', 'sonata.page.admin.block'),
(253, 64, 1, 'subject', NULL),
(254, 65, 28, 'target', NULL),
(255, 65, NULL, 'target_text', ' ~ #42'),
(256, 65, NULL, 'admin_code', 'sonata.page.admin.block'),
(257, 65, 1, 'subject', NULL),
(258, 66, 29, 'target', NULL),
(259, 66, NULL, 'target_text', 'fr ~ #43'),
(260, 66, NULL, 'admin_code', 'sonata.page.admin.block'),
(261, 66, 1, 'subject', NULL),
(262, 67, 30, 'target', NULL),
(263, 67, NULL, 'target_text', 'The Beatles'),
(264, 67, NULL, 'admin_code', 'sonata.admin.band'),
(265, 67, 1, 'subject', NULL),
(266, 68, 31, 'target', NULL),
(267, 68, NULL, 'target_text', ' ~ #44'),
(268, 68, NULL, 'admin_code', 'sonata.page.admin.block'),
(269, 68, 1, 'subject', NULL),
(270, 69, 32, 'target', NULL),
(271, 69, NULL, 'target_text', 'fos_user_security_login'),
(272, 69, NULL, 'admin_code', 'sonata.page.admin.page'),
(273, 69, 1, 'subject', NULL),
(274, 70, 32, 'target', NULL),
(275, 70, NULL, 'target_text', 'fos_user_security_login'),
(276, 70, NULL, 'admin_code', 'sonata.page.admin.page'),
(277, 70, 1, 'subject', NULL),
(278, 71, 33, 'target', NULL),
(279, 71, NULL, 'target_text', 'Moloko'),
(280, 71, NULL, 'admin_code', 'sonata.admin.band'),
(281, 71, 1, 'subject', NULL),
(282, 72, 3, 'target', NULL),
(283, 72, NULL, 'target_text', 'ye_site_dashboard'),
(284, 72, NULL, 'admin_code', 'sonata.page.admin.page'),
(285, 72, 1, 'subject', NULL),
(286, 73, 3, 'target', NULL),
(287, 73, NULL, 'target_text', 'ye_site_dashboard'),
(288, 73, NULL, 'admin_code', 'sonata.page.admin.page'),
(289, 73, 1, 'subject', NULL),
(290, 74, 3, 'target', NULL),
(291, 74, NULL, 'target_text', 'ye_site_dashboard'),
(292, 74, NULL, 'admin_code', 'sonata.page.admin.page'),
(293, 74, 1, 'subject', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `timeline__component`
--

DROP TABLE IF EXISTS `timeline__component`;
CREATE TABLE `timeline__component` (
  `id` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__component`
--

INSERT INTO `timeline__component` (`id`, `model`, `identifier`, `hash`) VALUES
(1, 'YE\\UserBundle\\Entity\\User', 's:1:"1";', 'YE\\UserBundle\\Entity\\User#s:1:"1";'),
(2, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"21";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"21";'),
(3, 'Application\\Sonata\\PageBundle\\Entity\\Page', 's:1:"4";', 'Application\\Sonata\\PageBundle\\Entity\\Page#s:1:"4";'),
(4, 'Application\\Sonata\\PageBundle\\Entity\\Page', 's:2:"35";', 'Application\\Sonata\\PageBundle\\Entity\\Page#s:2:"35";'),
(5, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"29";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"29";'),
(6, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"401";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"401";'),
(7, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"30";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"30";'),
(8, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"435";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"435";'),
(9, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"436";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"436";'),
(10, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"31";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"31";'),
(11, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"437";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"437";'),
(12, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"24";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"24";'),
(13, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"438";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"438";'),
(14, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"32";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"32";'),
(15, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"33";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"33";'),
(16, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"439";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"439";'),
(17, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"440";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"440";'),
(18, 'Application\\Sonata\\MediaBundle\\Entity\\Media', 's:1:"8";', 'Application\\Sonata\\MediaBundle\\Entity\\Media#s:1:"8";'),
(19, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"34";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"34";'),
(20, 'Application\\Sonata\\PageBundle\\Entity\\Snapshot', 's:3:"441";', 'Application\\Sonata\\PageBundle\\Entity\\Snapshot#s:3:"441";'),
(21, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"35";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"35";'),
(22, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"36";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"36";'),
(23, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"37";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"37";'),
(24, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"38";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"38";'),
(25, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"39";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"39";'),
(26, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"40";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"40";'),
(27, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"41";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"41";'),
(28, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"42";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"42";'),
(29, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"43";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"43";'),
(30, 'YE\\UserBundle\\Entity\\Band', 's:1:"1";', 'YE\\UserBundle\\Entity\\Band#s:1:"1";'),
(31, 'Application\\Sonata\\PageBundle\\Entity\\Block', 's:2:"44";', 'Application\\Sonata\\PageBundle\\Entity\\Block#s:2:"44";'),
(32, 'Application\\Sonata\\PageBundle\\Entity\\Page', 's:1:"5";', 'Application\\Sonata\\PageBundle\\Entity\\Page#s:1:"5";'),
(33, 'YE\\UserBundle\\Entity\\Band', 's:2:"98";', 'YE\\UserBundle\\Entity\\Band#s:2:"98";');

-- --------------------------------------------------------

--
-- Structure de la table `timeline__timeline`
--

DROP TABLE IF EXISTS `timeline__timeline`;
CREATE TABLE `timeline__timeline` (
  `id` int(11) NOT NULL,
  `action_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `timeline__timeline`
--

INSERT INTO `timeline__timeline` (`id`, `action_id`, `subject_id`, `context`, `type`, `created_at`) VALUES
(1, 1, 1, 'GLOBAL', 'timeline', '2015-08-15 20:31:01'),
(2, 2, 1, 'GLOBAL', 'timeline', '2015-08-16 15:52:11'),
(3, 3, 1, 'GLOBAL', 'timeline', '2015-08-16 20:22:38'),
(4, 4, 1, 'GLOBAL', 'timeline', '2015-08-16 20:49:03'),
(5, 5, 1, 'GLOBAL', 'timeline', '2015-08-16 20:49:45'),
(6, 6, 1, 'GLOBAL', 'timeline', '2015-08-17 18:45:29'),
(7, 7, 1, 'GLOBAL', 'timeline', '2015-08-17 18:46:23'),
(8, 8, 1, 'GLOBAL', 'timeline', '2015-08-17 18:49:30'),
(9, 9, 1, 'GLOBAL', 'timeline', '2015-08-17 20:28:21'),
(10, 10, 1, 'GLOBAL', 'timeline', '2015-08-17 20:28:41'),
(11, 11, 1, 'GLOBAL', 'timeline', '2015-08-17 20:28:49'),
(12, 12, 1, 'GLOBAL', 'timeline', '2015-08-17 20:31:51'),
(13, 13, 1, 'GLOBAL', 'timeline', '2015-08-17 20:31:59'),
(14, 14, 1, 'GLOBAL', 'timeline', '2015-08-17 20:32:00'),
(15, 15, 1, 'GLOBAL', 'timeline', '2015-08-18 22:27:13'),
(16, 16, 1, 'GLOBAL', 'timeline', '2015-08-18 22:27:19'),
(17, 17, 1, 'GLOBAL', 'timeline', '2015-08-18 22:27:23'),
(18, 18, 1, 'GLOBAL', 'timeline', '2015-08-19 00:12:18'),
(19, 19, 1, 'GLOBAL', 'timeline', '2015-08-19 00:12:46'),
(20, 20, 1, 'GLOBAL', 'timeline', '2015-08-19 00:13:08'),
(21, 21, 1, 'GLOBAL', 'timeline', '2015-08-19 00:16:59'),
(22, 22, 1, 'GLOBAL', 'timeline', '2015-08-19 00:17:11'),
(23, 23, 1, 'GLOBAL', 'timeline', '2015-08-19 00:17:19'),
(24, 24, 1, 'GLOBAL', 'timeline', '2015-08-19 00:19:14'),
(25, 25, 1, 'GLOBAL', 'timeline', '2015-08-19 00:20:18'),
(26, 26, 1, 'GLOBAL', 'timeline', '2015-08-19 00:20:42'),
(27, 27, 1, 'GLOBAL', 'timeline', '2015-08-19 00:20:45'),
(28, 28, 1, 'GLOBAL', 'timeline', '2015-08-19 00:22:03'),
(29, 29, 1, 'GLOBAL', 'timeline', '2015-08-19 00:22:15'),
(30, 30, 1, 'GLOBAL', 'timeline', '2015-08-19 00:24:40'),
(31, 31, 1, 'GLOBAL', 'timeline', '2015-08-19 00:24:51'),
(32, 32, 1, 'GLOBAL', 'timeline', '2015-08-19 00:24:55'),
(33, 33, 1, 'GLOBAL', 'timeline', '2015-08-19 00:26:22'),
(34, 34, 1, 'GLOBAL', 'timeline', '2015-08-19 00:26:34'),
(35, 35, 1, 'GLOBAL', 'timeline', '2015-08-19 00:26:38'),
(36, 36, 1, 'GLOBAL', 'timeline', '2015-08-19 00:27:45'),
(37, 37, 1, 'GLOBAL', 'timeline', '2015-08-19 00:30:04'),
(38, 38, 1, 'GLOBAL', 'timeline', '2015-08-19 00:31:01'),
(39, 39, 1, 'GLOBAL', 'timeline', '2015-08-19 00:31:17'),
(40, 40, 1, 'GLOBAL', 'timeline', '2015-08-19 00:31:21'),
(41, 41, 1, 'GLOBAL', 'timeline', '2015-08-19 00:31:23'),
(42, 42, 1, 'GLOBAL', 'timeline', '2015-08-19 00:32:25'),
(43, 43, 1, 'GLOBAL', 'timeline', '2015-08-19 00:35:36'),
(44, 44, 1, 'GLOBAL', 'timeline', '2015-08-19 00:36:42'),
(45, 45, 1, 'GLOBAL', 'timeline', '2015-08-19 00:37:12'),
(46, 46, 1, 'GLOBAL', 'timeline', '2015-08-19 00:37:30'),
(47, 47, 1, 'GLOBAL', 'timeline', '2015-08-19 00:37:51'),
(48, 48, 1, 'GLOBAL', 'timeline', '2015-08-19 00:38:35'),
(49, 49, 1, 'GLOBAL', 'timeline', '2015-08-19 00:39:55'),
(50, 50, 1, 'GLOBAL', 'timeline', '2015-08-19 00:40:31'),
(51, 51, 1, 'GLOBAL', 'timeline', '2015-08-19 00:42:10'),
(52, 52, 1, 'GLOBAL', 'timeline', '2015-08-19 00:43:11'),
(53, 53, 1, 'GLOBAL', 'timeline', '2015-08-19 00:44:01'),
(54, 54, 1, 'GLOBAL', 'timeline', '2015-08-19 00:44:34'),
(55, 55, 1, 'GLOBAL', 'timeline', '2015-08-19 00:45:01'),
(56, 56, 1, 'GLOBAL', 'timeline', '2015-08-19 00:45:12'),
(57, 57, 1, 'GLOBAL', 'timeline', '2015-08-19 00:45:29'),
(58, 58, 1, 'GLOBAL', 'timeline', '2015-08-19 00:46:08'),
(59, 59, 1, 'GLOBAL', 'timeline', '2015-08-19 00:46:15'),
(60, 60, 1, 'GLOBAL', 'timeline', '2015-08-19 00:46:31'),
(61, 61, 1, 'GLOBAL', 'timeline', '2015-08-19 01:05:12'),
(62, 62, 1, 'GLOBAL', 'timeline', '2015-08-19 01:18:14'),
(63, 63, 1, 'GLOBAL', 'timeline', '2015-08-19 01:18:33'),
(64, 64, 1, 'GLOBAL', 'timeline', '2015-08-19 01:18:53'),
(65, 65, 1, 'GLOBAL', 'timeline', '2015-08-19 01:19:24'),
(66, 66, 1, 'GLOBAL', 'timeline', '2015-08-19 01:20:22'),
(67, 67, 1, 'GLOBAL', 'timeline', '2015-08-19 01:23:31'),
(68, 68, 1, 'GLOBAL', 'timeline', '2015-08-19 11:01:53'),
(69, 69, 1, 'GLOBAL', 'timeline', '2015-08-19 11:46:10'),
(70, 70, 1, 'GLOBAL', 'timeline', '2015-08-19 11:46:36'),
(71, 71, 1, 'GLOBAL', 'timeline', '2015-08-19 22:13:39'),
(72, 72, 1, 'GLOBAL', 'timeline', '2015-08-19 22:26:24'),
(73, 73, 1, 'GLOBAL', 'timeline', '2015-08-19 22:26:52'),
(74, 74, 1, 'GLOBAL', 'timeline', '2015-08-23 00:59:28');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twoStepVerificationCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `facebook_id`, `facebook_access_token`, `google_id`, `google_access_token`, `first_name`, `middle_name`, `last_name`, `birthday`, `created_at`, `updated_at`, `gender`, `locale`, `timezone`, `phone`, `twoStepVerificationCode`, `media_id`) VALUES
(1, '10206191690005358', '10206191690005358', 'youssef.elgharbaoui@gmail.com', 'youssef.elgharbaoui@gmail.com', 1, 'da621qlqpg8c8o8sgc0ggs008gg0kcs', 'G3stXdg332x89JV95mlqfHni3o9AzIEDbhMqPouYVDb8GP0o74sMNpO6rK78iPetP54EDCVbKABV99Krzw3Aag==', '2015-08-20 15:22:17', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, '10206191690005358', 'CAAWwEGYpqRUBAFXW13N2trNCOq9ZB8AbdC4QpLZCKGrkxvOPOMLZA9GwllXg4SGnrZC3SF2P7DV8IF34wBVrkkeWD1oUhud82k0bdPVKEId4rVX3eZChsZAwpGDbM3yTjTd3pZCpnlHoSNP7axDr80DZCMI9T95XMPg6ZBZArudnUY5ImWIKCMXCJzJgCiMspaRU0ZD', NULL, NULL, 'Youssef', 'El', 'Gharbaoui', '1989-05-31', '2015-08-19 22:07:05', '2015-08-19 22:07:05', 'u', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_band`
--

DROP TABLE IF EXISTS `user_band`;
CREATE TABLE `user_band` (
  `user_id` int(11) NOT NULL,
  `band_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_band`
--

INSERT INTO `user_band` (`user_id`, `band_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 75),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99),
(1, 100);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Index pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Index pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Index pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Index pour la table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Index pour la table `bands`
--
ALTER TABLE `bands`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `classification__category`
--
ALTER TABLE `classification__category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_43629B36727ACA70` (`parent_id`),
  ADD KEY `IDX_43629B36E25D857E` (`context`),
  ADD KEY `IDX_43629B36EA9FDD75` (`media_id`);

--
-- Index pour la table `classification__collection`
--
ALTER TABLE `classification__collection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_collection` (`slug`,`context`),
  ADD KEY `IDX_A406B56AE25D857E` (`context`),
  ADD KEY `IDX_A406B56AEA9FDD75` (`media_id`);

--
-- Index pour la table `classification__context`
--
ALTER TABLE `classification__context`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `classification__tag`
--
ALTER TABLE `classification__tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_context` (`slug`,`context`),
  ADD KEY `IDX_CA57A1C7E25D857E` (`context`);

--
-- Index pour la table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`);

--
-- Index pour la table `fos_user_user`
--
ALTER TABLE `fos_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`);

--
-- Index pour la table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Index pour la table `lexik_translation_file`
--
ALTER TABLE `lexik_translation_file`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash_idx` (`hash`);

--
-- Index pour la table `lexik_trans_unit`
--
ALTER TABLE `lexik_trans_unit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key_domain_idx` (`key_name`,`domain`);

--
-- Index pour la table `lexik_trans_unit_translations`
--
ALTER TABLE `lexik_trans_unit_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trans_unit_locale_idx` (`trans_unit_id`,`locale`),
  ADD KEY `IDX_B0AA394493CB796C` (`file_id`),
  ADD KEY `IDX_B0AA3944C3C583C9` (`trans_unit_id`);

--
-- Index pour la table `media__gallery`
--
ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

--
-- Index pour la table `media__media`
--
ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5C6DD74E12469DE2` (`category_id`);

--
-- Index pour la table `news__comment`
--
ALTER TABLE `news__comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A90210404B89032C` (`post_id`);

--
-- Index pour la table `news__post`
--
ALTER TABLE `news__post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7D109BC83DA5256D` (`image_id`),
  ADD KEY `IDX_7D109BC8F675F31B` (`author_id`),
  ADD KEY `IDX_7D109BC8514956FD` (`collection_id`);

--
-- Index pour la table `news__post_tag`
--
ALTER TABLE `news__post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `IDX_682B20514B89032C` (`post_id`),
  ADD KEY `IDX_682B2051BAD26311` (`tag_id`);

--
-- Index pour la table `notification__message`
--
ALTER TABLE `notification__message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_at` (`created_at`);

--
-- Index pour la table `page__block`
--
ALTER TABLE `page__block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_66F58DA0727ACA70` (`parent_id`),
  ADD KEY `IDX_66F58DA0C4663E4` (`page_id`);

--
-- Index pour la table `page__page`
--
ALTER TABLE `page__page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2FAE39EDF6BD1646` (`site_id`),
  ADD KEY `IDX_2FAE39ED727ACA70` (`parent_id`),
  ADD KEY `IDX_2FAE39ED158E0B66` (`target_id`);

--
-- Index pour la table `page__site`
--
ALTER TABLE `page__site`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `page__snapshot`
--
ALTER TABLE `page__snapshot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3963EF9AF6BD1646` (`site_id`),
  ADD KEY `IDX_3963EF9AC4663E4` (`page_id`),
  ADD KEY `idx_snapshot_dates_enabled` (`publication_date_start`,`publication_date_end`,`enabled`);

--
-- Index pour la table `timeline__action`
--
ALTER TABLE `timeline__action`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `timeline__action_component`
--
ALTER TABLE `timeline__action_component`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6ACD1B169D32F035` (`action_id`),
  ADD KEY `IDX_6ACD1B16E2ABAFFF` (`component_id`);

--
-- Index pour la table `timeline__component`
--
ALTER TABLE `timeline__component`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1B2F01CDD1B862B8` (`hash`);

--
-- Index pour la table `timeline__timeline`
--
ALTER TABLE `timeline__timeline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FFBC6AD59D32F035` (`action_id`),
  ADD KEY `IDX_FFBC6AD523EDC87` (`subject_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_D5428AED92FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_D5428AEDA0D96FBF` (`email_canonical`),
  ADD KEY `IDX_D5428AEDEA9FDD75` (`media_id`);

--
-- Index pour la table `user_band`
--
ALTER TABLE `user_band`
  ADD PRIMARY KEY (`user_id`,`band_id`),
  ADD KEY `IDX_325EEE22A76ED395` (`user_id`),
  ADD KEY `IDX_325EEE2249ABEB17` (`band_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `bands`
--
ALTER TABLE `bands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT pour la table `classification__category`
--
ALTER TABLE `classification__category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `classification__collection`
--
ALTER TABLE `classification__collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `classification__tag`
--
ALTER TABLE `classification__tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `fos_user_user`
--
ALTER TABLE `fos_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `lexik_translation_file`
--
ALTER TABLE `lexik_translation_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `lexik_trans_unit`
--
ALTER TABLE `lexik_trans_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1001;
--
-- AUTO_INCREMENT pour la table `lexik_trans_unit_translations`
--
ALTER TABLE `lexik_trans_unit_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1403;
--
-- AUTO_INCREMENT pour la table `media__gallery`
--
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `media__media`
--
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `news__comment`
--
ALTER TABLE `news__comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `news__post`
--
ALTER TABLE `news__post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `notification__message`
--
ALTER TABLE `notification__message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `page__block`
--
ALTER TABLE `page__block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT pour la table `page__page`
--
ALTER TABLE `page__page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `page__site`
--
ALTER TABLE `page__site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `page__snapshot`
--
ALTER TABLE `page__snapshot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=470;
--
-- AUTO_INCREMENT pour la table `timeline__action`
--
ALTER TABLE `timeline__action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT pour la table `timeline__action_component`
--
ALTER TABLE `timeline__action_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=294;
--
-- AUTO_INCREMENT pour la table `timeline__component`
--
ALTER TABLE `timeline__component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `timeline__timeline`
--
ALTER TABLE `timeline__timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Contraintes pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `classification__category`
--
ALTER TABLE `classification__category`
  ADD CONSTRAINT `FK_43629B36727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `classification__category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_43629B36E25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`),
  ADD CONSTRAINT `FK_43629B36EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `classification__collection`
--
ALTER TABLE `classification__collection`
  ADD CONSTRAINT `FK_A406B56AE25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`),
  ADD CONSTRAINT `FK_A406B56AEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `classification__tag`
--
ALTER TABLE `classification__tag`
  ADD CONSTRAINT `FK_CA57A1C7E25D857E` FOREIGN KEY (`context`) REFERENCES `classification__context` (`id`);

--
-- Contraintes pour la table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `lexik_trans_unit_translations`
--
ALTER TABLE `lexik_trans_unit_translations`
  ADD CONSTRAINT `FK_B0AA394493CB796C` FOREIGN KEY (`file_id`) REFERENCES `lexik_translation_file` (`id`),
  ADD CONSTRAINT `FK_B0AA3944C3C583C9` FOREIGN KEY (`trans_unit_id`) REFERENCES `lexik_trans_unit` (`id`);

--
-- Contraintes pour la table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Contraintes pour la table `media__media`
--
ALTER TABLE `media__media`
  ADD CONSTRAINT `FK_5C6DD74E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `classification__category` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `news__comment`
--
ALTER TABLE `news__comment`
  ADD CONSTRAINT `FK_A90210404B89032C` FOREIGN KEY (`post_id`) REFERENCES `news__post` (`id`);

--
-- Contraintes pour la table `news__post`
--
ALTER TABLE `news__post`
  ADD CONSTRAINT `FK_7D109BC83DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media__media` (`id`),
  ADD CONSTRAINT `FK_7D109BC8514956FD` FOREIGN KEY (`collection_id`) REFERENCES `classification__collection` (`id`),
  ADD CONSTRAINT `FK_7D109BC8F675F31B` FOREIGN KEY (`author_id`) REFERENCES `fos_user_user` (`id`);

--
-- Contraintes pour la table `news__post_tag`
--
ALTER TABLE `news__post_tag`
  ADD CONSTRAINT `FK_682B20514B89032C` FOREIGN KEY (`post_id`) REFERENCES `news__post` (`id`),
  ADD CONSTRAINT `FK_682B2051BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `classification__tag` (`id`);

--
-- Contraintes pour la table `page__block`
--
ALTER TABLE `page__block`
  ADD CONSTRAINT `FK_66F58DA0727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `page__block` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_66F58DA0C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page__page` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `page__page`
--
ALTER TABLE `page__page`
  ADD CONSTRAINT `FK_2FAE39ED158E0B66` FOREIGN KEY (`target_id`) REFERENCES `page__page` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2FAE39ED727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `page__page` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2FAE39EDF6BD1646` FOREIGN KEY (`site_id`) REFERENCES `page__site` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `page__snapshot`
--
ALTER TABLE `page__snapshot`
  ADD CONSTRAINT `FK_3963EF9AC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page__page` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_3963EF9AF6BD1646` FOREIGN KEY (`site_id`) REFERENCES `page__site` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `timeline__action_component`
--
ALTER TABLE `timeline__action_component`
  ADD CONSTRAINT `FK_6ACD1B169D32F035` FOREIGN KEY (`action_id`) REFERENCES `timeline__action` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6ACD1B16E2ABAFFF` FOREIGN KEY (`component_id`) REFERENCES `timeline__component` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `timeline__timeline`
--
ALTER TABLE `timeline__timeline`
  ADD CONSTRAINT `FK_FFBC6AD523EDC87` FOREIGN KEY (`subject_id`) REFERENCES `timeline__component` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FFBC6AD59D32F035` FOREIGN KEY (`action_id`) REFERENCES `timeline__action` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_D5428AEDEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Contraintes pour la table `user_band`
--
ALTER TABLE `user_band`
  ADD CONSTRAINT `FK_325EEE2249ABEB17` FOREIGN KEY (`band_id`) REFERENCES `Bands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_325EEE22A76ED395` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE;
