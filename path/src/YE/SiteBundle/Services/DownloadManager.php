<?php 
namespace YE\SiteBundle\Services;
use Facebook\Facebook;

use Symfony\Component\HttpFoundation\Request;

use YE\SiteBundle\Entity\Downloads;
use YE\SiteBundle\Entity\FacebookPosts;

use Cocur\BackgroundProcess\BackgroundProcess;
use Symfony\Component\Process\Process;

use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Cookie;

class DownloadManager
{
    private $oContainer;
    private $oDoctrine;
    private $oManager;
    private $oSession;

    private $sWebPath;
    private $sBaseStorePath;

    private $sFileSizePrefix;
    private $sFileMetaDataPrefix;
    private $sExtensionInfo;

    private $iBestFormatID;
    private $aAudioFormats;
    private $aVideoFormats;

    private $sDefaultYoutubeLink;
    private $sDefaultOption;

    private $oResponse;

    public function __construct($oContainer)
    {
        $this->oContainer = $oContainer;
        $this->oDoctrine = $this->oContainer->get('doctrine');
        $this->oManager = $this->oDoctrine->getManager();
        $this->oSession = $this->oContainer->get('session');
        $this->sWebPath = $this->oContainer->get('kernel')->getRootDir() . "/../web/";
        $this->sBaseStorePath = $this->sWebPath . $this->oContainer->getParameter('youtube_downloads_folder') . "/";

        $this->sFileSizePrefix = "iFilesize_";
        $this->sFileMetaDataPrefix = "aFormats_";
        $this->sExtensionInfo = ".json";

        $this->sDefaultYoutubeLink = "https://www.youtube.com/watch?v=";
        $this->sDefaultOptions = "--no-check-certificate --no-post-overwrites --no-call-home --no-mtime --max-downloads 1";
    }

    public function getBestFormatID()
    {
        return $this->iBestFormatID;
    }

    public function setBestFormatID($iBestFormatID)
    {
        $this->iBestFormatID = $iBestFormatID;
    }

    public function setAudioFormats($aAudioFormats)
    {
        $this->aAudioFormats = $aAudioFormats;
    }

    public function getAudioFormats()
    {
        return $this->aAudioFormats;
    }

    public function setVideoFormats($aVideoFormats)
    {
        $this->aVideoFormats = $aVideoFormats;
    }

    public function getVideoFormats()
    {
        return $this->aVideoFormats;
    }

    public function getResponse()
    {
        return $this->oResponse;
    }

    public function setResponse(Response $oResponse)
    {
        $this->oResponse = $oResponse;
    }

    public function checkForReferer(Request $oRequest)
    {
        $bFoundSlugInDownload = false;
        if ( $oRequest->get('slug') != null && strlen($oRequest->get('slug')) > 0 )
        {
            $sReferer = $this->oManager->getRepository('YESiteBundle:Downloads')->findOneBySlug($oRequest->get('slug'));
            $sReferer = ( $sReferer instanceof Downloads ) ? $sReferer->getVideoID() : false;
            $bFoundSlugInDownload = ( $sReferer != null && strlen($sReferer) > 0 ) ? true : false;
        }

        if ( !$bFoundSlugInDownload )
        $sReferer = ( $oRequest->get('v') != null && strlen($oRequest->get('v')) > 0 ) ? $oRequest->get('v') : false;

        return $sReferer;
    }

    public function fetchVideoToDisplayById(Request $oRequest, $_sVideoID = null)
    {
        $sUrl = $oRequest->request->get('sUrl');

        // Capture de l'ID de la video
        $sVideoID = ( $_sVideoID == null ) ? $this->parseYoutubeId($sUrl) : $_sVideoID;

        if ( !$sVideoID || $sVideoID == null ) return array("empty" => "yes");

        $oVideo = $this->oContainer->get('happyr.google.api.youtube')->videos->listVideos('snippet', array('id' => $sVideoID));


        // Si l'ID de la video recherchée est invalide
        if ( isset($oVideo->getItems()[0]) )
        {
            $oVideoData = $oVideo->getItems()[0];

            $this->getVideoMetaData($sVideoID);

            return $this->extractAndSaveMetaData($oVideoData, $sVideoID);
        }

        return array("empty" => "yes");

    }

    public function extractAndSaveMetaData($oVideoData, $sVideoID)
    {
        $_oDatePublishedAt = new \DateTime($oVideoData->getSnippet()->getPublishedAt());
        $oDatePublishedAt = $_oDatePublishedAt->format('l j F Y \a\t H:i');

        $sTitle = $this->sanitizeTitle($oVideoData->getSnippet()->getTitle());

        $sDescription = $oVideoData->getSnippet()->getDescription();
        $sChannelTitle = $oVideoData->getSnippet()->getChannelTitle();

        $this->saveMetaData($sVideoID, $sTitle, $sDescription, $_oDatePublishedAt);

        return array(
            'empty' => 'no',
            'idVideo' => $sVideoID,
            'title' => $sTitle,
            'description' => $sDescription,
            'channelTitle' => $sChannelTitle,
            'channelId' => $oVideoData->getSnippet()->getChannelId(),
            'publishedAt' => $oDatePublishedAt
        );

    }

    public function saveMetaData($sVideoID, $sTitle, $sDescription, $_oDatePublishedAt)
    {
        // ladybug_dump($sTitle); die();
        $this->storeDataIntoSession('Youtube-Video-ID', $sVideoID);
        $this->storeDataIntoSession('Youtube-Video-Title', $sTitle);
        $this->storeDataIntoSession('Youtube-Video-Description', $sDescription);
        $this->storeDataIntoSession('Youtube-Video-DatePublication', $_oDatePublishedAt);
    }

    public function sanitizeTitle($sTitle)
    {
        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;:[]().
        $sTitle = preg_replace("/([^[:ascii:]\p{Arabic}\w\s\d\-_~,;:\[\]\(\).])/u", '', $sTitle);
        // Remove any runs of periods (thanks falstro!)
        $sTitle = preg_replace("([\.]{2,})", '', $sTitle);
        // Replace / by -
        $sTitle = preg_replace("(\/)", '-', $sTitle);
        // Replace ' and " by null
        $sTitle = str_replace(array('"', "'"), "", $sTitle);

        return $sTitle;
    }

    public function parseYoutubeId($sUrl)
    {
        $sPattern = '#^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=))([\w-]{11})(?![\w-])#';
        preg_match($sPattern, $sUrl, $aMatches);
        return (isset($aMatches[1])) ? $aMatches[1] : false;
    }

    public function checkMetaDataExists($sVideoID, $bDeleteFile = false, $bExecuteInBackground = true)
    {
        $sFile = $this->sBaseStorePath . $this->sFileMetaDataPrefix . $sVideoID . $this->sExtensionInfo;
        if ( !file_exists( $sFile ) ) 
            $this->getVideoMetaData($sVideoID, $bDeleteFile, $bExecuteInBackground);
        else if ( file_exists( $sFile ) && count((array)$this->getFileContent($this->sFileMetaDataPrefix, $sVideoID)) <= 1 )
            $this->getVideoMetaData($sVideoID, true, false);
    }

    public function getVideoMetaData($sVideoID, $bDeleteFile = false, $bExecuteInBackground = true)
    {
        $sFile = $this->sBaseStorePath . $this->sFileMetaDataPrefix . $sVideoID . $this->sExtensionInfo;
        // If the file exists and bDeleteFile is true, then delete the file
        if ( file_exists($sFile) && $bDeleteFile )
        {
            unlink($sFile);
        }
        // If the file doesnt exists
        if ( !file_exists($sFile) )
        {
            $oProcess = new Process('youtube-dl -f best --simulate --print-json https://www.youtube.com/watch?v=' . $sVideoID . ' --no-check-certificate --no-post-overwrites --no-call-home --no-mtime --max-downloads 1 > ' . $sFile);
            // the same works when using start instead of run
            $oProcess->start();
        }

        // Fetch the filesize
        $this->getFileSize($sVideoID);
    }

    public function getFileSize($sVideoID, $bDeleteFile = false)
    {
        $oProcess = new Process('ytdl -s ' . $this->sDefaultYoutubeLink . $sVideoID);
        // the same works when using start instead of run
        $oProcess->start(function ($sType, $sData) 
        {
            if (Process::ERR == $sType) {} // Error 
            else $this->storeDataIntoSession('Youtube-Video-FileSize', $sData); 
        });
    }

    // public function checkFileSizeExists($sVideoID, $bDeleteFile = false)
    // {
    //     $sFile = $this->sBaseStorePath . $this->sFileSizePrefix . $sVideoID . $this->sExtensionInfo;
    //     if ( !file_exists( $sFile ) ) 
    //         $this->getFileSize($sVideoID, $bDeleteFile);
    //     else if ( file_exists( $sFile ) && !is_numeric( $this->getFileContent($this->sFileSizePrefix, $sVideoID, false) ) )
    //         $this->getFileSize($sVideoID, true);
    // }

    public function getFileContent($sTypeFile, $sVideoID, $jsonResponse = true)
    {
        $sContent = file_get_contents($this->sBaseStorePath . $sTypeFile . $sVideoID . $this->sExtensionInfo);
        return ($jsonResponse) ? json_decode($sContent) : $sContent;       
    }

    public function getDifferentDownloadFormats($aData)
    {
        $aFormats = array();

        foreach ($aData->formats as $iKey => $sValue) 
        {
            // Catch all available video formats (with filesize)
            if ( isset($sValue->height) && $sValue->height != "" && isset($sValue->width) && $sValue->width != "" && isset($sValue->filesize) && $sValue->filesize != "" )
            {
                $aFormats['Video'][$sValue->format_id]['width'] = ( isset($sValue->width) ) ? $sValue->width : null;
                $aFormats['Video'][$sValue->format_id]['height'] = ( isset($sValue->height) ) ? $sValue->height : null;
                $aFormats['Video'][$sValue->format_id]['ext'] = ( isset($sValue->ext) ) ? $sValue->ext : null;
                $aFormats['Video'][$sValue->format_id]['filesize'] = ( isset($sValue->filesize) ) ? $sValue->filesize : null;
                $aFormats['Video'][$sValue->format_id]['format_note'] = ( isset($sValue->format_note) ) ? $sValue->format_note : null;

            }
            // Catch all available videos formats (without filesize)
            else if ( isset($sValue->height) && $sValue->height != "" && isset($sValue->width) && $sValue->width != "" && isset($sValue->filesize) && $sValue->filesize == "" )
            {
                $aFormats['AV'][$sValue->format_id]['width'] = ( isset($sValue->width) ) ? $sValue->width : null;
                $aFormats['AV'][$sValue->format_id]['height'] = ( isset($sValue->height) ) ? $sValue->height : null;
                $aFormats['AV'][$sValue->format_id]['ext'] = ( isset($sValue->ext) ) ? $sValue->ext : null;
                $aFormats['AV'][$sValue->format_id]['filesize'] = ( isset($sValue->filesize) ) ? $sValue->filesize : null;   
                $aFormats['AV'][$sValue->format_id]['format_note'] = ( isset($sValue->format_note) ) ? $sValue->format_note : null;   
            }
            // Catch all available audio formats
            else if ( isset($sValue->filesize) && $sValue->filesize != "" )
            {
                $aFormats['Audio'][$sValue->format_id]['asr'] = ( isset($sValue->asr) ) ? $sValue->asr : null;
                $aFormats['Audio'][$sValue->format_id]['tbr'] = ( isset($sValue->tbr) ) ? $sValue->tbr : null;
                $aFormats['Audio'][$sValue->format_id]['ext'] = ( isset($sValue->ext) ) ? $sValue->ext : null;
                $aFormats['Audio'][$sValue->format_id]['filesize'] = ( isset($sValue->filesize) )? $sValue->filesize : null;
                $aFormats['Audio'][$sValue->format_id]['format_note'] = ( isset($sValue->format_note) )? $sValue->format_note : null;
            }
        }

        return $aFormats;
    }

    public function prepareResponse($sTitle, $sExtension)
    {
        $oResponse = new Response();
        $sFileName = $sTitle . '.' . $sExtension;

        //$oResponse->headers->set('Cache-Control', 'no-cache');
        $oResponse->headers->set('Content-type', 'application/octet-stream');
        // $sDisposition = $oResponse->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, \Behat\Transliterator\Transliterator::utf8ToAscii($sFileName));
        $oResponse->headers->set('Content-Disposition', 'attachment; filename="' . $sFileName . '"');
        $oResponse->headers->setCookie(new Cookie('download', 'finished', time() + (60*60*24), '/', null, false, false));

        $this->setResponse($oResponse);
    }

    public function persistDownload($sExtension)
    {
        $oDownload = new Downloads();

        $oDownload->setTitle($this->oSession->get('Youtube-Video-Title'));
        $oDownload->setExtension($sExtension);
        $oDownload->setVideoID($this->oSession->get('Youtube-Video-ID'));
        $oDownload->setDescription($this->oSession->get('Youtube-Video-Description'));
        $oDownload->setDatePublication($this->oSession->get('Youtube-Video-DatePublication'));


        $oDownload->setFormatID($this->getBestFormatID());

        $this->oManager->persist($oDownload);
        $this->oManager->flush();

        return $oDownload;
    }

    public function shareOnFacebook($oDownload)
    {
        $oRepository = $this->oDoctrine->getRepository('YESiteBundle:FacebookPosts');
        $iMax = $this->oContainer->getParameter('facebook_max_post_same_post');
        // Check if this Video has been posted on Facebook lately or not and do this only if we are in PROD env
        if ( $oRepository->checkIfNotExcessif($oDownload, $iMax) && $this->oContainer->get('kernel')->getEnvironment() == 'prod' )
        {
            // Post to Facebook If the same video hasnt been posted today OR lastly
            $oFacebookPost = new FacebookPosts();
            $oFacebookPost->setDownload($oDownload);
            $oNode = $this->oContainer->get('api.facebook')->post($oDownload->getSlug(), $oDownload->getTitle(), $oDownload->getVideoID(), $oDownload->getDescription());

            if ( is_object($oNode) && isset($oNode->getGraphNode()['id']) ) $oFacebookPost->setNodeId($oNode->getGraphNode()['id']);

            $this->oManager->persist($oDownload);
            $this->oManager->persist($oFacebookPost);
            $this->oManager->flush();
        }
    }

    public function getBestFormat($aFormats, $sExtension)
    {
        $iBestFormatID = 0;

        if ( $sExtension == "mp3" )
        {
            $aAudioFormats = array_reverse($aFormats['Audio'], true);
            reset($aAudioFormats);
            $iBestFormatID = key($aAudioFormats);

            $this->setAudioFormats($aAudioFormats);
        }
        else
        {
            if ( isset($aFormats['AV']) )
            {
                $aVideoFormats = array_reverse($aFormats['AV'], true);
                reset($aVideoFormats);
                $iBestFormatID = key($aVideoFormats);

                $this->setVideoFormats($aVideoFormats);
            }
            else
            {
                $iBestFormatID = 'best';    
            }
        }

        $this->setBestFormatID($iBestFormatID);
        
        

        return $iBestFormatID;
    }

    public function buildCommand($sExtension, $aData)
    {
        if ( $sExtension == 'mp3' )
        {
            $iDuration = ( $aData->duration && (int)$aData->duration > 1 ) ? $aData->duration : 0;
            $iDefaultBitrates = 192;

            $iBitrates = ( isset($this->getAudioFormats()[$this->getBestFormatID()]['tbr']) && 
                            (int)$this->getAudioFormats()[$this->getBestFormatID()]['tbr'] > $iDefaultBitrates 
                         ) ? $this->getAudioFormats()[$this->getBestFormatID()]['tbr'] : $iDefaultBitrates;

            $iSize = '';

            $sUserAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
            // Si c'est un android ou qu'on est en HTTPS, pas besoin d'envoyer la taille.
            // if(stripos($sUserAgent,'android') == false || 
            //     ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && 
            //       $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ))
            // {
                // Calcul de la taille à telecharger
                $_iSize =  (((int)$iBitrates * ((int)$iDuration - 1)) / 8) * 1000;
                $iSize = '-fs ' . $_iSize;

                //$this->oResponse->headers->set('Content-type', 'audio/mpeg');
                $this->oResponse->headers->set('Content-Length', (int)$_iSize);
            //}

            return 'youtube-dl -q -i -f ' . $this->getBestFormatID() . ' -o - '  . $this->sDefaultYoutubeLink . $this->oSession->get('Youtube-Video-ID') . ' ' . $this->sDefaultOptions . ' | ffmpeg -i - -f mp3 -y -b:a ' . $iBitrates . 'k -t ' . $iDuration . ' ' . $iSize . ' pipe:1';
        }
        else
        {
            //$iAudioBitrates = 192;


            //$this->checkFileSizeExists($this->oSession->get('Youtube-Video-ID'));

            //$iSize = $this->getFileContent($this->sFileSizePrefix, $this->oSession->get('Youtube-Video-ID'), false);
            $iSize = $this->oSession->get('Youtube-Video-FileSize');

            if ( (int)$iSize > 10 ) $this->oResponse->headers->set('Content-Length', (int)$iSize);

            return 'youtube-dl -q -f best -o - ' . $this->sDefaultYoutubeLink . $this->oSession->get('Youtube-Video-ID') . ' ' . $this->sDefaultOptions;
        }
    }

    // public function extractExtension($oForm)
    // {
    //     $sReturn = ( strpos($oForm->get('videoID')->getData(), '??mp3') ) ? '.mp3' : '.mp4';
    //     $this->storeDataIntoSession('Youtube-Video-Extension', $sExtensionToDownload);
    //     return $sReturn;
    // }

    // public function populateEntity($oEntity)
    // {
    //     $oEntity->setVideoID($this->oSession->get('Youtube-Video-ID'));
    //     $oEntity->setTitle($this->oSession->get('Youtube-Video-Title'));
    //     $oEntity->setExtension(str_replace('.', '', $this->oSession->get('Youtube-Video-Extension')));
    //     return $oEntity;
    // }

    public function storeDataIntoSession($iKey, $sValue)
    {
        $this->oSession->set($iKey, $sValue);
    }

    public function getDataFromSession()
    {
        
    }

    // public function buildPathToFile($sFileName)
    // {
    //     return str_replace("\"", "'", $this->baseStorePath . $sTitle . $sExtensionToDownload);
    // }


     
}


?>