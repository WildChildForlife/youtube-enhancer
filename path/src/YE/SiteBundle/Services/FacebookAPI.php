<?php 
namespace YE\SiteBundle\Services;
use Facebook\Facebook;

class FacebookAPI
{
	private $oContainer;
	private $oFacebook;

    public function __construct($oContainer)
    {
    	$this->oContainer = $oContainer;
    	$this->oFacebook = new Facebook([
			'app_id' => $this->oContainer->getParameter('facebook_app_id'),
			'app_secret' => $this->oContainer->getParameter('facebook_app_secret'),
			'default_access_token' => $this->oContainer->getParameter('facebook_app_token'),
			'default_graph_version' => $this->oContainer->getParameter('facebook_app_version')
         ]);
    }

    public function post($sLink, $sTitle, $iVideoID = false, $sDescription = false)
    {
    	$aParams = array();
        //$aParams['link']    = 'http://www.youtube-enhancer.com/download/' . $sLink;
        $aParams['link']    = 'https://www.youtube.com/watch?v=' . $iVideoID;
    	$aParams['type'] 	= 'video';
    	$aParams['message'] = 'Link to download it (MP3 / MP4) HD :
    	' . 'http://www.youtube-enhancer.com/download/' . $sLink;
    	if ($iVideoID) 	$aParams['picture'] = 'http://img.youtube.com/vi/' . $iVideoID . '/hqdefault.jpg';
    	//if ($sName) 	$aParams['name'] 	= $sName . ' | Download - Youtube Enhancer';
    	$aParams['name'] 	= $sTitle . ' | Youtube Enhancer';
    	if ($sDescription) $aParams['description'] 	= ' DOWNLOAD | ' . $sDescription;
    	    
    	try
    	{
          	// Returns a `Facebook\FacebookResponse` object
          	$oResponse = $this->oFacebook->post('/me/feed', $aParams);
        } 
        catch(Facebook\Exceptions\FacebookResponseException $oException) 
        {
          	echo 'Graph returned an error: ' . $oException->getMessage();
          	exit;
        } 
        catch(Facebook\Exceptions\FacebookSDKException $oException) 
        {
          	echo 'Facebook SDK returned an error: ' . $oException->getMessage();
          	exit;
        }

        return $oResponse;
    }


     
}


?>