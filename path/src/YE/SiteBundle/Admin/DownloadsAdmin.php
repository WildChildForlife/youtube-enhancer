<?php 
namespace YE\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DownloadsAdmin extends Admin
{

    //protected $baseRoutePattern = 'downloads';
    public $supportsPreviewMode = true;
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'dateDownload',
    );

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper

            /*
             * The default option is to just display the value as text (for boolean this will be 1 or 0)
             */
            ->add('ip')
            ->add('title')
            ->add('extension')
            ->add('videoID')
            ->add('formatID')
            ->add('dateDownload')
            ->add('datePublication')
            ->add('recentlyDownloaded')
            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('ip', null, array('label' => 'IP'))
            ->add('title', null, array('label' => 'Titre'))
            ->add('extension', null, array('label' => 'Extension'))
            ->add('videoID', null, array('label' => 'Video'))
            ->add('formatID', null, array('label' => 'Format'))
            ->add('dateDownload', null, array('label' => 'Date Download'))
            ->add('datePublication', null, array('label' => 'Date Publication'))
            ->add('recentlyDownloaded', null, array('label' => 'Recently Downloaded List ?'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('ip')
            ->add('title')
            ->add('extension')
            ->add('videoID')
            ->add('formatID')
            ->add('dateDownload')
            ->add('datePublication')
            ->add('recentlyDownloaded')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('ip', null, array('editable' => true))
            ->addIdentifier('title', null, array('editable' => true))
            ->addIdentifier('extension', null, array('editable' => true))
            ->addIdentifier('videoID', null, array('editable' => true))
            ->addIdentifier('formatID', null, array('editable' => true))
            ->addIdentifier('dateDownload', null, array('editable' => true))
            ->addIdentifier('datePublication', null, array('editable' => true))
            ->add('recentlyDownloaded', null, array('editable' => true))
        ;
    }
}
?>