<?php 
namespace YE\SiteBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class BandAdmin extends Admin
{

    protected $baseRoutePattern = 'band';
    public $supportsPreviewMode = true;

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper

            /*
             * The default option is to just display the value as text (for boolean this will be 1 or 0)
             */
            ->add('name')
            ->add('genre')
            ;

    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Nom'))
            ->add('genre', 'text', array('label' => 'Genre'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('genre')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('media', 'sonata_type_model_list', array('label' => 'Image', 'template' => 'ApplicationSonataAdminBundle:User:picture.html.twig'), array('link_parameters' => array('context' => 'band')))
            ->addIdentifier('name', null, array('editable' => true))
            ->addIdentifier('genre', null, array('editable' => true))
        ;
    }
}
?>