<?php

namespace YE\SiteBundle\Sitemap;

use Symfony\Component\Routing\RouterInterface;
use Werkspot\Bundle\SitemapBundle\Provider\AbstractSitemapProvider;
use Werkspot\Bundle\SitemapBundle\Sitemap\SitemapSectionPage;
use Werkspot\Bundle\SitemapBundle\Sitemap\Url;
use YE\SiteBundle\Entity\Repository\PageRepository;
use YE\SiteBundle\Entity\Downloads;

class DownloadsSitemapProvider extends AbstractSitemapProvider
{
    /**
     * @var DownloadsRepository
     */
    private $oDownloadsRepository;

    /**
     * @param RouterInterface $oRouter
     * @param DownloadsRepository $oDownloadsRepository
     */
    public function __construct(RouterInterface $oRouter, $oDoctrine)
    {
        parent::__construct($oRouter);
        $this->oDownloadsRepository = $oDoctrine->getRepository('YESiteBundle:Downloads');
    }

    /**
     * @param int $pageNumber
     * @return SitemapSectionPage
     */
    public function getPage($pageNumber)
    {
        $aDownloads = $this->oDownloadsRepository->getAllDistinctDownloads();

        $oPage = new SitemapSectionPage();

        $aStaticPages = array('homepage', 'aboutus', 'faq', 'termsofuse', 'privacy');
        foreach ($aStaticPages as $iKey => $sValue) 
        {
            $iPriority = ( $sValue == 'homepage' ) ? 0.1 : 0.3;
            $sChangeFrequency = ( $sValue == 'homepage' ) ? Url::CHANGEFREQ_ALWAYS : Url::CHANGEFREQ_NEVER;
            $oUrlRoute = $this->generateUrl('ye_site_' . $sValue, ['_locale' => 'en']);
            $oPage->addUrl(new Url($oUrlRoute, $sChangeFrequency, $iPriority));                
        }

        foreach ($aDownloads as $oDownload) 
        {
            $oUrlRoute = $this->generateUrl('ye_site_homepage_download_slug', [
                'slug' => $oDownload['slug']
            ]);
            $oPage->addUrl(new Url($oUrlRoute, Url::CHANGEFREQ_ALWAYS, 0.2));
        }

        
        return $oPage;
    }

    /**
     * @return string
     */
    public function getSectionName()
    {
        return 'downloads';
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->oDownloadsRepository->getTotalCount();
    }
}