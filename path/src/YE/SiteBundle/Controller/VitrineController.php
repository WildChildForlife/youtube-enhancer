<?php

namespace YE\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Youtubedl\Youtubedl;
use PHPVideoToolkit\Video;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Finder\Finder;

use Symfony\Component\Process\Process;

use Cocur\BackgroundProcess\BackgroundProcess;

//use Sonata\MediaBundle\Filesystem\Local;

use YE\SiteBundle\Entity\Downloads;
use YE\SiteBundle\Entity\FacebookPosts;
use YE\SiteBundle\Form\DownloadsType;

use Facebook\Facebook;

class VitrineController extends Controller
{


    public function indexAction(Request $oRequest)
    {
        // Get the Referer if there is one
        $sReferer = $this->get('manager.downloads')->checkForReferer($oRequest);
        
        // Instanciation de l'entité
        $oDownload = new Downloads();
        
        // Instanciation du Formulaire
        $oForm = $this->createForm(new DownloadsType(), $oDownload);

        // Get the recent videos to display in Carousel
        $oRecentVideos = $this->getDoctrine()->getRepository('YESiteBundle:Downloads')->selectDistinctOrderByDate(16);

        $aVideoToDisplay = ( $sReferer != false ) ? $this->getVideoFromIDAction($oRequest, $sReferer, false) : false;

        return $this->render('YESiteBundle:Pages:OnePage.html.twig', array(
            'oForm' => $oForm->createView(),
            'oRecentVideos' => $oRecentVideos,
            'sReferer' => $sReferer,
            'aVideoToDisplay' => $aVideoToDisplay
        ));
    }

    /**
    * @Secure(roles="ROLE_USER, ROLE_ADMIN")
    */
    public function landingAction()
    {
        return $this->render('YESiteBundle:Pages:landing.html.twig');
    }

    public function getVideoFromIDAction(Request $oRequest, $_sVideoID = null, $bAjax = true)
    {
        $aResponse = $this->get('manager.downloads')->fetchVideoToDisplayById($oRequest, $_sVideoID);
        return ( $bAjax ) ? new JsonResponse($aResponse) : $aResponse;
    }

    public function downloadOctectFileAction($sExtension = 'mp3')
    {
        $oResponse = new Response();
        $oSession = $this->get('request')->getSession();

        $sTitle = $oSession->get('Youtube-Video-Title');

        $sVideoID = $oSession->get('Youtube-Video-ID');
        $sDescription = $oSession->get('Youtube-Video-Description');
        $sDatePublication = $oSession->get('Youtube-Video-DatePublication');

        $oDownloadManager = $this->get('manager.downloads');


        if ( strlen($sVideoID) <= 1 || strlen($sTitle) <= 1 ) return $this->redirectToRoute('ye_site_homepage');

        $oDownloadManager->checkMetaDataExists($sVideoID);

        $aData = $oDownloadManager->getFileContent('aFormats_', $sVideoID); 

        $aFormats = $oDownloadManager->getDifferentDownloadFormats($aData); 
        
        $oDownloadManager->prepareResponse($sTitle, $sExtension);        

        $oDownloadManager->getBestFormat($aFormats, $sExtension);
        $oDownload     = $oDownloadManager->persistDownload($sExtension);

        $oDownloadManager->shareOnFacebook($oDownload);

        // Execution de la commande : Telechargement du fichier
        $sCommand = $oDownloadManager->buildCommand($sExtension, $aData);

        $oResponse = $oDownloadManager->getResponse();
        $oResponse->send();

        // Conversion
        passthru($sCommand);

        return $oResponse;

    }

    public function downloadFileAction($sFileName)
    {  
        $sPath = $this->get('kernel')->getRootDir(). "/../web/" . $this->getParameter('youtube_downloads_folder') . "/";
        $sContent = file_get_contents($sPath.$sFileName);

        $sResponse = new Response();

        //set headers
        $sResponse->headers->set('Content-Type', 'mime/type');
        $sResponse->headers->set('Content-Disposition', 'attachment;filename="'.$sFileName);
        $sResponse->headers->set('Content-Length', filesize($sPath.$sFileName));

        $sResponse->setContent($sContent);
        return $sResponse;
    }

    public function deleteFileAction(Request $oRequest)
    {
        if ( $oRequest->isXmlHttpRequest() && !empty((string)$oRequest->request->get('sFileName')) )
        {
            $sFileName = $oRequest->request->get('sFileName');
            $oFileSystem = new Filesystem();
            $oSession = $oRequest->getSession();

            $sTitle = $oSession->get('Youtube-Video-Title');
            $sVideoID = $oSession->get('Youtube-Video-ID');
            $sExtension = $oSession->get('Youtube-Video-Extension');


            $sPath = $this->get('kernel')->getRootDir(). "/../web/" . $this->getParameter('youtube_downloads_folder') . "/";
            $oFileSystem->remove($sPath . $sFileName . $sExtension);

            return new JsonResponse(array('message' => 'success'));

        }
        else return new JsonResponse(array('message' => 'error'), 400);
    }

    public function aboutUsAction()
    {
        return $this->render('YESiteBundle:Pages:about-us.html.twig');
    }
    public function faqAction()
    {
        return $this->render('YESiteBundle:Pages:faq.html.twig');
    }
    public function termsOfUseAction()
    {
        return $this->render('YESiteBundle:Pages:terms-of-use.html.twig');
    }
    public function privacyAction()
    {
        return $this->render('YESiteBundle:Pages:privacy.html.twig');
    }
    // public function testtokenAction()
    // {
    //     
    // }
}
