<?php

namespace YE\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Downloads
 *
 * @ORM\Table(name="facebookposts")
 * @ORM\Entity(repositoryClass="YE\SiteBundle\Entity\Repository\FacebookPostsRepository")
 */
class FacebookPosts
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="datePost", type="datetime")
     */
    private $datePost;

    /**
     * @var string
     *
     * @ORM\Column(name="nodeId", type="string", length=255)
     */
    private $nodeId;


    /**
     * @ORM\OneToOne(targetEntity="Downloads")
     * @ORM\JoinColumn(name="download", referencedColumnName="id")
     */
    protected $download;
    
    function __construct() {
        $this->datePost = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datePost
     *
     * @param \DateTime $datePost
     *
     * @return FacebookPosts
     */
    public function setDatePost($datePost)
    {
        $this->datePost = $datePost;

        return $this;
    }

    /**
     * Get datePost
     *
     * @return \DateTime
     */
    public function getDatePost()
    {
        return $this->datePost;
    }

    /**
     * Set nodeId
     *
     * @param string $nodeId
     *
     * @return FacebookPosts
     */
    public function setNodeId($nodeId)
    {
        $this->nodeId = $nodeId;

        return $this;
    }

    /**
     * Get nodeId
     *
     * @return string
     */
    public function getNodeId()
    {
        return $this->nodeId;
    }

    /**
     * Set download
     *
     * @param \YE\SiteBundle\Entity\Downloads $download
     *
     * @return FacebookPosts
     */
    public function setDownload(\YE\SiteBundle\Entity\Downloads $download = null)
    {
        $this->download = $download;

        return $this;
    }

    /**
     * Get download
     *
     * @return \YE\SiteBundle\Entity\Downloads
     */
    public function getDownload()
    {
        return $this->download;
    }
}
