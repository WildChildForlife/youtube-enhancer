<?php

namespace YE\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Downloads
 *
 * @ORM\Table(name="downloads")
 * @ORM\Entity(repositoryClass="YE\SiteBundle\Entity\Repository\DownloadsRepository")
 */
class Downloads
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, updatable=true)
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=3)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="videoID", type="string", length=255, nullable=true)
     */
    private $videoID;

    /**
     * @var string
     *
     * @ORM\Column(name="formatID", type="integer")
     */
    private $formatID;

    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDownload", type="datetime")
     */
    private $dateDownload;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePublication", type="datetime")
     */
    private $datePublication;

    /**
     * @var boolean
     *
     * @ORM\Column(name="recentlyDownloaded", type="boolean")
     */
    private $recentlyDownloaded = true;


    function __construct() {
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->dateDownload = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Downloads
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

     /**
     * Set title
     *
     * @param string $title
     * @return Downloads
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Downloads
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return Downloads
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set videoID
     *
     * @param string $videoID
     * @return Downloads
     */
    public function setVideoID($videoID)
    {
        $this->videoID = $videoID;

        return $this;
    }

    /**
     * Get videoID
     *
     * @return string 
     */
    public function getVideoID()
    {
        return $this->videoID;
    }

    /**
     * Set dateDownload
     *
     * @param \DateTime $dateDownload
     * @return Downloads
     */
    public function setDateDownload($dateDownload)
    {
        $this->dateDownload = $dateDownload;

        return $this;
    }

    /**
     * Get dateDownload
     *
     * @return \DateTime 
     */
    public function getDateDownload()
    {
        return $this->dateDownload;
    }

    /**
     * Set datePublication
     *
     * @param \DateTime $datePublication
     * @return Downloads
     */
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get datePublication
     *
     * @return \DateTime 
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * Set formatID
     *
     * @param integer $formatID
     * @return Downloads
     */
    public function setFormatID($formatID)
    {
        $this->formatID = $formatID;

        return $this;
    }

    /**
     * Get formatID
     *
     * @return integer 
     */
    public function getFormatID()
    {
        return $this->formatID;
    }

    /**
     * Set recentlyDownloaded
     *
     * @param boolean $recentlyDownloaded
     * @return Downloads
     */
    public function setRecentlyDownloaded($recentlyDownloaded)
    {
        $this->recentlyDownloaded = $recentlyDownloaded;

        return $this;
    }

    /**
     * Get recentlyDownloaded
     *
     * @return boolean 
     */
    public function getRecentlyDownloaded()
    {
        return $this->recentlyDownloaded;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Downloads
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
