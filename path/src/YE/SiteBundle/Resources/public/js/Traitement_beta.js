$(document).ready(function(){

	var $sVideoID = '';
	var $sUrl = '';
	var $sTitle = '';

	$("html").niceScroll({
		horizrailenabled: false
	}); 

	if ( typeof typeAnimation != 'undefined' && typeAnimation == true && $(window).innerWidth() > 960 )
	{
		// Typed animation
		$(".typed").typed({
	    	strings: ["http://www.youtube.com/watch?v=Hfjdfbdnf", 'http://www.youtube.com/watch?v=^500fdhsjkfl8767'],
	    	typeSpeed: 0,
	    	showCursor: true,
	    	cursorChar: "|",
	    	callback: function(){
	    		// Focus l'input à la fin
	    		$(".input-convert").focus();
	    		// Hide l'animation
	    		$(".typed-container").hide(500);
	    	}
	  	});
	  	// En cliquant sur l'animatione elle disparait
	  	$(document).on("click", ".typed-container", function(){
	  		$(this).hide(500);
	  		$(".input-convert").focus();
	  	});
	}
	else $(".typed-container").hide(5);

	

  	$(document).on("click", ".goTop", function(event){
		event.preventDefault();
		$("html").getNiceScroll(0).doScrollTop(0, 1000);
		setTimeout(function()
		{
			$(".input-convert").focus();
		}, 1000);
	});

  	// En faisant une première recherche pour afficher la video en question
  	$(document).on("submit", ".beta-version .form-search-website", function(event){
  		event.preventDefault();
  		if (sProdEnv)
		{
			console.log('Search');
			fbq('track','Search');
		}
  		// Préservation de l'URL
  		$sUrl = $(".input-convert").val();
  		$(".beta-version .result-convesion, .no-result-found").hide(500, function(){
  			$(".beta-version .ajax-show-result").animate({ opacity: 1 }, 500);	
  			$(".beta-version .loader-result-ajax").show(1000);
  		});
  		$("html").getNiceScroll(0).doScrollTop($(".beta-version .ajax-show-result").position().top, 3000);
  		$.ajax({
	        type: "POST",
	        url: PATH_GET,
	        data: 
	        {
	           sUrl: $(".input-convert").val()
	        },
	        dataType: "json",
	        success: function(response) 
	        {
	        	if ( response.empty == "yes" )
	        	{
	        		$(".no-result-found").css({ display: 'block' }).animate({ opacity: 1 }, 500);
	        		$(".loader-result-ajax, .result-convesion").hide(1000);
	        	}
	        	else
	        	{
	        		$sVideoID = response.idVideo;
	        		$sTitle = response.title;	        		

        			$(".loader-result-ajax, .no-result-found").hide(1000);
	        		$(".beta-version .result-convesion").css({ display: 'block' }).animate({ opacity: 1 }, 500);
		            $(".video-iframe").empty().append("<iframe src='https://www.youtube.com/embed/" + response.idVideo + "?autoplay=0&showinfo=0&controls=0' frameborder='0' allowfullscreen width='100%' height='220' >");
		            $(".result-title").text(response.title);
		            $(".result-date").text(response.publishedAt);
		            $(".download-ajax-file").attr('data-id', response.idVideo);
		            $(".result-channel").html('<a href="https://www.youtube.com/channel/' + response.channelId + '" target="_blank" >' + response.channelTitle + '</a>');

		            if (sProdEnv)
					{
						console.log('Result Found');
						//_fbq.push(['track','220855258249128']);
						fbq('track','Result', {
							'videoID':response.idVideo
						});
					}

		            if ( response.description.length == 0 ) $(".result-description-container").hide(); 
		            else
	            	{
	            		var $sDescription = response.description;
	            		//console.log($sDescription.length);
	            		if ( $sDescription.length > 480 ) $sDescription = $sDescription.substring(0, 480) + '...';
	            		$(".result-description-container").show(); 
	            		$(".result-description").html($sDescription);
	            	}
	        	}
	        }
	    });
  	});


	var $oLoop = setInterval(function()
	{
		if ( Cookies.get("begindownload") == 1 )
		{
			if ( Cookies.get("progressdownload") == undefined ) Cookies.set("progressdownload", 0);
			var $iActualProgress = parseInt(Cookies.get("progressdownload"));
			var $iNextProgress = "";

			if ( $iActualProgress == 0 ) $iNextProgress = $iActualProgress + 5;
			else if ( $iActualProgress > 0 && $iActualProgress < 50 ) $iNextProgress = $iActualProgress + 5;
			else $iNextProgress = $iActualProgress + ( ( 100 - $iActualProgress ) / 4 ) ;

			//console.log($iNextProgress);

			if ( Cookies.get('download') != 'finished' ) 
				$(".textProgress").data("animated", false).textProgress($iNextProgress);

			Cookies.set("progressdownload", $iNextProgress);

			//console.log($iNextProgress);
		}

		if ( Cookies.get('download') == 'finished' )
		{
			$(".textProgress").data("animated", false).textProgress(100);

			setTimeout(function()
			{
				$(".loader-preparing-download").animate({ opacity : 0 }, 500, function()
				{
					$(this).css({ display : 'none' });
					Cookies.remove('download');
					Cookies.remove('begindownload');
					Cookies.set("progressdownload", 0);

					//console.log("All Cookies reinitialized");

					$(".textProgress").data("animated", false).textProgress(0);
				});
			}, 500);
		}
	}, 1000);


	
	$(document).on("click", ".download-ajax-file", function()
	{
		if (sProdEnv)
		{
			console.log('Download');
			var sFormat;

			if ( $(this).hasClass('download-ajax-mp3') ) sFormat = 'mp3';
			else sFormat = 'mp4';	

			//_fbq.push(['track','220855258249128',{'Download':'true','format':sFormat, 'videoID':$(this).data('id')}]);

			fbq('track','Download', {
				'format':sFormat,
				'videoID':$(this).data('id')
			});
		}

		Cookies.set("begindownload", 1);
		$(".loader-preparing-download").css({ display : 'block' }).animate({ opacity : 1 }, 500, function(){
			$(".textProgress").data("animated", false).textProgress(10);
		});		
	});
	

	// En cliquant sur le picto de telechargement (recherche de video), déclenchez l'envoi du formulaire
	$(document).on("click", ".beta-version .form-search-website i", function(event){
		event.preventDefault();
		$(".beta-version .form-search-website").submit();
	});

	$(document).on("click", ".preventDefault", function(event){ event.preventDefault(); })

	$(".owl-carousel").owlCarousel(
	{
		items: 4,
		loop: true,
		center: true,
		margin: 50,
		autoplay: true,
		autoplayTimeout: 3000,
		autoplayHoverPause: true
	});

	$(document).on("click", ".section-faq h2", function(){
		$(this).parent("article").toggleClass('checked');
	});
        
        
        
        


	// $(document).on("click", ".download-recent", function(event){
	// 	event.preventDefault();
	// 	$(".input-convert").val("https://www.youtube.com/watch?v=" + $(this).data("videoid"));
	// 	$(".beta-version .form-search-website").submit();
	// });

});