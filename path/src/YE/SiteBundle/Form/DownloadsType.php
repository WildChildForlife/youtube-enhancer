<?php

namespace YE\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class DownloadsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('videoID', 'url', array('attr' => array('autocomplete' => 'off')));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'YE\SiteBundle\Entity\Downloads'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ye_sitebundle_downloads';
    }
}
