<?php

namespace YE\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class YESiteBundle extends Bundle
{
	public function getParent()
    {
        return 'SonataDoctrineORMAdminBundle';
    }
}
