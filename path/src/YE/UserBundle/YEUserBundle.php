<?php

namespace YE\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class YEUserBundle extends Bundle
{
	public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}
